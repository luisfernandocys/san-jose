/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    $("#divnuevaentrada").click(function () {
        $("#nuevaentradadiv").toggle('slow');
    });
    $('#unidades').prop('disabled', true);
    $('#paquetes').prop('disabled', true);
    $('#cajas').prop('disabled', true);
    //$('#producto').change(cargar_proveedores); //cargar proveedor al elegir un producto
    $(".boton_disable").click(boton_disable);
    $("#cantidadingresa").bind('change paste keyup', calcular_total);
    $("#formaentrada").change(forma_entrada);
    $('#btn_agregar_entrada').click(agregarentrada);
    $(".detalle_entradas").click(detalle_entradas);
    $(".print").click(imprimir);

    //tabla de entradas
    
    var table = $('#tabla_entradas').DataTable({
         dom: 'Bfrtip',
        buttons: [
            {
                extend: 'colvis',
                text: 'Selecciona columnas',
                columns: ':not(:first-child)'
            },
            {
                extend: 'print',
                text:'Imprimir',
                exportOptions: {
                    columns: ':visible'
                }
            },
             {
                extend: 'pdf',
                exportOptions: {
                    columns: ':visible'
                }
            },
             {
                extend: 'excel',
                exportOptions: {
                    columns: ':visible'
                }
            }
        ],
        /*'copy', 'csv', 'excel', 'pdf', 'print',*/
        "language": {
            "emptyTable": "No hay datos para mostrar",
            "info": "Mostrando _START_ de _END_ de _TOTAL_ entradas totales ",
            "infoEmpty": "Mostrando 0 de 0 de 0 entradas",
            "infoFiltered": "(filtered from _MAX_ total entries)",
            "infoPostFix": "",
            "lengthMenu": "Mostrando _MENU_ resultados",
            "thousands": ",",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "No hay resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior",
            },
            "aria": {
                "sortAscending": ": Activa para ordenar de forma ascendente",
                "sortDescending": ": Activa para ordenar de forma descendente"
            },
        }, "scrollX": true

    });
});

function agregarentrada() {


    $("div.modal-body").find('input').val(''); //pa limpiar modals

    // add the rule here
    $.validator.addMethod("valueNotEquals", function (value, element, arg) {
        return arg != value;
    }, "Value must not equal arg.");
    $("#entradasform").validate({
        errorClass: "error_validate",
        validClass: "ok_validate",
        rules: {
            producto: {valueNotEquals: "0"},
            cantidadingresa: "required",
            formaentrada: {valueNotEquals: "0"},
            almacen: {valueNotEquals: "0"}

        }, //fin rules
        messages:
                {
                    producto: "Elija un producto",
                    cantidadingresa: "La cantidad no puede ser 0",
                    formaentrada: "Elija una forma de entrada",
                    almacen: "Elija un almacen"
                }, //fin msg
        submitHandler: function (form) {



            $('#loading').slideDown();

            form.submit();
        }, //fin submit handler
        invalidHandler: function (event, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                var message = errors == 1
                        ? 'Please correct the following error:\n'
                        : 'Please correct the following ' + errors + ' errors.\n';
                var errors = "";
                if (validator.errorList.length > 0) {
                    for (x = 0; x < validator.errorList.length; x++) {
                        errors += "\n\u25CF " + validator.errorList[x].message;
                    }
                }
                //  alert(message + errors);
            }
            validator.focusInvalid();
        }



    }); //fin validate


}

function cargar_proveedores()
{
    // $('#div_tipoproducto_ddl').hide('fast');
    $('#loading_proveedor').slideDown();
    $("#div_proveedor").hide();
    $("#cantidadingresa").val(0);
    $("#cantidadunidad").val(0);
    $("#cantidadunidadh").val(0);
    $("#valor").val(0);
    var path = $(this).attr('data-path');
    var id = this.value;
    var url = path + '/' + id;
    if (id > 0)
    {
        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: {},
            success: function (response) {

                var sel = $("#proveedor");
                sel.html('');
                var detalle = response.detalle;
                console.log(detalle);
                /*console.log(prov[0].empresa);*/

                for (var i = 0; i < detalle.length; i++) {
                    sel.append('<option value="' + detalle[i][0].proveedor.id + '">' + detalle[i][0].proveedor.empresa + '</option>');
                }
                // $("#proveedor").text(detalle[0].empresa);
                if(detalle[0].nombre=='Metros')
                {
                    $("#unidad").val(detalle[0].nombre+' 2');
                }
                else
                {
                    $("#unidad").val(detalle[0].nombre);
                }
                
                $("#unidadmedida").val(detalle[0].id);
                $("#numero").val(detalle[0][0].producto.numero);
                $("#peso").val(detalle[0][0].producto.peso);
                $("#ancho").val(detalle[0][0].producto.ancho);
                $("#longitud").val(detalle[0][0].producto.longitud);
                $("#div_proveedor").fadeIn('slow');
                $("#loading_proveedor").hide();
            }
        });
    } else
    {
        $("#unidad").val('');
        $("#loading_proveedor").hide();
    }


}

function calcular_total()
{
    var uni = $("#unidadmedida_ddl").val();
    console.log(uni);
    var numero = $("#numero").val();
    var peso = $("#peso").val();
    var ancho = $("#ancho").val();
    var lon = $("#longitud").val();
    var cantidad = $("#cantidadingresa").val();
    var total = $("#cantidadunidad");
    var totalh = $("#cantidadunidadh");

    if (uni == 1)
    {
        total.val(((ancho/100) * (lon/100)) * (cantidad));
        totalh.val(((ancho/100) * (lon/100)) * (cantidad));
        $("#valor").val(((ancho/100) * (lon/100))); //entre 100 pa sacar los mts
    }
    if (uni == 2)
    {
        total.val(cantidad * peso);
        totalh.val(cantidad * peso);
        $("#valor").val(peso);
    }
    if (uni == 3 || uni == 4 || uni==5)
    {
        total.val(cantidad * numero);
        totalh.val(cantidad * numero);
        $("#valor").val(numero);
    }
   
}

function forma_entrada()
{
    var id = $("#formaentrada").val();
    if (id == 1) //1 unidad, 2 paquete, 3 caja      $('#peso').prop('disabled', true);
    {
        $('#unidades').prop('disabled', false).focus();
        $('#paquetes').prop('disabled', true).val(0);
        $('#cajas').prop('disabled', true).val(0);

    }

    if (id == 2)
    {
        $('#unidades').prop('disabled', false).val(0);
        $('#paquetes').prop('disabled', false).focus();
        ;
        $('#cajas').prop('disabled', true).val(0);
    }

    if (id == 3)
    {
        $('#unidades').prop('disabled', false).val(0);
        $('#paquetes').prop('disabled', false).val(0);
        $('#cajas').prop('disabled', false).focus();
    }
    if (id == 0)
    {
        $('#unidades').prop('disabled', true).val(0);
        $('#paquetes').prop('disabled', true).val(0);
        $('#cajas').prop('disabled', true).val(0);
    }





}

function detalle_entradas()
{

    $('#loading').slideDown();
    var path = $(this).attr('data-path');
    var identrada = $(this).attr('data-id');
    //limpian los inputs e img

    $("div.modal-body").find('input').val(''); //pa limpiar modals


    $.post(path,
            {
                id: identrada
            },
            function (response) {
                if (response.code == 100 && response.success) {//dummy check

                    $("#codigos").empty();
                    var datos = response.detalle;

                    var i = 0;

                    for (i = 0; i < Object.keys(datos).length; i++)
                    {
                        var html = '';
                        html += '<div class="col-md-12">';
                        html += '<img id="barcode' + datos[i].id + '" class="img-responsive" style="margin:0 auto;" /><div class="text-center">Codigo:' + datos[i].codigo + '  <a download="" style="cursor: pointer;" class="btn-link" id="btndescargar' + datos[i].id + '">Descargar     </a>';
                        // html+='<a  style="cursor: pointer;" class="btn-link print" id="printcodigo'+datos[i].id+'">Imprimir</a>';
                        html += '</div></div>';

                        $("#codigos").append(html);
                    }
                    for (i = 0; i < Object.keys(datos).length; i++)
                    {
                        generar_codigo(datos[i].codigo, datos[i].id);
                    }

                    //var urlred = window.location.href;
                    //window.location.href = urlred;

                    $('#loading').slideUp('3000');


                    //$('#detalleentradas').modal();

                }

            }, "json");
}

function generar_codigo(codigo, id)
{
    var idimg = "#barcode" + id;

    $(idimg).JsBarcode(codigo, {
        width: 2,
        height: 60,
        quite: 10,
        format: "CODE128",
        backgroundColor: "#fff",
        lineColor: "#000"

    });

    var img = $("#barcode" + id);
    var src = img.attr('src');
    $("#btndescargar" + id).attr('href', src).attr('download', codigo);
}
function imprimir()
{
    alert();
    var id = $("#barcode" + idcodigo);
    $.print(id);
}
function boton_disable()
{
    alert('No tienes los permisos suficientes para realizar esta acción');
    exit(0);
}