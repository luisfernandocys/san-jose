/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function() {

   

    //tabla de entradas
    var table = $('#tabla_inventario').DataTable({
         dom: 'Bfrtip',
        buttons: [
            {
                extend: 'colvis',
                text: 'Selecciona columnas',
                columns: ':not(:first-child)'
            },
            {
                extend: 'print',
                text:'Imprimir',
                exportOptions: {
                    columns: ':visible'
                }
            },
             {
                extend: 'pdf',
                exportOptions: {
                    columns: ':visible'
                }
            },
             {
                extend: 'excel',
                exportOptions: {
                    columns: ':visible'
                }
            }
        ],
        /*'copy', 'csv', 'excel', 'pdf', 'print',*/
        "language": {
            "emptyTable": "No hay datos para mostrar",
            "info": "Mostrando _START_ de _END_ de _TOTAL_ entradas totales ",
            "infoEmpty": "Mostrando 0 de 0 de 0 entradas",
            "infoFiltered": "(filtered from _MAX_ total entries)",
            "infoPostFix": "",
            "lengthMenu": "Mostrando _MENU_ resultados",
            "thousands": ",",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar por producto:",
            "zeroRecords": "No hay resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior",
            },
            "aria": {
                "sortAscending": ": Activa para ordenar de forma ascendente",
                "sortDescending": ": Activa para ordenar de forma descendente"
            },
            
        },"scrollX": true

    });
});

function disponible(id,path)
{
$(".loading_dp").show();
    $('#loading').slideDown();
    $.post(path,
            {
                id: id
            },
    function(response) {
        if (response.code == 100 && response.success) {//dummy check

   
    //console.log(response.valor[0]);
   //console.log()
   
    if(!jQuery.isEmptyObject(response.valor[0]))
    {
         //$('#detalleentradas').modal();
         var valor=response.valor[0].valor;
         var existencia = response.existencia;
         var disponible = valor*existencia;
         
            
            $("#dp"+id).text(disponible);
            $("#undp"+id).fadeIn('slow');
            $("#loading_dp"+id).fadeOut('fast');
            
    }
    else
    {
         //$('#detalleentradas').modal();
            
            $("#dp"+id).text(0);
             $("#undp"+id).fadeIn('slow');
            $("#loading_dp"+id).fadeOut('fast');
           
    }
    
            $('#loading').slideUp('3000');


           
        }
       
    }, "json");
}