/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Get context with jQuery - using jQuery's .get() method.



var areaChartData = {
    labels: labelsnuevos(productos),
    datasets: entradasgenerales(entradasg, salidasg)
};

var existenciasData=
        {
            labels: labelsnuevos(productos),
    datasets:existenciasDataset(existencias)
        }

function labelsnuevos(productos)
{
    var labels = [];


    $.each(productos, function (key, value) {
        var lab = value.nombre;
        labels.push(lab);

    });

    return labels;
}

function entradasgenerales(exist, salidas) //función para generar la gráfica de entradas y salidas generales
{

    var data;
    var datasets = [];
    var data = [];
    var datasalida = [];

    $.each(exist, function (key, value) {

        var ex = value;
        data.push(ex);

    });

    $.each(salidas, function (key, value) {

        var ex = value;
        datasalida.push(ex);

    });




    var dataset =
            {
                label: "Entradas",
                backgroundColor: "red",
                data: data
            }
    var dataset2 =
            {
                label: "Salidas   ",
                data: datasalida
            }

    datasets.push(dataset);
    datasets.push(dataset2);




    return datasets;
}

function existenciasDataset(exist) //función para generar la gráfica de entradas y salidas generales
{

    var data;
    var datasets = [];
    var data = [];
    var datasalida = [];

    $.each(exist, function (key, value) {

        var ex = value;
        data.push(ex);

    });




    var dataset =
            {
                label: "Existencias",
                
                data: data
            }
   
    datasets.push(dataset);
    




    return datasets;
}

//-------------
//- BAR CHART -
//-------------
var barChartCanvas = $("#miChart").get(0).getContext("2d");
var barChartExistencias = $("#miChartExistencias").get(0).getContext("2d");
var barChart = new Chart(barChartCanvas);
var barChartExistencias = new Chart(barChartExistencias);
var barChartData = areaChartData;
var barChartDataExistencias = existenciasData;
//barChartData.labels[0]="AAA";
barChartData.datasets[0].fillColor = "#FF8F00";
barChartData.datasets[0].strokeColor = "#FF8F00";
barChartData.datasets[0].pointColor = "#FF8F00";
barChartData.datasets[0].highlightFill = "#FF8F00";
barChartData.datasets[0].highlightStroke = "#FF8F00";
barChartData.datasets[1].fillColor = "#3C7DC4";
barChartData.datasets[1].strokeColor = "#3C7DC4";
barChartData.datasets[1].pointColor = "#3C7DC4";
barChartData.datasets[1].highlightFill = "#3C7DC4";
barChartData.datasets[1].highlightStroke = "#3C7DC4";
//Opciones para las existencias
barChartDataExistencias.datasets[0].fillColor = "#FFFF66";
barChartDataExistencias.datasets[0].strokeColor = "#FFFF66";
barChartDataExistencias.datasets[0].pointColor = "#FFFF66";
barChartDataExistencias.datasets[0].highlightFill = "#FFFF66";
barChartDataExistencias.datasets[0].highlightStroke = "#FFFF66";

var barChartOptions = {
    
    responsive: true,
    multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>",
    maintainAspectRatio: true
};

barChartOptions.datasetFill = false;

barChart.Bar(barChartData, barChartOptions);
barChartExistencias.Bar(barChartDataExistencias,barChartOptions);


    