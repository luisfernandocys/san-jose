
$(document).ready(function () {



    $('#loading').slideUp('2000'); //subir el loading
datatable();

    $("#proveedor_ddl").change(function () {
        var valor = $("#proveedor_ddl").val();
        if (valor == 0)
        {
            $("#divpppr").fadeOut('3000');
        } else
        {
            console.log('call');
            detalleReporte();
        }

    });




});
function datatable()
{
    var table = $('#table_pui').DataTable({
        
        "language": {
            "emptyTable": "No hay datos para mostrar",
            "info": "Mostrando _START_ de _END_ de _TOTAL_ entradas totales ",
            "infoEmpty": "Mostrando 0 de 0 de 0 entradas",
            "infoFiltered": "(filtered from _MAX_ total entries)",
            "infoPostFix": "",
            "lengthMenu": "Mostrando _MENU_ resultados",
            "thousands": ",",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "No hay resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior",
            },
            "aria": {
                "sortAscending": ": Activa para ordenar de forma ascendente",
                "sortDescending": ": Activa para ordenar de forma descendente"
            },
            
        },"scrollX": true,
        "columnDefs": [
            { "visible": false, "targets": 2 }
        ],
        "order": [[ 2, 'asc' ]],
        "displayLength": 25,
        "drawCallback": function ( settings ) {
        var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
        api.column(2, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group" style="background-color: #FFFF66;"><td colspan="14">'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
             // Order by the grouping
    $('#table_pui tbody').on( 'click', 'tr.group', function () {
        var currentOrder = table.order()[0];
        if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
            table.order( [ 2, 'desc' ] ).draw();
        }
        else {
            table.order( [ 2, 'asc' ] ).draw();
        }
    } );
        }
        
        

    });
}
function detalleReporte()
{

    var path = $("#proveedor_ddl").attr('data-path');
    var prov = $("#proveedor_ddl").val();
     $('#loadingddl').fadeIn();
    //console.log(prov);


    $.post(path,
            {
                id: prov
            },
            function (response) {
                if (response.code == 100 && response.success) {//dummy check
                    if (jQuery.isEmptyObject(response.detalle))
                    {
                       
                        $("#divpppr").fadeIn('3000');
                     $("#table_pui tbody").remove(); 
                     $("#table_pui thead").remove();
                    var tabla= "";
                    tabla+='<thead><tr><th colspan="4" class="text-center">NO HAY PRODUCTOS PARA ESTE PROVEEDOR</th></tr><tr><th>Producto</th><th>Precio 1</th><th>Precio 2</th><th>Precio 3</th></tr></thead>';
                     $('#loadingddl').fadeOut();
                        $('#table_pppr').append(tabla);
                      
                    }
                    else
                    {
                     console.log(response.detalle);
                    $("#divpppr").fadeIn('3000');
                     $("#table_pui tbody").remove(); 
                     $("#table_pui thead").remove();
                    var tabla= "";
                    tabla+='<thead><tr style="background-color:rgb(255, 143, 0);color:#fbfbfb;"><th colspan="4" class="text-center">' + response.detalle[0].proveedor.empresa + '</th></tr><tr><th>Producto</th><th>Precio 1</th><th>Precio 2</th><th>Precio 3</th></tr></thead>';
                    for (var i = 0; i < response.detalle.length; i++) {
                        tabla += '<tr><td>' + response.detalle[i].producto.nombre +' - '+ response.detalle[i].producto.marca +' - '+response.detalle[i].producto.modelo.nombre+' - '+response.detalle[i].producto.color.nombre+'</td><td>' + response.detalle[i].precio1 + '</td><td>' + response.detalle[i].precio2 + '</td><td>' + response.detalle[i].precio3 + '</td></tr>';

                    }
                    $('#loadingddl').fadeOut();
                    $('#table_pui').append(tabla); 
                     
                    }
                    
                    

                   



                }
               
                
            }, "json").fail(function (jqXHR, textStatus, errorThrown)
    {
        console.log(jqXHR);
        console.log(errorThrown);
        console.log(textStatus);
        alert(textStatus);
    });
}