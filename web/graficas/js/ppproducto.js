
$(document).ready(function () {



    $('#loading').slideUp('2000'); //subir el loading
    $("#nuevoreporte").click(refrescar);

    $("#producto_ddl").change(function () {
        var valor = $("#producto_ddl").val();
        if (valor == 0)
        {
            $("#divpppr").fadeOut('3000');
        } else
        {
            console.log('call');
            detalleReporte();
        }

    });




});
function refrescar()
{
    location.reload();
}
function detalleReporte()
{

    var path = $("#producto_ddl").attr('data-path');
    var prov = $("#producto_ddl").val();
    $('#loadingddl').fadeIn();
    //console.log(prov);


    $.post(path,
            {
                id: prov
            },
            function (response) {
                if (response.code == 100 && response.success) {//dummy check
                    if (jQuery.isEmptyObject(response.detalle))
                    {

                        $("#divpppr").fadeIn('3000');
                        $("#table_ppproducto tbody").remove();
                        $("#table_ppproducto thead").remove();
                        grafica(0, 0, 0,0);
                        var tabla = "";
                        tabla += '<thead><tr><th colspan="4" class="text-center">NO HAY DATOS PARA ESTA CONSULTA</th></tr><tr><th>Proveedor</th><th>Precio 1</th><th>Precio 2</th><th>Precio 3</th></tr></thead>';
                        $('#loadingddl').fadeOut();
                        $('#table_ppproducto').append(tabla);

                    } else
                    {
                        var precios1 = [];
                        var precios2 = [];
                        var precios3 = [];
                        var prov = [];
                        console.log(response.detalle);
                        $("#divpppr").fadeIn('3000');
                        $("#table_ppproducto tbody").remove();
                        $("#table_ppproducto thead").remove();
                        var tabla = "";
                        tabla += '<thead><tr style="background-color:rgb(255, 143, 0);color:#fbfbfb;"><th colspan="4" class="text-center">' + response.detalle[0].entrada.producto.nombre + '</th></tr><tr><th>Proveedor</th><th>Precio 1</th><th>Precio 2</th><th>Precio 3</th></tr></thead>';
                        for (var i = 0; i < response.detalle.length; i++) {
                            if (response.detalle[i].precio1 == null)
                            {
                                response.detalle[i].precio1 = 0;
                            }
                            if (response.detalle[i].precio2 == null)
                            {
                                response.detalle[i].precio2 = 0;
                            }
                            if (response.detalle[i].precio3 == null)
                            {
                                response.detalle[i].precio3 = 0;
                            }

                            tabla += '<tr><td>' + response.detalle[i].entrada.proveedor.empresa + '</td><td>' + response.detalle[i].precio1 + '</td><td>' + response.detalle[i].precio2 + '</td><td>' + response.detalle[i].precio3 + '</td></tr>';
                            //asignar precios
                            precios1.push(response.detalle[i].precio1);
                            precios2.push(response.detalle[i].precio2);
                            precios3.push(response.detalle[i].precio3);
                            prov.push(response.detalle[i].entrada.proveedor.empresa);

                        }

                        $('#loadingddl').fadeOut();
                        $('#table_ppproducto').append(tabla);
                        //PINTAR GRÁFICA
                        $("#productotitulo").text(response.detalle[0].entrada.producto.nombre);
                        grafica(precios1, precios2, precios3, prov);
                    }







                }


            }, "json").fail(function (jqXHR, textStatus, errorThrown)
    {
        console.log(jqXHR);
        console.log(errorThrown);
        console.log(textStatus);
        alert(textStatus);
    });
}

function grafica(precios1, precios2, precios3, prov)
{

    var areaChartData1 = {
        labels: labelsnuevos(prov),
        datasets: precios(precios1)
    };
    var areaChartData2 = {
        labels: labelsnuevos(prov),
        datasets: precios(precios2)
    };
    var areaChartData3 = {
        labels: labelsnuevos(prov),
        datasets: precios(precios3)
    };


    function precios(precio) //función para generar la gráfica de entradas y salidas generales
    {
        var datasets = [];
        var datauno = [];
        $.each(precio, function (key, value) {

            var ex = value;
            datauno.push(ex);
        });
        var dataset1 =
                {
                    data: datauno
                };
        datasets.push(dataset1);
        return datasets;
    }

    function labelsnuevos(prov)
    {
        var labels = [];


        $.each(prov, function (key, value) {

            var lab = value;
            labels.push(lab);

        });
        

        return labels;
    }



//-------------
//- BAR CHART -
//-------------contenedorgraficas
$("#charprecio1").remove();
$("#charprecio2").remove();
$("#charprecio3").remove();
$("#grafica1").append('<canvas id="charprecio1" height="400"><canvas>');
$("#grafica2").append('<canvas id="charprecio2" height="400"><canvas>');
$("#grafica3").append('<canvas id="charprecio3" height="400"><canvas>');
    var barChartCanvas = $("#charprecio1").get(0).getContext("2d");
    
    var barChartCanvas2 = $("#charprecio2").get(0).getContext("2d");
    var barChartCanvas3 = $("#charprecio3").get(0).getContext("2d");
    var barChartData1 = areaChartData1;
    var barChartData2 = areaChartData2;
    var barChartData3 = areaChartData3;
    var barChart = new Chart(barChartCanvas);
    var barChart2 = new Chart(barChartCanvas2);
    var barChart3 = new Chart(barChartCanvas3);

//barChartData.labels[0]="AAA";
    barChartData1.datasets[0].fillColor = "#FF8F00";
    barChartData1.datasets[0].strokeColor = "#FF8F00";
    barChartData1.datasets[0].pointColor = "#FF8F00";
    barChartData1.datasets[0].highlightFill = "#FF8F00";
    barChartData1.datasets[0].highlightStroke = "#FF8F00";
    barChartData2.datasets[0].fillColor = "#3C7DC4";
    barChartData2.datasets[0].strokeColor = "#3C7DC4";
    barChartData2.datasets[0].pointColor = "#3C7DC4";
    barChartData2.datasets[0].highlightFill = "#3C7DC4";
    barChartData2.datasets[0].highlightStroke = "#3C7DC4";
    barChartData3.datasets[0].fillColor = "#EDE574";
    barChartData3.datasets[0].strokeColor = "#EDE574";
    barChartData3.datasets[0].pointColor = "#EDE574";
    barChartData3.datasets[0].highlightFill = "#EDE574";
    barChartData3.datasets[0].highlightStroke = "#EDE574";


    var barChartOptions = {
        responsive: true,
        multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>",
        maintainAspectRatio: true
    };

    barChartOptions.datasetFill = false;

    barChart.Bar(barChartData1, barChartOptions);
    
    barChart2.Bar(barChartData2, barChartOptions);
    barChart3.Bar(barChartData3, barChartOptions);
}