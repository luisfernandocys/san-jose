/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Get context with jQuery - using jQuery's .get() method.
var areaChartCanvas = $("#barChart").get(0).getContext("2d");
// This will get the first returned node in the jQuery collection.
var areaChart = new Chart(areaChartCanvas);

console.log(exist);
var areaChartData = {
    labels: labelsnuevos(productos),
    datasets: existencias(exist)
};


function labelsnuevos(productos)
{
    var labels = [];


    $.each(productos, function (key, value) {
        var lab = value.nombre;
        labels.push(lab);

    });

    return labels;
}

function existencias(exist)
{

    var data;
    var datasets = [];
    var data = [];

    $.each(exist, function (key, value) {



        var ex = value;
        data.push(ex);

    });




    var dataset =
            {
                fillColor: "rgba(62,141,188,0.9)",
                strokeColor: "rgba(60,241,188,0.9)",
                highlightFill: "rgba(60,141,088,0.9)",
                highlightStroke: "rgba(60,141,18,0.9)",
                data: data
            }
   /* var dataset2 =
            {
                fillColor: "rgba(000,64,34,0.3)",
                strokeColor: "rgba(60,241,188,0.9)",
                highlightFill: "rgba(60,141,088,0.9)",
                highlightStroke: "rgba(60,141,18,0.9)",
                data: data
            }*/

    datasets.push(dataset);
    //datasets.push(dataset2);




    return datasets;
}

//-------------
//- BAR CHART -
//-------------
var barChartCanvas = $("#barChart").get(0).getContext("2d");
var barChart = new Chart(barChartCanvas);
var barChartData = areaChartData;
barChartData.datasets[0].fillColor = "#00a65a";
barChartData.datasets[0].strokeColor = "#00a65a";
barChartData.datasets[0].pointColor = "#00a65a";
var barChartOptions = {
    //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
    scaleBeginAtZero: true,
    //Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines: true,
    //String - Colour of the grid lines
    scaleGridLineColor: "rgba(0,0,0,.05)",
    //Number - Width of the grid lines
    scaleGridLineWidth: 1,
    //Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,
    //Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines: true,
    //Boolean - If there is a stroke on each bar
    barShowStroke: true,
    //Number - Pixel width of the bar stroke
    barStrokeWidth: 2,
    //Number - Spacing between each of the X value sets
    barValueSpacing: 5,
    //Number - Spacing between data sets within X values
    barDatasetSpacing: 1,
    //String - A legend template
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
    //Boolean - whether to make the chart responsive
    responsive: true,
    maintainAspectRatio: true
};

barChartOptions.datasetFill = false;
barChart.Bar(barChartData, barChartOptions);


    