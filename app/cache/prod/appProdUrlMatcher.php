<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/admin')) {
            // admin_homepage
            if ($pathinfo === '/admin') {
                return array (  '_controller' => 'AdminBundle\\Controller\\InicioController::indexAction',  '_route' => 'admin_homepage',);
            }

            if (0 === strpos($pathinfo, '/admin/usuario')) {
                if (0 === strpos($pathinfo, '/admin/usuarios')) {
                    // usuarios
                    if ($pathinfo === '/admin/usuarios') {
                        return array (  '_controller' => 'AdminBundle\\Controller\\UsuariosController::usuariosAction',  '_route' => 'usuarios',);
                    }

                    // crear_usuario
                    if ($pathinfo === '/admin/usuarios/crear_usuario') {
                        return array (  '_controller' => 'AdminBundle\\Controller\\UsuariosController::crearAction',  '_route' => 'crear_usuario',);
                    }

                    // detalle_usuario
                    if (0 === strpos($pathinfo, '/admin/usuarios/detalle') && preg_match('#^/admin/usuarios/detalle/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'detalle_usuario')), array (  '_controller' => 'AdminBundle\\Controller\\UsuariosController::detalleAction',));
                    }

                    // modificar_usuario
                    if ($pathinfo === '/admin/usuarios/modificar') {
                        return array (  '_controller' => 'AdminBundle\\Controller\\UsuariosController::modificarAction',  '_route' => 'modificar_usuario',);
                    }

                    // eliminar_usuario
                    if (0 === strpos($pathinfo, '/admin/usuarios/eliminar') && preg_match('#^/admin/usuarios/eliminar/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'eliminar_usuario')), array (  '_controller' => 'AdminBundle\\Controller\\UsuariosController::eliminarAction',));
                    }

                }

                // modulos_usuario
                if (0 === strpos($pathinfo, '/admin/usuario/modulos') && preg_match('#^/admin/usuario/modulos(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'modulos_usuario')), array (  '_controller' => 'AdminBundle\\Controller\\UsuariosController::modulos_usuarioAction',  'id' => NULL,));
                }

                // permisos
                if ($pathinfo === '/admin/usuarios/permisos') {
                    return array (  '_controller' => 'AdminBundle\\Controller\\UsuariosController::permisosAction',  '_route' => 'permisos',);
                }

                // agregar_permiso
                if ($pathinfo === '/admin/usuario/permisos/agregar') {
                    return array (  '_controller' => 'AdminBundle\\Controller\\UsuariosController::permisoagregarAction',  '_route' => 'agregar_permiso',);
                }

                // editar_permiso
                if ($pathinfo === '/admin/usuarios/permisos/editar') {
                    return array (  '_controller' => 'AdminBundle\\Controller\\UsuariosController::permisoseditarAction',  '_route' => 'editar_permiso',);
                }

            }

            if (0 === strpos($pathinfo, '/admin/pro')) {
                if (0 === strpos($pathinfo, '/admin/productos')) {
                    // productos
                    if ($pathinfo === '/admin/productos') {
                        return array (  '_controller' => 'AdminBundle\\Controller\\ProductosController::productosAction',  '_route' => 'productos',);
                    }

                    // productosfecha
                    if (0 === strpos($pathinfo, '/admin/productosfecha') && preg_match('#^/admin/productosfecha(?:/(?P<inicio>[^/]++)(?:/(?P<fin>[^/]++))?)?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'productosfecha')), array (  '_controller' => 'AdminBundle\\Controller\\ProductosController::productosfechasAction',  'inicio' => NULL,  'fin' => NULL,));
                    }

                    // crear_producto
                    if ($pathinfo === '/admin/productos/crear') {
                        return array (  '_controller' => 'AdminBundle\\Controller\\ProductosController::crearAction',  '_route' => 'crear_producto',);
                    }

                    // detalle_producto
                    if ($pathinfo === '/admin/productos/detalle') {
                        return array (  '_controller' => 'AdminBundle\\Controller\\ProductosController::detalleAction',  '_route' => 'detalle_producto',);
                    }

                    if (0 === strpos($pathinfo, '/admin/productos/e')) {
                        // eliminar_producto
                        if ($pathinfo === '/admin/productos/eliminar') {
                            return array (  '_controller' => 'AdminBundle\\Controller\\ProductosController::eliminarAction',  '_route' => 'eliminar_producto',);
                        }

                        // modificar_producto
                        if ($pathinfo === '/admin/productos/editar') {
                            return array (  '_controller' => 'AdminBundle\\Controller\\ProductosController::modificarAction',  '_route' => 'modificar_producto',);
                        }

                    }

                    // clase_tipos
                    if (0 === strpos($pathinfo, '/admin/productos/clasetipos') && preg_match('#^/admin/productos/clasetipos(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'clase_tipos')), array (  '_controller' => 'AdminBundle\\Controller\\ProductosController::clasetiposAction',  'id' => NULL,));
                    }

                    if (0 === strpos($pathinfo, '/admin/productos/agregar_')) {
                        // agregar_modelo
                        if ($pathinfo === '/admin/productos/agregar_modelo') {
                            return array (  '_controller' => 'AdminBundle\\Controller\\ProductosController::agregar_modeloAction',  '_route' => 'agregar_modelo',);
                        }

                        // agregar_color
                        if ($pathinfo === '/admin/productos/agregar_color') {
                            return array (  '_controller' => 'AdminBundle\\Controller\\ProductosController::agregar_colorAction',  '_route' => 'agregar_color',);
                        }

                    }

                }

                if (0 === strpos($pathinfo, '/admin/proveedores')) {
                    // proveedores
                    if ($pathinfo === '/admin/proveedores') {
                        return array (  '_controller' => 'AdminBundle\\Controller\\ProveedoresController::proveedoresAction',  '_route' => 'proveedores',);
                    }

                    // crear_proveedor
                    if ($pathinfo === '/admin/proveedores/crear') {
                        return array (  '_controller' => 'AdminBundle\\Controller\\ProveedoresController::crearAction',  '_route' => 'crear_proveedor',);
                    }

                    // detalle_proveedor
                    if ($pathinfo === '/admin/proveedores/detalle') {
                        return array (  '_controller' => 'AdminBundle\\Controller\\ProveedoresController::detalleAction',  '_route' => 'detalle_proveedor',);
                    }

                    // modificar_proveedor
                    if ($pathinfo === '/admin/proveedores/modificar') {
                        return array (  '_controller' => 'AdminBundle\\Controller\\ProveedoresController::modificarAction',  '_route' => 'modificar_proveedor',);
                    }

                }

            }

            if (0 === strpos($pathinfo, '/admin/entradas')) {
                // entradas
                if ($pathinfo === '/admin/entradas') {
                    return array (  '_controller' => 'AdminBundle\\Controller\\entradasController::entradasAction',  '_route' => 'entradas',);
                }

                // entradasfecha
                if (0 === strpos($pathinfo, '/admin/entradasfecha') && preg_match('#^/admin/entradasfecha(?:/(?P<inicio>[^/]++)(?:/(?P<fin>[^/]++))?)?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'entradasfecha')), array (  '_controller' => 'AdminBundle\\Controller\\entradasController::entradasfechasAction',  'inicio' => NULL,  'fin' => NULL,));
                }

                // entradas_proveedor
                if (0 === strpos($pathinfo, '/admin/entradas/proveedor') && preg_match('#^/admin/entradas/proveedor(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'entradas_proveedor')), array (  '_controller' => 'AdminBundle\\Controller\\entradasController::entradas_proveedorAction',  'id' => NULL,));
                }

                // crear_entrada
                if ($pathinfo === '/admin/entradas/crear') {
                    return array (  '_controller' => 'AdminBundle\\Controller\\entradasController::crearAction',  '_route' => 'crear_entrada',);
                }

                if (0 === strpos($pathinfo, '/admin/entradas/detalle')) {
                    // detalle_entrada
                    if (preg_match('#^/admin/entradas/detalle(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'detalle_entrada')), array (  '_controller' => 'AdminBundle\\Controller\\entradasController::detalleAction',  'id' => NULL,));
                    }

                    // detalle_entrada_codigo
                    if (0 === strpos($pathinfo, '/admin/entradas/detallecodigo') && preg_match('#^/admin/entradas/detallecodigo(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'detalle_entrada_codigo')), array (  '_controller' => 'AdminBundle\\Controller\\entradasController::detallecodigoAction',  'id' => NULL,));
                    }

                }

            }

            if (0 === strpos($pathinfo, '/admin/inventario')) {
                // inventario
                if ($pathinfo === '/admin/inventario') {
                    return array (  '_controller' => 'AdminBundle\\Controller\\inventarioController::inventarioAction',  '_route' => 'inventario',);
                }

                // disponible
                if ($pathinfo === '/admin/inventario/disponible') {
                    return array (  '_controller' => 'AdminBundle\\Controller\\inventarioController::disponibleAction',  '_route' => 'disponible',);
                }

            }

            if (0 === strpos($pathinfo, '/admin/salidas')) {
                // salidas
                if ($pathinfo === '/admin/salidas') {
                    return array (  '_controller' => 'AdminBundle\\Controller\\SalidasController::salidasAction',  '_route' => 'salidas',);
                }

                // salidasfecha
                if (0 === strpos($pathinfo, '/admin/salidasfecha') && preg_match('#^/admin/salidasfecha(?:/(?P<inicio>[^/]++)(?:/(?P<fin>[^/]++))?)?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'salidasfecha')), array (  '_controller' => 'AdminBundle\\Controller\\SalidasController::salidasfechasAction',  'inicio' => NULL,  'fin' => NULL,));
                }

                // salidas_proveedor
                if (0 === strpos($pathinfo, '/admin/salidas/proveedor') && preg_match('#^/admin/salidas/proveedor(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'salidas_proveedor')), array (  '_controller' => 'AdminBundle\\Controller\\SalidasController::entradas_proveedorAction',  'id' => NULL,));
                }

                // crear_salidas
                if ($pathinfo === '/admin/salidas/crear') {
                    return array (  '_controller' => 'AdminBundle\\Controller\\SalidasController::crearAction',  '_route' => 'crear_salidas',);
                }

                // detalle_salidas
                if (0 === strpos($pathinfo, '/admin/salidas/detalle') && preg_match('#^/admin/salidas/detalle(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'detalle_salidas')), array (  '_controller' => 'AdminBundle\\Controller\\SalidasController::detalleAction',  'id' => NULL,));
                }

            }

            if (0 === strpos($pathinfo, '/admin/reportes')) {
                // reportes
                if ($pathinfo === '/admin/reportes') {
                    return array (  '_controller' => 'AdminBundle\\Controller\\ReportesController::reportesAction',  '_route' => 'reportes',);
                }

                if (0 === strpos($pathinfo, '/admin/reportes/pr')) {
                    if (0 === strpos($pathinfo, '/admin/reportes/prod')) {
                        // ppproveedor
                        if ($pathinfo === '/admin/reportes/productospreciosproveedor') {
                            return array (  '_controller' => 'AdminBundle\\Controller\\ReportesController::ppproveedorAction',  '_route' => 'ppproveedor',);
                        }

                        // detalle_pppr
                        if (0 === strpos($pathinfo, '/admin/reportes/prodprecprov') && preg_match('#^/admin/reportes/prodprecprov(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'detalle_pppr')), array (  '_controller' => 'AdminBundle\\Controller\\ReportesController::ppproveedordetalleAction',  'id' => NULL,));
                        }

                    }

                    if (0 === strpos($pathinfo, '/admin/reportes/preciop')) {
                        // ppproducto
                        if ($pathinfo === '/admin/reportes/precioporproducto') {
                            return array (  '_controller' => 'AdminBundle\\Controller\\ReportesController::ppproductoAction',  '_route' => 'ppproducto',);
                        }

                        // detalle_ppproducto
                        if (0 === strpos($pathinfo, '/admin/reportes/precioproducto') && preg_match('#^/admin/reportes/precioproducto(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'detalle_ppproducto')), array (  '_controller' => 'AdminBundle\\Controller\\ReportesController::ppproductodetalleAction',  'id' => NULL,));
                        }

                    }

                    // pui
                    if ($pathinfo === '/admin/reportes/productosunidadesingresadas') {
                        return array (  '_controller' => 'AdminBundle\\Controller\\ReportesController::puiAction',  '_route' => 'pui',);
                    }

                }

            }

        }

        // login_homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'login_homepage');
            }

            return array (  '_controller' => 'LoginBundle\\Controller\\SecurityController::loginAction',  '_route' => 'login_homepage',);
        }

        if (0 === strpos($pathinfo, '/log')) {
            // login
            if ($pathinfo === '/login') {
                return array (  '_controller' => 'LoginBundle\\Controller\\SecurityController::loginAction',  '_route' => 'login',);
            }

            // logout
            if ($pathinfo === '/logout') {
                return array('_route' => 'logout');
            }

        }

        // homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'homepage');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
