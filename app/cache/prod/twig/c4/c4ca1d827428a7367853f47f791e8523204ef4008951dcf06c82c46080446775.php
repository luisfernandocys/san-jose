<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_8c4d34d1e196e7f7535f7e8763c169d8c43fa541d7482e43c546989f9bb49783 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7886a33d6782e96e9a8011cc94c1979051bd69dff6b8ec9650e61007c185cd57 = $this->env->getExtension("native_profiler");
        $__internal_7886a33d6782e96e9a8011cc94c1979051bd69dff6b8ec9650e61007c185cd57->enter($__internal_7886a33d6782e96e9a8011cc94c1979051bd69dff6b8ec9650e61007c185cd57_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7886a33d6782e96e9a8011cc94c1979051bd69dff6b8ec9650e61007c185cd57->leave($__internal_7886a33d6782e96e9a8011cc94c1979051bd69dff6b8ec9650e61007c185cd57_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_525f66d46f261a3f760c67add2f5e775b49dc8ab885ee8798374529bb93b6e41 = $this->env->getExtension("native_profiler");
        $__internal_525f66d46f261a3f760c67add2f5e775b49dc8ab885ee8798374529bb93b6e41->enter($__internal_525f66d46f261a3f760c67add2f5e775b49dc8ab885ee8798374529bb93b6e41_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_525f66d46f261a3f760c67add2f5e775b49dc8ab885ee8798374529bb93b6e41->leave($__internal_525f66d46f261a3f760c67add2f5e775b49dc8ab885ee8798374529bb93b6e41_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_e28b577cf9188bf72c9b74c048b1aad3182159396c136a5e170eb44ef74b344a = $this->env->getExtension("native_profiler");
        $__internal_e28b577cf9188bf72c9b74c048b1aad3182159396c136a5e170eb44ef74b344a->enter($__internal_e28b577cf9188bf72c9b74c048b1aad3182159396c136a5e170eb44ef74b344a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_e28b577cf9188bf72c9b74c048b1aad3182159396c136a5e170eb44ef74b344a->leave($__internal_e28b577cf9188bf72c9b74c048b1aad3182159396c136a5e170eb44ef74b344a_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_b3d603d6f03e2a091505ca64a0652c1cbc9fb55ff0f4fe78c5a4bd15a01a20b1 = $this->env->getExtension("native_profiler");
        $__internal_b3d603d6f03e2a091505ca64a0652c1cbc9fb55ff0f4fe78c5a4bd15a01a20b1->enter($__internal_b3d603d6f03e2a091505ca64a0652c1cbc9fb55ff0f4fe78c5a4bd15a01a20b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_b3d603d6f03e2a091505ca64a0652c1cbc9fb55ff0f4fe78c5a4bd15a01a20b1->leave($__internal_b3d603d6f03e2a091505ca64a0652c1cbc9fb55ff0f4fe78c5a4bd15a01a20b1_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
