<?php

/* base.html.twig */
class __TwigTemplate_605eb3c9842de05ea4a678253d24b078e9e42bf8d7d1abf57afd21a4229cafda extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'header' => array($this, 'block_header'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b57905e701f717354a911912289b06309b550e2656a288cb9fbc304733074b66 = $this->env->getExtension("native_profiler");
        $__internal_b57905e701f717354a911912289b06309b550e2656a288cb9fbc304733074b66->enter($__internal_b57905e701f717354a911912289b06309b550e2656a288cb9fbc304733074b66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />

        <meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;\">
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <script src=\"//code.jquery.com/jquery-1.12.3.js\"></script>
        <script src=\"//code.jquery.com/jquery-1.12.3.min.js\"></script>
        <script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.js\"></script>
        <script src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bootstrap/js/bootstrap.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/generales.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/admin/js/menu.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/jquery.validate.min.js"), "html", null, true);
        echo "\"></script>
        <link href=";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bootstrap/css/bootstrap.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\"/>
        <link href=";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/generales.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\"/>
        <link href=";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/admin/css/menu.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\"/>

        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css\">
        <link href='https://fonts.googleapis.com/css?family=Roboto:100,500,400' rel='stylesheet' type='text/css'>

        <script type=\"text/javascript\" src=\"https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js\"></script>
        <script type=\"text/javascript\" src=\"https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js\"></script>
        <link href=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\"/>
        <link href=\"https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\"/>
        <link rel=\"stylesheet\" href=\"//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css\">
        

        



        <style>

            h1 {
                margin-bottom: 20px;
            }
            a {
                color: #fbfbfb;
                text-decoration: none;
            }
            .control-group
            {
                padding: 5px 0px;
            }
        </style>

        ";
        // line 48
        $this->displayBlock('header', $context, $blocks);
        // line 51
        echo "    ";
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 52
        echo "    <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
</head>
<body>

    ";
        // line 56
        $this->loadTemplate("AdminBundle:menu:menu.html.twig", "base.html.twig", 56)->display($context);
        // line 57
        echo "
    ";
        // line 58
        $this->displayBlock('body', $context, $blocks);
        // line 61
        echo "
";
        // line 62
        $this->displayBlock('javascripts', $context, $blocks);
        // line 63
        echo "
<div id=\"loading\">
    <img src=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("imagenes/loading.gif"), "html", null, true);
        echo "\" alt=\"loading\" style=\"width: 120px; position: absolute; top: 50%; left: 50%; margin-top: -60px; margin-left: -80px;\"/>
</div>
</body>
</html>
";
        
        $__internal_b57905e701f717354a911912289b06309b550e2656a288cb9fbc304733074b66->leave($__internal_b57905e701f717354a911912289b06309b550e2656a288cb9fbc304733074b66_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_affb61cf1287d5d3b44d594a3ecf137fc5c0d7df1fd2d6b5dfb60d14ebdc48bd = $this->env->getExtension("native_profiler");
        $__internal_affb61cf1287d5d3b44d594a3ecf137fc5c0d7df1fd2d6b5dfb60d14ebdc48bd->enter($__internal_affb61cf1287d5d3b44d594a3ecf137fc5c0d7df1fd2d6b5dfb60d14ebdc48bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_affb61cf1287d5d3b44d594a3ecf137fc5c0d7df1fd2d6b5dfb60d14ebdc48bd->leave($__internal_affb61cf1287d5d3b44d594a3ecf137fc5c0d7df1fd2d6b5dfb60d14ebdc48bd_prof);

    }

    // line 48
    public function block_header($context, array $blocks = array())
    {
        $__internal_71d8124759a2b3c53ab731fe7ebf9ec3f6751d877903a655550c7cea16be6f51 = $this->env->getExtension("native_profiler");
        $__internal_71d8124759a2b3c53ab731fe7ebf9ec3f6751d877903a655550c7cea16be6f51->enter($__internal_71d8124759a2b3c53ab731fe7ebf9ec3f6751d877903a655550c7cea16be6f51_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 49
        echo "
        ";
        
        $__internal_71d8124759a2b3c53ab731fe7ebf9ec3f6751d877903a655550c7cea16be6f51->leave($__internal_71d8124759a2b3c53ab731fe7ebf9ec3f6751d877903a655550c7cea16be6f51_prof);

    }

    // line 51
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_9db6b76da003891e4ab54d30418edbe8bcf2d9b60512b0966ddd180e46b7b791 = $this->env->getExtension("native_profiler");
        $__internal_9db6b76da003891e4ab54d30418edbe8bcf2d9b60512b0966ddd180e46b7b791->enter($__internal_9db6b76da003891e4ab54d30418edbe8bcf2d9b60512b0966ddd180e46b7b791_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_9db6b76da003891e4ab54d30418edbe8bcf2d9b60512b0966ddd180e46b7b791->leave($__internal_9db6b76da003891e4ab54d30418edbe8bcf2d9b60512b0966ddd180e46b7b791_prof);

    }

    // line 58
    public function block_body($context, array $blocks = array())
    {
        $__internal_9362dd13e41b6a22610d086037bd116b355d8a959c65ca05eac4198f051fc5e9 = $this->env->getExtension("native_profiler");
        $__internal_9362dd13e41b6a22610d086037bd116b355d8a959c65ca05eac4198f051fc5e9->enter($__internal_9362dd13e41b6a22610d086037bd116b355d8a959c65ca05eac4198f051fc5e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 59
        echo "
    ";
        
        $__internal_9362dd13e41b6a22610d086037bd116b355d8a959c65ca05eac4198f051fc5e9->leave($__internal_9362dd13e41b6a22610d086037bd116b355d8a959c65ca05eac4198f051fc5e9_prof);

    }

    // line 62
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_51eed3793aaa7d20df6e1fa266d4fe6f470ba489c669e6e5caca04e814a26f53 = $this->env->getExtension("native_profiler");
        $__internal_51eed3793aaa7d20df6e1fa266d4fe6f470ba489c669e6e5caca04e814a26f53->enter($__internal_51eed3793aaa7d20df6e1fa266d4fe6f470ba489c669e6e5caca04e814a26f53_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_51eed3793aaa7d20df6e1fa266d4fe6f470ba489c669e6e5caca04e814a26f53->leave($__internal_51eed3793aaa7d20df6e1fa266d4fe6f470ba489c669e6e5caca04e814a26f53_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  191 => 62,  183 => 59,  177 => 58,  166 => 51,  158 => 49,  152 => 48,  141 => 7,  129 => 65,  125 => 63,  123 => 62,  120 => 61,  118 => 58,  115 => 57,  113 => 56,  105 => 52,  102 => 51,  100 => 48,  66 => 17,  62 => 16,  58 => 15,  54 => 14,  50 => 13,  46 => 12,  42 => 11,  35 => 7,  27 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/* */
/*         <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">*/
/*         <title>{% block title %}{% endblock %}</title>*/
/*         <script src="//code.jquery.com/jquery-1.12.3.js"></script>*/
/*         <script src="//code.jquery.com/jquery-1.12.3.min.js"></script>*/
/*         <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>*/
/*         <script src="{{ asset('bootstrap/js/bootstrap.js') }}"></script>*/
/*         <script src="{{ asset('js/generales.js') }}"></script>*/
/*         <script src="{{ asset('bundles/admin/js/menu.js') }}"></script>*/
/*         <script src="{{ asset('js/jquery.validate.min.js') }}"></script>*/
/*         <link href={{ asset('bootstrap/css/bootstrap.css') }} rel="stylesheet" type="text/css"/>*/
/*         <link href={{ asset('css/generales.css') }} rel="stylesheet" type="text/css"/>*/
/*         <link href={{ asset('bundles/admin/css/menu.css') }} rel="stylesheet" type="text/css"/>*/
/* */
/*         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">*/
/*         <link href='https://fonts.googleapis.com/css?family=Roboto:100,500,400' rel='stylesheet' type='text/css'>*/
/* */
/*         <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>*/
/*         <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>*/
/*         <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>*/
/*         <link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>*/
/*         <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">*/
/*         */
/* */
/*         */
/* */
/* */
/* */
/*         <style>*/
/* */
/*             h1 {*/
/*                 margin-bottom: 20px;*/
/*             }*/
/*             a {*/
/*                 color: #fbfbfb;*/
/*                 text-decoration: none;*/
/*             }*/
/*             .control-group*/
/*             {*/
/*                 padding: 5px 0px;*/
/*             }*/
/*         </style>*/
/* */
/*         {% block header %}*/
/* */
/*         {% endblock %}*/
/*     {% block stylesheets %}{% endblock %}*/
/*     <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/* </head>*/
/* <body>*/
/* */
/*     {% include 'AdminBundle:menu:menu.html.twig' %}*/
/* */
/*     {% block body %}*/
/* */
/*     {% endblock %}*/
/* */
/* {% block javascripts %}{% endblock %}*/
/* */
/* <div id="loading">*/
/*     <img src="{{asset('imagenes/loading.gif')}}" alt="loading" style="width: 120px; position: absolute; top: 50%; left: 50%; margin-top: -60px; margin-left: -80px;"/>*/
/* </div>*/
/* </body>*/
/* </html>*/
/* */
