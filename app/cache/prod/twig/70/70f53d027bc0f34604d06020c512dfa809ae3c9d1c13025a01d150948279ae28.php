<?php

/* AdminBundle:inicio:index.html.twig */
class __TwigTemplate_1bee3e9b13a472df0a00d4b9dd6529aea004d4b8070af23d4ed2ef7ca2ceaddf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_91f8c76f1761dd70405038985ea2be6fba0e7cdc920476574c26b650a30ee74a = $this->env->getExtension("native_profiler");
        $__internal_91f8c76f1761dd70405038985ea2be6fba0e7cdc920476574c26b650a30ee74a->enter($__internal_91f8c76f1761dd70405038985ea2be6fba0e7cdc920476574c26b650a30ee74a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AdminBundle:inicio:index.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />

        <meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;\">
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <script src=\"//code.jquery.com/jquery-1.11.2.min.js\"></script>
        <script src=\"//code.jquery.com/jquery-migrate-1.2.1.min.js\"></script>
        <script src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bootstrap/js/bootstrap.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/generales.js"), "html", null, true);
        echo "\"></script>

        <link href=";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bootstrap/css/bootstrap.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\"/>
        <link href=";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/inicio.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\"/>
        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css\">
        <link href='https://fonts.googleapis.com/css?family=Roboto:100,500,400' rel='stylesheet' type='text/css'>


        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        <div class=\"container-fluid\" id=\"menu\">
            <img src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("imagenes/logo.png"), "html", null, true);
        echo "\" class=\"img-responsive\" />
        </div>
        <div class=\"container\">
            <div class=\"row\">

                <h1 class=\"text-center header\">Menú principal</h1>

            </div>
        </div>
        <div class=\"container main\">
            <div class=\"row centrado\">
                <div class=\"col-md-2\">
                    <a href=\"";
        // line 35
        echo $this->env->getExtension('routing')->getPath("usuarios");
        echo "\" class=\"menu\"><i class=\"fa fa-users\"></i></a>
                    <br><span> Usuarios</span>
                </div>
                <div class=\"col-md-2\">
                    <a href=\"";
        // line 39
        echo $this->env->getExtension('routing')->getPath("productos");
        echo "\" class=\"menu\"><i class=\"fa fa-cart-plus\"></i></a>
                    <br><span> Productos</span>
                </div>

                <div class=\"col-md-2\">
                    <a href=\"";
        // line 44
        echo $this->env->getExtension('routing')->getPath("proveedores");
        echo "\" class=\"menu\"><i class=\"fa fa-product-hunt\"></i></a>
                    <br><span> Proveedores</span>
                </div>
                <div class=\"col-md-2\">
                    <a href=\"";
        // line 48
        echo $this->env->getExtension('routing')->getPath("inventario");
        echo "\" class=\"menu\"> <i class=\"fa fa-bar-chart\"></i></a>
                    <br><span> Inventario</span>
                </div>
                <div class=\"col-md-2\">
                    <a href=\"";
        // line 52
        echo $this->env->getExtension('routing')->getPath("entradas");
        echo "\" class=\"menu\"><i class=\"fa fa-plus\"></i></a>
                    <br><span> Entradas</span>
                </div>
                <div class=\"col-md-2\">
                    <a href=\"";
        // line 56
        echo $this->env->getExtension('routing')->getPath("salidas");
        echo "\" class=\"menu\"><i class=\"fa fa-minus\"></i></a>
                    <br><span> Salidas</span>
                </div>

            </div>
            <div class=\"row centrado\">
                <div class=\"col-md-2\">
                    <a href=\"";
        // line 63
        echo $this->env->getExtension('routing')->getPath("reportes");
        echo "\" class=\"menu\"><i class=\"fa fa-pie-chart\"></i></a>
                    <br><span> Reportes</span>
                </div>

            </div>
        </div>



        <div id=\"loading\">
            <img src=\"";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("imagenes/loading.gif"), "html", null, true);
        echo "\"  alt=\"loading\" style=\"width: 120px; position: absolute; top: 50%; left: 50%; margin-top: -60px; margin-left: -80px;\"/>
        </div>
    </body>
</html>
";
        
        $__internal_91f8c76f1761dd70405038985ea2be6fba0e7cdc920476574c26b650a30ee74a->leave($__internal_91f8c76f1761dd70405038985ea2be6fba0e7cdc920476574c26b650a30ee74a_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_fa44de8375c398564b05fc365cb3dcf116f164214145c1f84877216f79deef52 = $this->env->getExtension("native_profiler");
        $__internal_fa44de8375c398564b05fc365cb3dcf116f164214145c1f84877216f79deef52->enter($__internal_fa44de8375c398564b05fc365cb3dcf116f164214145c1f84877216f79deef52_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Inicio";
        
        $__internal_fa44de8375c398564b05fc365cb3dcf116f164214145c1f84877216f79deef52->leave($__internal_fa44de8375c398564b05fc365cb3dcf116f164214145c1f84877216f79deef52_prof);

    }

    public function getTemplateName()
    {
        return "AdminBundle:inicio:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 7,  139 => 73,  126 => 63,  116 => 56,  109 => 52,  102 => 48,  95 => 44,  87 => 39,  80 => 35,  65 => 23,  58 => 19,  50 => 14,  46 => 13,  41 => 11,  37 => 10,  31 => 7,  23 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/* */
/*         <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">*/
/*         <title>{% block title %}Inicio{% endblock %}</title>*/
/*         <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>*/
/*         <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>*/
/*         <script src="{{ asset('bootstrap/js/bootstrap.js') }}"></script>*/
/*         <script src="{{ asset('js/generales.js') }}"></script>*/
/* */
/*         <link href={{ asset('bootstrap/css/bootstrap.css') }} rel="stylesheet" type="text/css"/>*/
/*         <link href={{ asset('css/inicio.css') }} rel="stylesheet" type="text/css"/>*/
/*         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">*/
/*         <link href='https://fonts.googleapis.com/css?family=Roboto:100,500,400' rel='stylesheet' type='text/css'>*/
/* */
/* */
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*         <div class="container-fluid" id="menu">*/
/*             <img src="{{asset('imagenes/logo.png')}}" class="img-responsive" />*/
/*         </div>*/
/*         <div class="container">*/
/*             <div class="row">*/
/* */
/*                 <h1 class="text-center header">Menú principal</h1>*/
/* */
/*             </div>*/
/*         </div>*/
/*         <div class="container main">*/
/*             <div class="row centrado">*/
/*                 <div class="col-md-2">*/
/*                     <a href="{{path('usuarios')}}" class="menu"><i class="fa fa-users"></i></a>*/
/*                     <br><span> Usuarios</span>*/
/*                 </div>*/
/*                 <div class="col-md-2">*/
/*                     <a href="{{path('productos')}}" class="menu"><i class="fa fa-cart-plus"></i></a>*/
/*                     <br><span> Productos</span>*/
/*                 </div>*/
/* */
/*                 <div class="col-md-2">*/
/*                     <a href="{{path('proveedores')}}" class="menu"><i class="fa fa-product-hunt"></i></a>*/
/*                     <br><span> Proveedores</span>*/
/*                 </div>*/
/*                 <div class="col-md-2">*/
/*                     <a href="{{path('inventario')}}" class="menu"> <i class="fa fa-bar-chart"></i></a>*/
/*                     <br><span> Inventario</span>*/
/*                 </div>*/
/*                 <div class="col-md-2">*/
/*                     <a href="{{path('entradas')}}" class="menu"><i class="fa fa-plus"></i></a>*/
/*                     <br><span> Entradas</span>*/
/*                 </div>*/
/*                 <div class="col-md-2">*/
/*                     <a href="{{path('salidas')}}" class="menu"><i class="fa fa-minus"></i></a>*/
/*                     <br><span> Salidas</span>*/
/*                 </div>*/
/* */
/*             </div>*/
/*             <div class="row centrado">*/
/*                 <div class="col-md-2">*/
/*                     <a href="{{path('reportes')}}" class="menu"><i class="fa fa-pie-chart"></i></a>*/
/*                     <br><span> Reportes</span>*/
/*                 </div>*/
/* */
/*             </div>*/
/*         </div>*/
/* */
/* */
/* */
/*         <div id="loading">*/
/*             <img src="{{asset('imagenes/loading.gif')}}"  alt="loading" style="width: 120px; position: absolute; top: 50%; left: 50%; margin-top: -60px; margin-left: -80px;"/>*/
/*         </div>*/
/*     </body>*/
/* </html>*/
/* */
