<?php

/* AdminBundle:menu:menu.html.twig */
class __TwigTemplate_814825f200b828cc49094465bf13278e3cf896f15bf34f49b72dab948467b45b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_60b55fbb0940814f6717c49c3413df616909ca47a234e0e7643c6d81105b8ee3 = $this->env->getExtension("native_profiler");
        $__internal_60b55fbb0940814f6717c49c3413df616909ca47a234e0e7643c6d81105b8ee3->enter($__internal_60b55fbb0940814f6717c49c3413df616909ca47a234e0e7643c6d81105b8ee3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AdminBundle:menu:menu.html.twig"));

        // line 2
        echo "<div id=\"container\">
    <div class=\"navT\">
        <div class=\"icon\"></div>
        <img src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("imagenes/logo.png"), "html", null, true);
        echo "\" class=\"img-responsive pull-right\"  style=\"height: 100%; padding:5px;\" />
    </div>

    <div id=\"menu\" class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-3\">
                <a href=\"";
        // line 11
        echo $this->env->getExtension('routing')->getPath("admin_homepage");
        echo "\" class=\"space\"> <i class=\"fa fa-align-justify\"></i><span> Inicio</span></a>

            </div>
            <div class=\"col-md-3\">
                <a href=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("usuarios");
        echo "\" class=\"space\"><i class=\"fa fa-users\"></i> <span> Usuarios</span></a>

            </div>
            <div class=\"col-md-3\">
                <a href=\"";
        // line 19
        echo $this->env->getExtension('routing')->getPath("proveedores");
        echo "\" class=\"space\"><i class=\"fa fa-product-hunt\"></i><span> Proveedores</span></a>

            </div>
            <div class=\"col-md-3\">
                <a href=\"";
        // line 23
        echo $this->env->getExtension('routing')->getPath("productos");
        echo "\" class=\"space\"><i class=\"fa fa-cart-plus\"></i><span> Productos</span></a>

            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-3\">
                <a href=\"";
        // line 29
        echo $this->env->getExtension('routing')->getPath("inventario");
        echo "\" class=\"space\"> <i class=\"fa fa-bar-chart\"></i><span> Inventario</span></a>

            </div>
            <div class=\"col-md-3\">
                <a href=\"";
        // line 33
        echo $this->env->getExtension('routing')->getPath("entradas");
        echo "\" class=\"space\"> <i class=\"fa fa-plus\"></i><span> Entradas</span></a>

            </div>

            <div class=\"col-md-3\">
                <a href=\"";
        // line 38
        echo $this->env->getExtension('routing')->getPath("salidas");
        echo "\" class=\"space\"> <i class=\"fa fa-minus\"></i><span> Salidas</span></a>

            </div>
            <div class=\"col-md-3\">
                <a href=\"";
        // line 42
        echo $this->env->getExtension('routing')->getPath("reportes");
        echo "\" class=\"space\"> <i class=\"fa fa-pie-chart\"></i><span> Reportes</span></a>

            </div>


            <div class=\"row\">


            </div>
            <div class=\"col-md-3\">
                <a href=\"";
        // line 52
        echo $this->env->getExtension('routing')->getPath("logout");
        echo "\" class=\"space\"> <i class=\"fa fa-close\"></i><span> Cerrar Sesión</span></a>

            </div>


        </div>
    </div>

</div>";
        
        $__internal_60b55fbb0940814f6717c49c3413df616909ca47a234e0e7643c6d81105b8ee3->leave($__internal_60b55fbb0940814f6717c49c3413df616909ca47a234e0e7643c6d81105b8ee3_prof);

    }

    public function getTemplateName()
    {
        return "AdminBundle:menu:menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 52,  88 => 42,  81 => 38,  73 => 33,  66 => 29,  57 => 23,  50 => 19,  43 => 15,  36 => 11,  27 => 5,  22 => 2,);
    }
}
/* {# empty Twig template #}*/
/* <div id="container">*/
/*     <div class="navT">*/
/*         <div class="icon"></div>*/
/*         <img src="{{asset('imagenes/logo.png')}}" class="img-responsive pull-right"  style="height: 100%; padding:5px;" />*/
/*     </div>*/
/* */
/*     <div id="menu" class="container">*/
/*         <div class="row">*/
/*             <div class="col-md-3">*/
/*                 <a href="{{path('admin_homepage')}}" class="space"> <i class="fa fa-align-justify"></i><span> Inicio</span></a>*/
/* */
/*             </div>*/
/*             <div class="col-md-3">*/
/*                 <a href="{{path('usuarios')}}" class="space"><i class="fa fa-users"></i> <span> Usuarios</span></a>*/
/* */
/*             </div>*/
/*             <div class="col-md-3">*/
/*                 <a href="{{path('proveedores')}}" class="space"><i class="fa fa-product-hunt"></i><span> Proveedores</span></a>*/
/* */
/*             </div>*/
/*             <div class="col-md-3">*/
/*                 <a href="{{path('productos')}}" class="space"><i class="fa fa-cart-plus"></i><span> Productos</span></a>*/
/* */
/*             </div>*/
/*         </div>*/
/*         <div class="row">*/
/*             <div class="col-md-3">*/
/*                 <a href="{{path('inventario')}}" class="space"> <i class="fa fa-bar-chart"></i><span> Inventario</span></a>*/
/* */
/*             </div>*/
/*             <div class="col-md-3">*/
/*                 <a href="{{path('entradas')}}" class="space"> <i class="fa fa-plus"></i><span> Entradas</span></a>*/
/* */
/*             </div>*/
/* */
/*             <div class="col-md-3">*/
/*                 <a href="{{path('salidas')}}" class="space"> <i class="fa fa-minus"></i><span> Salidas</span></a>*/
/* */
/*             </div>*/
/*             <div class="col-md-3">*/
/*                 <a href="{{path('reportes')}}" class="space"> <i class="fa fa-pie-chart"></i><span> Reportes</span></a>*/
/* */
/*             </div>*/
/* */
/* */
/*             <div class="row">*/
/* */
/* */
/*             </div>*/
/*             <div class="col-md-3">*/
/*                 <a href="{{path('logout')}}" class="space"> <i class="fa fa-close"></i><span> Cerrar Sesión</span></a>*/
/* */
/*             </div>*/
/* */
/* */
/*         </div>*/
/*     </div>*/
/* */
/* </div>*/
