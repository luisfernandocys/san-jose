<?php

/* AdminBundle:entradas:detalleentradas.html.twig */
class __TwigTemplate_8fd9f0e8c84de269ceee996d2d4aedd2be1b2c80e43b701fa6d3f8de5345b338 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_12bdd3ce3cad65fd4b948a91ab9cf09bdfc49696faf4a93c8858804560afbd97 = $this->env->getExtension("native_profiler");
        $__internal_12bdd3ce3cad65fd4b948a91ab9cf09bdfc49696faf4a93c8858804560afbd97->enter($__internal_12bdd3ce3cad65fd4b948a91ab9cf09bdfc49696faf4a93c8858804560afbd97_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AdminBundle:entradas:detalleentradas.html.twig"));

        // line 2
        echo "<div id=\"detalleentradas\" class=\"modal fade in\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"MyModalLabel\" aria-hidden=\"false\">
    <div class=\"modal-dialog text-left\">
        <div class=\"modal-content\">
            <div class=\"modal-header\"><!--#9adc09-->
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>
                <h4 class=\"modal-title\" id=\"exampleModalLabel\">Detalle Entrada</h4>
            </div>
            <form action=\"\" id=\"modificar_detalle\" method=\"POST\">
                <div class=\"modal-body\">
                    
                    <div class=\"row\">
<div class=\"control-group\">
                                <label for=\"codigos\" class=\"control-label\">\t
                                    Códigos Generados </label>
                                
                            </div>
                        <div class=\"col-md-12\" id=\"codigos\">
                            
                        </div>
                    </div>

                    <div class=\"row\">

                        <div class=\"col-md-12\">
                            <div class=\"control-group\">

                                

                                    <!--
                                    ";
        // line 31
        if (twig_in_filter("EDICION", (isset($context["ppm"]) ? $context["ppm"] : $this->getContext($context, "ppm")))) {
            // line 32
            echo "                                        <input type=\"submit\" value=\"modificar\" class=\" btn btn-lg col-md-12 text-center\" id=\"btn_modificar\"  data-path=\"";
            echo $this->env->getExtension('routing')->getPath("modificar_producto");
            echo "\"    />            
                                        <button class=\"btn col-md-12  btn-lg eliminarproducto\" data-id=\"\" data-path=\"";
            // line 33
            echo $this->env->getExtension('routing')->getPath("eliminar_producto");
            echo "\" id=\"eliminarproducto\" >Eliminar Producto</button>
                                    ";
        } else {
            // line 35
            echo "                                        <input type=\"submit\" class=\" btn btn-lg col-md-12 text-center boton_disable\" id=\"btn_modificar\"  data-path=\"";
            echo $this->env->getExtension('routing')->getPath("modificar_producto");
            echo "\"    value=\"Modificar\"/>
                                        <button class=\"col-md-12  btn btn-lg boton_disable\" data-id=\"\" data-path=\"";
            // line 36
            echo $this->env->getExtension('routing')->getPath("eliminar_producto");
            echo "\" id=\"eliminarproducto\" >Eliminar Producto</button>
                                    ";
        }
        // line 37
        echo "-->
                                
                            </div>
                        </div>
                    </div>
                    <input name=\"identradamodificar\" class=\"form-control\" placeholder=\"\" type=\"hidden\"  value=\"\" id=\"identradamodificar\" style=\"width: 100%;\" >


                </div>


            </form>
        </div>




    </div>

</div>



";
        
        $__internal_12bdd3ce3cad65fd4b948a91ab9cf09bdfc49696faf4a93c8858804560afbd97->leave($__internal_12bdd3ce3cad65fd4b948a91ab9cf09bdfc49696faf4a93c8858804560afbd97_prof);

    }

    public function getTemplateName()
    {
        return "AdminBundle:entradas:detalleentradas.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 37,  70 => 36,  65 => 35,  60 => 33,  55 => 32,  53 => 31,  22 => 2,);
    }
}
/* {# empty Twig template #}*/
/* <div id="detalleentradas" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="MyModalLabel" aria-hidden="false">*/
/*     <div class="modal-dialog text-left">*/
/*         <div class="modal-content">*/
/*             <div class="modal-header"><!--#9adc09-->*/
/*                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>*/
/*                 <h4 class="modal-title" id="exampleModalLabel">Detalle Entrada</h4>*/
/*             </div>*/
/*             <form action="" id="modificar_detalle" method="POST">*/
/*                 <div class="modal-body">*/
/*                     */
/*                     <div class="row">*/
/* <div class="control-group">*/
/*                                 <label for="codigos" class="control-label">	*/
/*                                     Códigos Generados </label>*/
/*                                 */
/*                             </div>*/
/*                         <div class="col-md-12" id="codigos">*/
/*                             */
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <div class="row">*/
/* */
/*                         <div class="col-md-12">*/
/*                             <div class="control-group">*/
/* */
/*                                 */
/* */
/*                                     <!--*/
/*                                     {% if "EDICION" in ppm %}*/
/*                                         <input type="submit" value="modificar" class=" btn btn-lg col-md-12 text-center" id="btn_modificar"  data-path="{{path('modificar_producto')}}"    />            */
/*                                         <button class="btn col-md-12  btn-lg eliminarproducto" data-id="" data-path="{{path('eliminar_producto')}}" id="eliminarproducto" >Eliminar Producto</button>*/
/*                                     {% else %}*/
/*                                         <input type="submit" class=" btn btn-lg col-md-12 text-center boton_disable" id="btn_modificar"  data-path="{{path('modificar_producto')}}"    value="Modificar"/>*/
/*                                         <button class="col-md-12  btn btn-lg boton_disable" data-id="" data-path="{{path('eliminar_producto')}}" id="eliminarproducto" >Eliminar Producto</button>*/
/*                                     {% endif %}-->*/
/*                                 */
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <input name="identradamodificar" class="form-control" placeholder="" type="hidden"  value="" id="identradamodificar" style="width: 100%;" >*/
/* */
/* */
/*                 </div>*/
/* */
/* */
/*             </form>*/
/*         </div>*/
/* */
/* */
/* */
/* */
/*     </div>*/
/* */
/* </div>*/
/* */
/* */
/* */
/* */
