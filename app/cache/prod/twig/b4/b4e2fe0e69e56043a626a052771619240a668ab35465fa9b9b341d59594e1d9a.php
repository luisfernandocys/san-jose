<?php

/* AdminBundle:Productos:detalleproductos.html.twig */
class __TwigTemplate_42eba3f977f9c8515af8d461271d17a192d997fccd7406c8476f96c20a67f4f1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_73d070df908031f23b345c0a3096c86e02b3c4c6a4a6efb3aae4bf566cd9a052 = $this->env->getExtension("native_profiler");
        $__internal_73d070df908031f23b345c0a3096c86e02b3c4c6a4a6efb3aae4bf566cd9a052->enter($__internal_73d070df908031f23b345c0a3096c86e02b3c4c6a4a6efb3aae4bf566cd9a052_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AdminBundle:Productos:detalleproductos.html.twig"));

        // line 2
        echo "<div id=\"detalleproductos\" class=\"modal fade in\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"MyModalLabel\" aria-hidden=\"false\">
    <div class=\"modal-dialog text-left\">
        <div class=\"modal-content\">
            <div class=\"modal-header\"><!--#9adc09-->
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>
                <h4 class=\"modal-title\" id=\"exampleModalLabel\">Detalle Producto</h4>
            </div>
            <form action=\"\" id=\"modificar_producto\" method=\"POST\">
                <div class=\"modal-body\">
                    <div class=\"row\">
                        <div class=\"col-md-6\">
                            <img src=\"\" id=\"foto_producto\" class=\"img-responsive\">
                        </div>
                        <div class=\"col-md-6\">
                            <div class=\"col-md-12\">

                                <div class=\"control-group\">
                                    <label for=\"det_fecha\" class=\"control-label\">Fecha
                                    </label>
                                    <input type=\"text\" class=\"form-control\" id=\"det_fecha\" name=\"det_fecha\" disabled placeholder=\"\">
                                </div>

                            </div>

                            <div class=\"col-md-12\">

                                <div class=\"control-group\">
                                    <label for=\"det_producto\" class=\"control-label\">Producto</label>
                                    <input type=\"text\" class=\"form-control\" id=\"det_producto\" name=\"det_producto\" placeholder=\"\">
                                </div>

                            </div>

                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-12\">

                            <div class=\"control-group\">
                                <label for=\"det_folio\" class=\"control-label\">Folio
                                </label>
                                <input type=\"text\" class=\"form-control\" id=\"det_folio\" name=\"det_folio\" placeholder=\"\" disabled>
                            </div>

                        </div>
                    </div>
                    <!--<div class=\"row\">
                        <div class=\"col-md-6 col-sm-6 col-xs-6 \"><label for=\"det_proveedor\" class=\"control-label\">Proveedor/es</label></div>
                        <div class=\"col-md-2 col-sm-2 col-xs-2\"><label for=\"det_precio1\" class=\"control-label\">Precio 1</label></div>
                        <div class=\"col-md-2 col-sm-2 col-xs-2\"><label for=\"det_precio2\" class=\"control-label\">Precio 2</label></div>
                        <div class=\"col-md-2 col-sm-2 col-xs-2\"><label for=\"det_precio3\" class=\"control-label\">Precio 3</label></div>

                    </div>-->
                    




                    <!--<div class=\"row\">
                        <div class=\"col-md-5\" >
                            <div class=\"control-group\">
                                <label for=\"det_clase\" class=\"control-label\">Clase del Producto</label>

                                <select name=\"det_clase\" id=\"det_clase\" class=\"form-control\" data-width=\"100%\" data-path=\"";
        // line 65
        echo $this->env->getExtension('routing')->getPath("clase_tipos");
        echo "\">
                                    <option value=\"0\" >Elige la clase del Producto </option>
                                    ";
        // line 67
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["clase"]);
        foreach ($context['_seq'] as $context["_key"] => $context["clase"]) {
            // line 68
            echo "                                        <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["clase"], "id", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["clase"], "nombre", array()), "html", null, true);
            echo " </option>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['clase'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 70
        echo "                                </select>
                            </div>
                        </div>
                        <div class=\"col-md-1\"><img src=\"";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("imagenes/loadingddl.gif"), "html", null, true);
        echo "\" id=\"loadingddl_det\" class=\"\" style=\"width:20px;height: 20px; display: none;\" /></div>

                        <div class=\"col-md-6\"  id=\"div_tipoproducto_det\" style=\"display: none;\" >
                            <label for=\"det_tipo\" class=\"control-label\">Tipo del Producto</label>
                            <div class=\"control-group\">

                                <select name=\"det_tipo\" id=\"det_tipo\" class=\"form-control\" data-width=\"100%\" >
                                    <option value=\"0\" >Elige el tipo de Producto </option>

                                </select>


                            </div>
                        </div>
                    </div>-->
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <div class=\"control-group\">
                                <label for=\"det_marca\" class=\"control-label\">Marca</label>
                                <input type=\"text\" class=\"form-control\" id=\"det_marca\" name=\"det_marca\" placeholder=\"\">
                            </div>
                        </div>
                        <div class=\"col-md-3\">
                            <div class=\"control-group\">
                                <label for=\"det_modelo\" class=\"control-label\">Modelo</label>
                                <select name=\"det_modelo\" id=\"det_modelo\" class=\"form-control\" data-width=\"100%\" >
                                    <option value=\"0\" >Elige el modelo del Producto </option>
                                    ";
        // line 100
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["modelo"]);
        foreach ($context['_seq'] as $context["_key"] => $context["modelo"]) {
            // line 101
            echo "                                        <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["modelo"], "id", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["modelo"], "nombre", array()), "html", null, true);
            echo " </option>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['modelo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 103
        echo "
                                </select>
                            </div>
                        </div>
                                    
                                    <div class=\"col-md-3\">
                            <div class=\"control-group\">
                                <label for=\"det_color\" class=\"control-label\">Color</label>
                                <select name=\"det_color\" id=\"det_color\" class=\"form-control\" data-width=\"100%\" >
                                    <option value=\"0\" >Elige el color del Producto </option>
                                    ";
        // line 113
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["color"]);
        foreach ($context['_seq'] as $context["_key"] => $context["color"]) {
            // line 114
            echo "                                        <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["color"], "id", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["color"], "nombre", array()), "html", null, true);
            echo " </option>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['color'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 116
        echo "

                                </select>
                            </div>
                        </div>
                                    
                                     <div class=\"col-md-3\">
                            <div class=\"control-group\">
                                <label for=\"det_tipotejido\" class=\"control-label\">Tipo de Tejido</label>
                                <select name=\"det_tipotejido\" id=\"det_tipotejido\" class=\"form-control\" data-width=\"100%\" >
                                    <option value=\"0\" >Eliga el tipo de tejido </option>
                                    ";
        // line 127
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["tipot"]);
        foreach ($context['_seq'] as $context["_key"] => $context["tipot"]) {
            // line 128
            echo "                                        <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["tipot"], "id", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["tipot"], "nombre", array()), "html", null, true);
            echo " </option>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tipot'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 130
        echo "                                </select>

                            </div>
                        </div>
                                    
                       <!-- <div class=\"col-md-3\">
                            <div class=\"control-group\">
                                <label for=\"det_numero\" class=\"control-label\">Número</label>
                                <input type=\"number\" step=\"any\" min=\"0\" class=\"form-control\"  id=\"det_numero\" name=\"det_numero\" placeholder=\"\">
                            </div>
                        </div>
                        <div class=\"col-md-3\">
                            <div class=\"control-group\">
                                <label for=\"det_unidad\" class=\"control-label\">Unidad de Medida</label>
                                <select name=\"det_unidad\" id=\"det_unidad\" class=\"form-control\" data-width=\"100%\" >
                                    <option value=\"0\" >Eliga la unidad de medida </option>
                                    ";
        // line 146
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["unidad"]);
        foreach ($context['_seq'] as $context["_key"] => $context["unidad"]) {
            // line 147
            echo "                                        <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["unidad"], "id", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["unidad"], "nombre", array()), "html", null, true);
            echo " </option>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['unidad'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 149
        echo "                                </select>   

                            </div>
                        </div>-->
                    </div>

                    <div class=\"row\">
                        <!--<div class=\"col-md-3\">
                            <div class=\"control-group\">
                                <label for=\"det_peso\" class=\"control-label\">Peso</label>
                                <input  type=\"number\" step=\"any\" min=\"0\" class=\"form-control\" id=\"det_peso\" name=\"det_peso\" placeholder=\"\">
                            </div>
                        </div>
                        <div class=\"col-md-3\">
                            <div class=\"control-group\">
                                <label for=\"det_ancho\" class=\"control-label\">Ancho</label>
                                <input  type=\"number\" step=\"any\" min=\"0\" class=\"form-control\" id=\"det_ancho\" name=\"det_ancho\" placeholder=\"\">
                            </div>
                        </div>
                        <div class=\"col-md-3\">
                            <div class=\"control-group\">
                                <label for=\"det_longitud\" class=\"control-label\">Longitud</label>
                                <input  type=\"number\" step=\"any\" min=\"0\" class=\"form-control\" id=\"det_longitud\" name=\"det_longitud\" placeholder=\"\">
                            </div>
                        </div>-->
                        
                    </div>

                    <div class=\"row\">
                       
                        <div class=\"col-md-4\">
                            <div class=\"control-group\">
                                <label for=\"det_densidad\" class=\"control-label\">Densidad</label>
                                <input  type=\"number\" step=\"any\" min=\"0\" class=\"form-control\" id=\"det_densidad\" name=\"det_densidad\" placeholder=\"\">
                            </div>
                        </div>
                        <div class=\"col-md-4\">
                            <div class=\"control-group\">
                                <label for=\"det_hilados\" class=\"control-label\">Hilados</label>
                                <input  type=\"number\" step=\"any\" min=\"0\" class=\"form-control\" id=\"det_hilados\" name=\"det_hilados\" placeholder=\"\">
                            </div>
                        </div>
                        <div class=\"col-md-4\">
                            <div class=\"control-group\">
                                <label for=\"det_fibras\" class=\"control-label\">Fibras</label>
                                <input  type=\"number\" step=\"any\" min=\"0\" class=\"form-control\" id=\"det_fibras\" name=\"det_fibras\" placeholder=\"\">
                            </div>
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"col-md-4\">
                            <div class=\"control-group\">
                                <label for=\"det_composicion\" class=\"control-label\">Composición</label>
                                <input  type=\"text\" class=\"form-control\" id=\"det_composicion\" name=\"det_composicion\" placeholder=\"\">
                            </div>
                        </div>
                        <div class=\"col-md-4\">
                            <div class=\"control-group\">
                                <label for=\"det_usos\" class=\"control-label\">Otros Usos</label>
                                <input  type=\"text\" class=\"form-control\" id=\"det_usos\" name=\"det_usos\" placeholder=\"\">
                            </div>
                        </div>


                        <div class=\"col-md-4 \">
                            <div class=\"control-group\">
                                <label for=\"det_otrosnombres\" class=\"control-label\">Otros Nombres</label>
                                <input type=\"text\" class=\"form-control\" id=\"det_otrosnombres\" name=\"det_otrosnombres\" placeholder=\"\">
                            </div>
                        </div>

                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-8\" id='divcodigo'>

                            <img style=\"margin:0 auto;\" class=\"img-responsive\" id=\"barcode\"/>

                        </div>
                        <div class=\"col-md-4 text-center\">
                            <div class=\"col-md-10 btn-lg \"> <a download=\"\" style=\"cursor: pointer;\" id='btndescargar'>Descargar</a></div>
                            <div class=\"col-md-10 btn-lg \"> <a  style=\"cursor: pointer;\" id=\"printcodigo\">Imprimir</a></div>
                        </div>
                    </div>
                    <div class=\"row\">

                        <div class=\"col-md-12\">
                            <div class=\"control-group\">
                                <label for=\"det_precio\" class=\"control-label\">\t
                                    Información Adicional </label>
                                <textarea class=\"form-control\" rows=\"5\" id=\"det_adicional\" name=\"det_adicional\" placeholder=\"\"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class=\"row\">

                        <div class=\"col-md-12\">
                            <div class=\"control-group\">




                                ";
        // line 252
        if (twig_in_filter("EDICION", (isset($context["ppm"]) ? $context["ppm"] : $this->getContext($context, "ppm")))) {
            // line 253
            echo "                                    <input type=\"submit\" value=\"modificar\" class=\" btn btn-lg col-md-12 text-center\" id=\"btn_modificar\"  data-path=\"";
            echo $this->env->getExtension('routing')->getPath("modificar_producto");
            echo "\"    />            
                                    <button class=\"btn col-md-12  btn-lg eliminarproducto\" data-id=\"\" data-path=\"";
            // line 254
            echo $this->env->getExtension('routing')->getPath("eliminar_producto");
            echo "\" id=\"eliminarproducto\" >Eliminar Producto</button>
                                ";
        } else {
            // line 256
            echo "                                    <input type=\"submit\" class=\" btn btn-lg col-md-12 text-center boton_disable\" id=\"btn_modificar\"  data-path=\"";
            echo $this->env->getExtension('routing')->getPath("modificar_producto");
            echo "\"    value=\"Modificar\"/>
                                    <button class=\"col-md-12  btn btn-lg boton_disable\" data-id=\"\" data-path=\"";
            // line 257
            echo $this->env->getExtension('routing')->getPath("eliminar_producto");
            echo "\" id=\"eliminarproducto\" >Eliminar Producto</button>
                                ";
        }
        // line 259
        echo "
                            </div>
                        </div>
                    </div>
                    <input name=\"idproductomodificar\" class=\"form-control\" placeholder=\"\" type=\"hidden\"  value=\"\" id=\"idproductomodificar\" style=\"width: 100%;\" >


                </div>


            </form>
        </div>




    </div>

</div>



";
        
        $__internal_73d070df908031f23b345c0a3096c86e02b3c4c6a4a6efb3aae4bf566cd9a052->leave($__internal_73d070df908031f23b345c0a3096c86e02b3c4c6a4a6efb3aae4bf566cd9a052_prof);

    }

    public function getTemplateName()
    {
        return "AdminBundle:Productos:detalleproductos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  372 => 259,  367 => 257,  362 => 256,  357 => 254,  352 => 253,  350 => 252,  245 => 149,  234 => 147,  230 => 146,  212 => 130,  201 => 128,  197 => 127,  184 => 116,  173 => 114,  169 => 113,  157 => 103,  146 => 101,  142 => 100,  112 => 73,  107 => 70,  96 => 68,  92 => 67,  87 => 65,  22 => 2,);
    }
}
/* {# empty Twig template #}*/
/* <div id="detalleproductos" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="MyModalLabel" aria-hidden="false">*/
/*     <div class="modal-dialog text-left">*/
/*         <div class="modal-content">*/
/*             <div class="modal-header"><!--#9adc09-->*/
/*                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>*/
/*                 <h4 class="modal-title" id="exampleModalLabel">Detalle Producto</h4>*/
/*             </div>*/
/*             <form action="" id="modificar_producto" method="POST">*/
/*                 <div class="modal-body">*/
/*                     <div class="row">*/
/*                         <div class="col-md-6">*/
/*                             <img src="" id="foto_producto" class="img-responsive">*/
/*                         </div>*/
/*                         <div class="col-md-6">*/
/*                             <div class="col-md-12">*/
/* */
/*                                 <div class="control-group">*/
/*                                     <label for="det_fecha" class="control-label">Fecha*/
/*                                     </label>*/
/*                                     <input type="text" class="form-control" id="det_fecha" name="det_fecha" disabled placeholder="">*/
/*                                 </div>*/
/* */
/*                             </div>*/
/* */
/*                             <div class="col-md-12">*/
/* */
/*                                 <div class="control-group">*/
/*                                     <label for="det_producto" class="control-label">Producto</label>*/
/*                                     <input type="text" class="form-control" id="det_producto" name="det_producto" placeholder="">*/
/*                                 </div>*/
/* */
/*                             </div>*/
/* */
/*                         </div>*/
/*                     </div>*/
/*                     <div class="row">*/
/*                         <div class="col-md-12">*/
/* */
/*                             <div class="control-group">*/
/*                                 <label for="det_folio" class="control-label">Folio*/
/*                                 </label>*/
/*                                 <input type="text" class="form-control" id="det_folio" name="det_folio" placeholder="" disabled>*/
/*                             </div>*/
/* */
/*                         </div>*/
/*                     </div>*/
/*                     <!--<div class="row">*/
/*                         <div class="col-md-6 col-sm-6 col-xs-6 "><label for="det_proveedor" class="control-label">Proveedor/es</label></div>*/
/*                         <div class="col-md-2 col-sm-2 col-xs-2"><label for="det_precio1" class="control-label">Precio 1</label></div>*/
/*                         <div class="col-md-2 col-sm-2 col-xs-2"><label for="det_precio2" class="control-label">Precio 2</label></div>*/
/*                         <div class="col-md-2 col-sm-2 col-xs-2"><label for="det_precio3" class="control-label">Precio 3</label></div>*/
/* */
/*                     </div>-->*/
/*                     */
/* */
/* */
/* */
/* */
/*                     <!--<div class="row">*/
/*                         <div class="col-md-5" >*/
/*                             <div class="control-group">*/
/*                                 <label for="det_clase" class="control-label">Clase del Producto</label>*/
/* */
/*                                 <select name="det_clase" id="det_clase" class="form-control" data-width="100%" data-path="{{path('clase_tipos')}}">*/
/*                                     <option value="0" >Elige la clase del Producto </option>*/
/*                                     {% for clase in clase %}*/
/*                                         <option value="{{clase.id}}" >{{clase.nombre}} </option>*/
/*                                     {% endfor %}*/
/*                                 </select>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-md-1"><img src="{{asset('imagenes/loadingddl.gif')}}" id="loadingddl_det" class="" style="width:20px;height: 20px; display: none;" /></div>*/
/* */
/*                         <div class="col-md-6"  id="div_tipoproducto_det" style="display: none;" >*/
/*                             <label for="det_tipo" class="control-label">Tipo del Producto</label>*/
/*                             <div class="control-group">*/
/* */
/*                                 <select name="det_tipo" id="det_tipo" class="form-control" data-width="100%" >*/
/*                                     <option value="0" >Elige el tipo de Producto </option>*/
/* */
/*                                 </select>*/
/* */
/* */
/*                             </div>*/
/*                         </div>*/
/*                     </div>-->*/
/*                     <div class="row">*/
/*                         <div class="col-md-3">*/
/*                             <div class="control-group">*/
/*                                 <label for="det_marca" class="control-label">Marca</label>*/
/*                                 <input type="text" class="form-control" id="det_marca" name="det_marca" placeholder="">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-md-3">*/
/*                             <div class="control-group">*/
/*                                 <label for="det_modelo" class="control-label">Modelo</label>*/
/*                                 <select name="det_modelo" id="det_modelo" class="form-control" data-width="100%" >*/
/*                                     <option value="0" >Elige el modelo del Producto </option>*/
/*                                     {% for modelo in modelo %}*/
/*                                         <option value="{{modelo.id}}" >{{modelo.nombre}} </option>*/
/*                                     {% endfor %}*/
/* */
/*                                 </select>*/
/*                             </div>*/
/*                         </div>*/
/*                                     */
/*                                     <div class="col-md-3">*/
/*                             <div class="control-group">*/
/*                                 <label for="det_color" class="control-label">Color</label>*/
/*                                 <select name="det_color" id="det_color" class="form-control" data-width="100%" >*/
/*                                     <option value="0" >Elige el color del Producto </option>*/
/*                                     {% for color in color %}*/
/*                                         <option value="{{color.id}}" >{{color.nombre}} </option>*/
/*                                     {% endfor %}*/
/* */
/* */
/*                                 </select>*/
/*                             </div>*/
/*                         </div>*/
/*                                     */
/*                                      <div class="col-md-3">*/
/*                             <div class="control-group">*/
/*                                 <label for="det_tipotejido" class="control-label">Tipo de Tejido</label>*/
/*                                 <select name="det_tipotejido" id="det_tipotejido" class="form-control" data-width="100%" >*/
/*                                     <option value="0" >Eliga el tipo de tejido </option>*/
/*                                     {% for tipot in tipot %}*/
/*                                         <option value="{{tipot.id}}" >{{tipot.nombre}} </option>*/
/*                                     {% endfor %}*/
/*                                 </select>*/
/* */
/*                             </div>*/
/*                         </div>*/
/*                                     */
/*                        <!-- <div class="col-md-3">*/
/*                             <div class="control-group">*/
/*                                 <label for="det_numero" class="control-label">Número</label>*/
/*                                 <input type="number" step="any" min="0" class="form-control"  id="det_numero" name="det_numero" placeholder="">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-md-3">*/
/*                             <div class="control-group">*/
/*                                 <label for="det_unidad" class="control-label">Unidad de Medida</label>*/
/*                                 <select name="det_unidad" id="det_unidad" class="form-control" data-width="100%" >*/
/*                                     <option value="0" >Eliga la unidad de medida </option>*/
/*                                     {% for unidad in unidad %}*/
/*                                         <option value="{{unidad.id}}" >{{unidad.nombre}} </option>*/
/*                                     {% endfor %}*/
/*                                 </select>   */
/* */
/*                             </div>*/
/*                         </div>-->*/
/*                     </div>*/
/* */
/*                     <div class="row">*/
/*                         <!--<div class="col-md-3">*/
/*                             <div class="control-group">*/
/*                                 <label for="det_peso" class="control-label">Peso</label>*/
/*                                 <input  type="number" step="any" min="0" class="form-control" id="det_peso" name="det_peso" placeholder="">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-md-3">*/
/*                             <div class="control-group">*/
/*                                 <label for="det_ancho" class="control-label">Ancho</label>*/
/*                                 <input  type="number" step="any" min="0" class="form-control" id="det_ancho" name="det_ancho" placeholder="">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-md-3">*/
/*                             <div class="control-group">*/
/*                                 <label for="det_longitud" class="control-label">Longitud</label>*/
/*                                 <input  type="number" step="any" min="0" class="form-control" id="det_longitud" name="det_longitud" placeholder="">*/
/*                             </div>*/
/*                         </div>-->*/
/*                         */
/*                     </div>*/
/* */
/*                     <div class="row">*/
/*                        */
/*                         <div class="col-md-4">*/
/*                             <div class="control-group">*/
/*                                 <label for="det_densidad" class="control-label">Densidad</label>*/
/*                                 <input  type="number" step="any" min="0" class="form-control" id="det_densidad" name="det_densidad" placeholder="">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-md-4">*/
/*                             <div class="control-group">*/
/*                                 <label for="det_hilados" class="control-label">Hilados</label>*/
/*                                 <input  type="number" step="any" min="0" class="form-control" id="det_hilados" name="det_hilados" placeholder="">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-md-4">*/
/*                             <div class="control-group">*/
/*                                 <label for="det_fibras" class="control-label">Fibras</label>*/
/*                                 <input  type="number" step="any" min="0" class="form-control" id="det_fibras" name="det_fibras" placeholder="">*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <div class="row">*/
/*                         <div class="col-md-4">*/
/*                             <div class="control-group">*/
/*                                 <label for="det_composicion" class="control-label">Composición</label>*/
/*                                 <input  type="text" class="form-control" id="det_composicion" name="det_composicion" placeholder="">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-md-4">*/
/*                             <div class="control-group">*/
/*                                 <label for="det_usos" class="control-label">Otros Usos</label>*/
/*                                 <input  type="text" class="form-control" id="det_usos" name="det_usos" placeholder="">*/
/*                             </div>*/
/*                         </div>*/
/* */
/* */
/*                         <div class="col-md-4 ">*/
/*                             <div class="control-group">*/
/*                                 <label for="det_otrosnombres" class="control-label">Otros Nombres</label>*/
/*                                 <input type="text" class="form-control" id="det_otrosnombres" name="det_otrosnombres" placeholder="">*/
/*                             </div>*/
/*                         </div>*/
/* */
/*                     </div>*/
/*                     <div class="row">*/
/*                         <div class="col-md-8" id='divcodigo'>*/
/* */
/*                             <img style="margin:0 auto;" class="img-responsive" id="barcode"/>*/
/* */
/*                         </div>*/
/*                         <div class="col-md-4 text-center">*/
/*                             <div class="col-md-10 btn-lg "> <a download="" style="cursor: pointer;" id='btndescargar'>Descargar</a></div>*/
/*                             <div class="col-md-10 btn-lg "> <a  style="cursor: pointer;" id="printcodigo">Imprimir</a></div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="row">*/
/* */
/*                         <div class="col-md-12">*/
/*                             <div class="control-group">*/
/*                                 <label for="det_precio" class="control-label">	*/
/*                                     Información Adicional </label>*/
/*                                 <textarea class="form-control" rows="5" id="det_adicional" name="det_adicional" placeholder=""></textarea>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <div class="row">*/
/* */
/*                         <div class="col-md-12">*/
/*                             <div class="control-group">*/
/* */
/* */
/* */
/* */
/*                                 {% if "EDICION" in ppm %}*/
/*                                     <input type="submit" value="modificar" class=" btn btn-lg col-md-12 text-center" id="btn_modificar"  data-path="{{path('modificar_producto')}}"    />            */
/*                                     <button class="btn col-md-12  btn-lg eliminarproducto" data-id="" data-path="{{path('eliminar_producto')}}" id="eliminarproducto" >Eliminar Producto</button>*/
/*                                 {% else %}*/
/*                                     <input type="submit" class=" btn btn-lg col-md-12 text-center boton_disable" id="btn_modificar"  data-path="{{path('modificar_producto')}}"    value="Modificar"/>*/
/*                                     <button class="col-md-12  btn btn-lg boton_disable" data-id="" data-path="{{path('eliminar_producto')}}" id="eliminarproducto" >Eliminar Producto</button>*/
/*                                 {% endif %}*/
/* */
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <input name="idproductomodificar" class="form-control" placeholder="" type="hidden"  value="" id="idproductomodificar" style="width: 100%;" >*/
/* */
/* */
/*                 </div>*/
/* */
/* */
/*             </form>*/
/*         </div>*/
/* */
/* */
/* */
/* */
/*     </div>*/
/* */
/* </div>*/
/* */
/* */
/* */
/* */
