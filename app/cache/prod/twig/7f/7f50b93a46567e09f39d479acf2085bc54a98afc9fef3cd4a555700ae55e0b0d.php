<?php

/* AdminBundle:entradas:entradas.html.twig */
class __TwigTemplate_efd14111bf5ef9db52bd8da14586238e863426c3450fb59f1d170cd6d982d191 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "AdminBundle:entradas:entradas.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'header' => array($this, 'block_header'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a72391b54d2411dcdd68cd82e02d56d42720e1ba1a00cb0b4827b638576964b0 = $this->env->getExtension("native_profiler");
        $__internal_a72391b54d2411dcdd68cd82e02d56d42720e1ba1a00cb0b4827b638576964b0->enter($__internal_a72391b54d2411dcdd68cd82e02d56d42720e1ba1a00cb0b4827b638576964b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AdminBundle:entradas:entradas.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a72391b54d2411dcdd68cd82e02d56d42720e1ba1a00cb0b4827b638576964b0->leave($__internal_a72391b54d2411dcdd68cd82e02d56d42720e1ba1a00cb0b4827b638576964b0_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_ab2764146f0812fa21313a30e8be9ba771fb88f1ffb092f32ac699b83c18f7cd = $this->env->getExtension("native_profiler");
        $__internal_ab2764146f0812fa21313a30e8be9ba771fb88f1ffb092f32ac699b83c18f7cd->enter($__internal_ab2764146f0812fa21313a30e8be9ba771fb88f1ffb092f32ac699b83c18f7cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "    Entradas
";
        
        $__internal_ab2764146f0812fa21313a30e8be9ba771fb88f1ffb092f32ac699b83c18f7cd->leave($__internal_ab2764146f0812fa21313a30e8be9ba771fb88f1ffb092f32ac699b83c18f7cd_prof);

    }

    // line 6
    public function block_header($context, array $blocks = array())
    {
        $__internal_ffa72476bfac4be33681f6c93ce75c5b0e326526ed115c4f7c32e12d66bbb277 = $this->env->getExtension("native_profiler");
        $__internal_ffa72476bfac4be33681f6c93ce75c5b0e326526ed115c4f7c32e12d66bbb277->enter($__internal_ffa72476bfac4be33681f6c93ce75c5b0e326526ed115c4f7c32e12d66bbb277_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 7
        echo "
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/admin/js/entradas.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/JsBarcode.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/jQuery.print.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/CODE128.js"), "html", null, true);
        echo "\"></script>

    <script src=\"https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js\"></script>
    <script src=\"https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js\"></script>
    <script src=\"https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js\"></script>
    <script src=\"https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js\"></script>
    <script src=\"https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js\"></script>
    <script src=\"https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js\"></script>
    <script src=\"https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js\"></script>

    <link rel=\"stylesheet\" type=\"text/css\" href=\"https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css\">

";
        
        $__internal_ffa72476bfac4be33681f6c93ce75c5b0e326526ed115c4f7c32e12d66bbb277->leave($__internal_ffa72476bfac4be33681f6c93ce75c5b0e326526ed115c4f7c32e12d66bbb277_prof);

    }

    // line 25
    public function block_body($context, array $blocks = array())
    {
        $__internal_53aaae91fd67570bab86aed9abe71446850effe037f339733df86af6efe2c971 = $this->env->getExtension("native_profiler");
        $__internal_53aaae91fd67570bab86aed9abe71446850effe037f339733df86af6efe2c971->enter($__internal_53aaae91fd67570bab86aed9abe71446850effe037f339733df86af6efe2c971_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 26
        echo "    <div class=\"container\">
        <div class=\"row\">

            <h1 class=\"text-center header\">Registro y Detalle de Entradas</h1>
            ";
        // line 30
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 31
            echo "
                <div class=\"alert alert-success\">
                    ";
            // line 33
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "
                </div>

            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "
            ";
        // line 38
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "warning"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 39
            echo "
                <div class=\"alert alert-danger\">
                    ";
            // line 41
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "
                </div>

            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "        </div>
    </div>

    <div class=\"container main\">

        <div class=\"row\">
            <div class=\"col-md-2 col-md-offset-5 btn btn-lg\" id=\"divnuevaentrada\">Nueva Entrada</div>
            <div class=\"col-md-12\" id=\"nuevaentradadiv\" style=\"display: none;\">
                <form role=\"form\" id=\"entradasform\" method=\"POST\" enctype=\"multipart/form-data\" action=\"";
        // line 53
        echo $this->env->getExtension('routing')->getPath("crear_entrada");
        echo "\">
                    <div class=\"row\">
                        <div class=\"col-md-3 col-sm-3 col-xs-3\">
                            <div class=\"control-group\">
                                <label for=\"producto\" class=\"control-label\">Producto
                                </label>
                                <select name=\"producto\" id=\"producto\" class=\"form-control\" data-width=\"100%\" data-path=\"";
        // line 59
        echo $this->env->getExtension('routing')->getPath("entradas_proveedor");
        echo "\">
                                    <option value=\"0\" >Elija el producto </option>
                                    ";
        // line 61
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["productos"]);
        foreach ($context['_seq'] as $context["_key"] => $context["productos"]) {
            // line 62
            echo "                                        <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["productos"], "id", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["productos"], "nombre", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["productos"], "color", array()), "nombre", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["productos"], "longitud", array()), "html", null, true);
            echo "-";
            echo twig_escape_filter($this->env, $this->getAttribute($context["productos"], "ancho", array()), "html", null, true);
            echo "-";
            echo twig_escape_filter($this->env, $this->getAttribute($context["productos"], "peso", array()), "html", null, true);
            echo "</option>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['productos'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 64
        echo "                                </select>
                            </div>
                        </div>

                        <div class=\"col-md-3\">
                            <div class=\"control-group\">
                                <label for=\"producto\" class=\"control-label\">Unidad de medida
                                </label>
                                <select name=\"unidadmedida_ddl\" id=\"unidadmedida_ddl\" class=\"form-control\" data-width=\"100%\" >
                                    <option value=\"0\" >Eliga la unidad de medida </option>
                                    ";
        // line 74
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["unidad"]);
        foreach ($context['_seq'] as $context["_key"] => $context["unidad"]) {
            // line 75
            echo "                                        <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["unidad"], "id", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["unidad"], "nombre", array()), "html", null, true);
            echo " </option>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['unidad'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo "                                </select>

                            </div>
                        </div>


                        <div class=\"col-md-6\">


                            ";
        // line 86
        $this->loadTemplate("AdminBundle:proveedores:proveedores.html.twig", "AdminBundle:entradas:entradas.html.twig", 86)->display($context);
        // line 87
        echo "                        </div>


                    </div>

                    <div class=\"row\">
                        <div class=\"col-md-3 col-sm-3 col-xs-12 \">
                            <div class=\"form-group\">
                                <label for=\"numero\" class=\"control-label\">Número
                                </label>
                                <input type=\"number\" step=\"any\" min=\"0\" class=\"form-control\" id=\"numero\" name=\"numero\"  placeholder=\"0\">
                            </div>
                        </div>
                        <div class=\"col-md-3 col-sm-3 col-xs-12 \">
                            <div class=\"form-group\">
                                <label for=\"peso\" class=\"control-label\">Peso
                                </label>
                                <input type=\"number\" step=\"any\" min=\"0\" class=\"form-control\" id=\"peso\" name=\"peso\"  placeholder=\"0\">
                            </div>
                        </div>
                        <div class=\"col-md-3 col-sm-3 col-xs-12 \">
                            <div class=\"form-group\">
                                <label for=\"ancho\" class=\"control-label\">Ancho en Cms
                                </label>
                                <input type=\"number\" step=\"any\" min=\"0\" class=\"form-control\" id=\"ancho\" name=\"ancho\"  placeholder=\"0\" >
                            </div>
                        </div><div class=\"col-md-3 col-sm-3 col-xs-12 \">
                            <div class=\"form-group\">
                                <label for=\"longitud\" class=\"control-label\">Longitud en Cms
                                </label>
                                <input type=\"number\" step=\"any\" min=\"0\" class=\"form-control\" id=\"longitud\" name=\"longitud\"  placeholder=\"0\" >
                            </div>
                        </div>

                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-3\" >
                            <div class=\"control-group\">
                                <label for=\"entrega\" class=\"control-label\">Persona que entrega
                                </label>
                                <input  type=\"text\"  class=\"form-control\" id=\"entrega\" name=\"entrega\" placeholder=\"Nombre completo\">

                            </div>
                        </div>
                        <div class=\"col-md-3\" >
                            <div class=\"control-group\">
                                <label for=\"longitud\" class=\"control-label\">Cantidad que ingresa
                                </label>
                                <input  type=\"number\" type=\"any\" min=\"0\" class=\"form-control\" id=\"cantidadingresa\" name=\"cantidadingresa\" placeholder=\"0\">
                                <input  type=\"hidden\" type=\"any\" min=\"0\" class=\"form-control\" id=\"valor\" name=\"valor\" placeholder=\"0\">
                            </div>
                        </div>
                        <div class=\"col-md-6\" >
                            <div class=\"control-group\">
                                <label for=\"longitud\" class=\"control-label\">Cantidad de Ingreso por Unidad de Medida
                                </label>
                                <input  type=\"number\" type=\"any\" min=\"0\" class=\"form-control\" id=\"cantidadunidad\" name=\"cantidadunidad\" placeholder=\"0\" disabled>
                                <input  type=\"hidden\" type=\"any\" min=\"0\" class=\"form-control\" id=\"cantidadunidadh\" name=\"cantidadunidadh\" >
                            </div>
                        </div>
                                               
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-3\" >
                            <div class=\"control-group\">
                                <label for=\"formaentrada\" class=\"control-label\">Forma de Entrada
                                </label>
                                <select name=\"formaentrada\" id=\"formaentrada\" class=\"form-control\" data-width=\"100%\">
                                    <option value=\"0\" >Elija la forma de entrada </option>
                                    ";
        // line 156
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["formaentrada"]);
        foreach ($context['_seq'] as $context["_key"] => $context["formaentrada"]) {
            // line 157
            echo "                                        <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["formaentrada"], "id", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["formaentrada"], "formanentrada", array()), "html", null, true);
            echo "</option>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['formaentrada'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 159
        echo "                                </select>
                            </div>
                        </div>
                        <div class=\"col-md-3\" >
                            <div class=\"control-group\">
                                <label for=\"cajas\" class=\"control-label\">Número de Cajas
                                </label>
                                <input  type=\"number\" step=\"any\" min=\"0\"  class=\"form-control\" id=\"cajas\" name=\"cajas\" placeholder=\"0\" >
                            </div>
                        </div> 
                        <div class=\"col-md-3\" >
                            <div class=\"control-group\">
                                <label for=\"paquetes\" class=\"control-label\">Número de Paquetes
                                </label>
                                <input  type=\"number\" step=\"any\" min=\"0\"  class=\"form-control\" id=\"paquetes\" name=\"paquetes\" placeholder=\"0\" >
                            </div>
                        </div> 
                        <div class=\"col-md-3\" >
                            <div class=\"control-group\">
                                <label for=\"unidades\" class=\"control-label\">Número de Unidades
                                </label>
                                <input  type=\"number\" step=\"any\" min=\"0\"  class=\"form-control\" id=\"unidades\" name=\"unidades\" placeholder=\"0\"  >
                            </div>
                        </div> 

                    </div>


                    <div class=\"row\">
                        <div class=\"col-md-6\" >
                            <div class=\"control-group\">
                                <label for=\"fecharegistro\" class=\"control-label\">Fecha de Registro de Entrada
                                </label>
                                <input  type=\"text\"   class=\"form-control\" id=\"fecharegistro\" name=\"fecharegistro\" value=\"";
        // line 192
        echo twig_escape_filter($this->env, $this->getAttribute(twig_date_converter($this->env), "format", array(0 => "d/m/Y"), "method"), "html", null, true);
        echo "\" disabled>
                            </div>
                        </div>
                        <div class=\"col-md-6\" >
                            <div class=\"control-group\">
                                <label for=\"almacen\" class=\"control-label\">Almacen de Resguardo
                                </label>
                                <select name=\"almacen\" id=\"almacen\" class=\"form-control\" data-width=\"100%\">
                                    <option value=\"0\" >Elija el almacen </option>
                                    ";
        // line 201
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["almacen"]);
        foreach ($context['_seq'] as $context["_key"] => $context["almacen"]) {
            // line 202
            echo "                                        <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["almacen"], "id", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["almacen"], "almacen", array()), "html", null, true);
            echo "</option>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['almacen'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 204
        echo "                                </select>
                            </div>
                        </div>
                    </div>







                    <div class=\"row\">
                        <div class=\"col-md-12\">
                            <input type=\"hidden\" id=\"urlinsertarentrada\" value=\"";
        // line 217
        echo $this->env->getExtension('routing')->getPath("crear_entrada");
        echo "\"/>
                            <input   ";
        // line 218
        if (twig_in_filter("ESCRITURA", (isset($context["ppm"]) ? $context["ppm"] : $this->getContext($context, "ppm")))) {
            echo " id=\"btn_agregar_entrada\" class=\"btn-lg col-md-2 text-center pull-right \" type=\"submit\"  ";
        } else {
            echo "  class=\"btn-lg col-md-2 text-center pull-right boton_disable\"  ";
        }
        echo " value=\"Agregar Producto\" /> 
                        </div>
                    </div>

                </form>
            </div>
        </div>

        <div class=\"row\">
            <div class=\"col-md-12 tableblanco \" >
                <form action=\"";
        // line 228
        echo $this->env->getExtension('routing')->getPath("entradasfecha");
        echo "\"  method=\"post\">
                <table border=\"0\" cellspacing=\"5\" cellpadding=\"5\" style=\"padding-bottom: 14px;\">
                    <tbody><tr>
                            <td>Fecha de inicio:</td>
                            <td><input type=\"text\" class=\"form-control datepicker\"  id=\"min\" name=\"min\"   value=\"\"></td>
                            
                            <td>Fecha de término:</td>
                            <td><input type=\"text\" class=\"form-control datepicker\"  id=\"max\" name=\"max\"   value=\"\"></td>
                           
                            <td><input type=\"submit\" value=\"Generar reporte\" class=\"btn-lg linkload\"></td>
                        </tr>
                        <tr>
                            <td colspan=\"2\">";
        // line 240
        if (array_key_exists("min", $context)) {
            echo " ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["min"]) ? $context["min"] : $this->getContext($context, "min")), "d/m/Y"), "html", null, true);
            echo " ";
        }
        echo "</td>
                             <td colspan=\"2\">";
        // line 241
        if (array_key_exists("max", $context)) {
            echo " ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["max"]) ? $context["max"] : $this->getContext($context, "max")), "d/m/Y"), "html", null, true);
            echo " ";
        }
        echo "</td>
                        </tr>
                    </tbody></table>
                </form>
            </div>
            <div class=\"col-md-12 tableblanco\" >
                <table id=\"tabla_entradas\" class=\"display nowrap table table-striped  \" cellspacing=\"0\" width=\"100%\">
                    <thead>
                        <tr>
                            <th class=\"text-center\">Entrada</th>
                            <th class=\"text-center\">Fecha de Registro</th>
                            <th class=\"text-center\">Producto</th> 
                            <th class=\"text-center\">Modelo</th>
                            <th class=\"text-center\">Color</th>
                            <th class=\"text-center\">Ancho</th>
                            <th class=\"text-center\">Largo</th>
                            <th class=\"text-center\">Peso</th>
                            <th class=\"text-center\">Número</th> 

                            <th class=\"text-center\">Caja</th>
                            <th class=\"text-center\">Paquete</th>
                            <th class=\"text-center\">Unidades</th>
                            <th class=\"text-center\">Total Ingreso Unidades</th>
                            <th class=\"text-center\">Total Ingreso p/u de medida</th>
                            <th class=\"text-center\">Código(s)</th>

                        </tr>
                    </thead>
                    <tbody>
                        ";
        // line 270
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["entradas"]);
        foreach ($context['_seq'] as $context["_key"] => $context["entradas"]) {
            echo " 
                            <tr>
                                <td class=\"text-center\">";
            // line 272
            echo twig_escape_filter($this->env, $this->getAttribute($context["entradas"], "id", array()), "html", null, true);
            echo "</td>
                                <td class=\"text-center\">";
            // line 273
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entradas"], "fecharegistro", array()), "d/m/Y"), "html", null, true);
            echo "</td>
                                <td class=\"text-center\">";
            // line 274
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entradas"], "producto", array()), "nombre", array()), "html", null, true);
            echo "</td>
                                <td class=\"text-center\">";
            // line 275
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["entradas"], "producto", array()), "modelo", array()), "nombre", array()), "html", null, true);
            echo "</td>
                                <td class=\"text-center\">";
            // line 276
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["entradas"], "producto", array()), "color", array()), "nombre", array()), "html", null, true);
            echo "</td>
                                <td class=\"text-center\">";
            // line 277
            echo twig_escape_filter($this->env, $this->getAttribute($context["entradas"], "ancho", array()), "html", null, true);
            echo "</td>
                                <td class=\"text-center\">";
            // line 278
            echo twig_escape_filter($this->env, $this->getAttribute($context["entradas"], "longitud", array()), "html", null, true);
            echo "</td>
                                <td class=\"text-center\">";
            // line 279
            echo twig_escape_filter($this->env, $this->getAttribute($context["entradas"], "peso", array()), "html", null, true);
            echo "</td>
                                <td class=\"text-center\">";
            // line 280
            echo twig_escape_filter($this->env, $this->getAttribute($context["entradas"], "numero", array()), "html", null, true);
            echo "</td>

                                <td class=\"text-center\">";
            // line 282
            echo twig_escape_filter($this->env, $this->getAttribute($context["entradas"], "cajas", array()), "html", null, true);
            echo "</td>
                                <td class=\"text-center\">";
            // line 283
            echo twig_escape_filter($this->env, $this->getAttribute($context["entradas"], "paquetes", array()), "html", null, true);
            echo "</td>
                                <td class=\"text-center\">";
            // line 284
            echo twig_escape_filter($this->env, $this->getAttribute($context["entradas"], "unidades", array()), "html", null, true);
            echo "</td>
                                <td class=\"text-center\">";
            // line 285
            echo twig_escape_filter($this->env, $this->getAttribute($context["entradas"], "cantidadingresa", array()), "html", null, true);
            echo "</td>
                                <td class=\"text-center\">";
            // line 286
            echo twig_escape_filter($this->env, $this->getAttribute($context["entradas"], "cantidadunidad", array()), "html", null, true);
            echo " </td>

                                <td class=\"text-center\">
                                    <button class=\"btn btn-lg-tabla detalle_entradas\" type=\"button\" data-toggle=\"modal\" data-path=\"";
            // line 289
            echo $this->env->getExtension('routing')->getPath("detalle_entrada");
            echo "\" data-id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entradas"], "id", array()), "html", null, true);
            echo "\" data-target=\"#detalleentradas\" > Detalles    </button>
                                </td>


                            </tr>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entradas'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 295
        echo "                    </tbody>
                </table>
            </div>
        </div>

        <div class=\"row\">
            ";
        // line 301
        $this->loadTemplate("AdminBundle:entradas:detalleentradas.html.twig", "AdminBundle:entradas:entradas.html.twig", 301)->display($context);
        // line 302
        echo "

        </div>
    </div>
            
            <script>
  
    \$.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '< Ant',
        nextText: 'Sig >',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    \$.datepicker.setDefaults(\$.datepicker.regional['es']);
    \$(function () {
        \$(\".datepicker\").datepicker();
        
    });
</script>
            
";
        
        $__internal_53aaae91fd67570bab86aed9abe71446850effe037f339733df86af6efe2c971->leave($__internal_53aaae91fd67570bab86aed9abe71446850effe037f339733df86af6efe2c971_prof);

    }

    public function getTemplateName()
    {
        return "AdminBundle:entradas:entradas.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  563 => 302,  561 => 301,  553 => 295,  539 => 289,  533 => 286,  529 => 285,  525 => 284,  521 => 283,  517 => 282,  512 => 280,  508 => 279,  504 => 278,  500 => 277,  496 => 276,  492 => 275,  488 => 274,  484 => 273,  480 => 272,  473 => 270,  437 => 241,  429 => 240,  414 => 228,  397 => 218,  393 => 217,  378 => 204,  367 => 202,  363 => 201,  351 => 192,  316 => 159,  305 => 157,  301 => 156,  230 => 87,  228 => 86,  217 => 77,  206 => 75,  202 => 74,  190 => 64,  171 => 62,  167 => 61,  162 => 59,  153 => 53,  143 => 45,  133 => 41,  129 => 39,  125 => 38,  122 => 37,  112 => 33,  108 => 31,  104 => 30,  98 => 26,  92 => 25,  71 => 11,  67 => 10,  63 => 9,  59 => 8,  56 => 7,  50 => 6,  42 => 4,  36 => 3,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block title %}*/
/*     Entradas*/
/* {% endblock %}*/
/* {% block header %}*/
/* */
/*     <script src="{{ asset('bundles/admin/js/entradas.js') }}"></script>*/
/*     <script src="{{ asset('js/JsBarcode.js') }}"></script>*/
/*     <script src="{{ asset('js/jQuery.print.js') }}"></script>*/
/*     <script src="{{ asset('js/CODE128.js') }}"></script>*/
/* */
/*     <script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>*/
/*     <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>*/
/*     <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>*/
/*     <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>*/
/*     <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>*/
/*     <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>*/
/*     <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>*/
/*     <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>*/
/* */
/*     <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">*/
/* */
/* {% endblock %}*/
/* {% block body %}*/
/*     <div class="container">*/
/*         <div class="row">*/
/* */
/*             <h1 class="text-center header">Registro y Detalle de Entradas</h1>*/
/*             {% for flashMessage in app.session.flashbag.get('notice') %}*/
/* */
/*                 <div class="alert alert-success">*/
/*                     {{ flashMessage }}*/
/*                 </div>*/
/* */
/*             {% endfor %}*/
/* */
/*             {% for flashMessage in app.session.flashbag.get('warning') %}*/
/* */
/*                 <div class="alert alert-danger">*/
/*                     {{ flashMessage }}*/
/*                 </div>*/
/* */
/*             {% endfor %}*/
/*         </div>*/
/*     </div>*/
/* */
/*     <div class="container main">*/
/* */
/*         <div class="row">*/
/*             <div class="col-md-2 col-md-offset-5 btn btn-lg" id="divnuevaentrada">Nueva Entrada</div>*/
/*             <div class="col-md-12" id="nuevaentradadiv" style="display: none;">*/
/*                 <form role="form" id="entradasform" method="POST" enctype="multipart/form-data" action="{{path('crear_entrada')}}">*/
/*                     <div class="row">*/
/*                         <div class="col-md-3 col-sm-3 col-xs-3">*/
/*                             <div class="control-group">*/
/*                                 <label for="producto" class="control-label">Producto*/
/*                                 </label>*/
/*                                 <select name="producto" id="producto" class="form-control" data-width="100%" data-path="{{path('entradas_proveedor')}}">*/
/*                                     <option value="0" >Elija el producto </option>*/
/*                                     {% for productos in productos %}*/
/*                                         <option value="{{productos.id}}" >{{productos.nombre}} {{productos.color.nombre}} - {{productos.longitud}}-{{productos.ancho}}-{{productos.peso}}</option>*/
/*                                     {% endfor %}*/
/*                                 </select>*/
/*                             </div>*/
/*                         </div>*/
/* */
/*                         <div class="col-md-3">*/
/*                             <div class="control-group">*/
/*                                 <label for="producto" class="control-label">Unidad de medida*/
/*                                 </label>*/
/*                                 <select name="unidadmedida_ddl" id="unidadmedida_ddl" class="form-control" data-width="100%" >*/
/*                                     <option value="0" >Eliga la unidad de medida </option>*/
/*                                     {% for unidad in unidad %}*/
/*                                         <option value="{{unidad.id}}" >{{unidad.nombre}} </option>*/
/*                                     {% endfor %}*/
/*                                 </select>*/
/* */
/*                             </div>*/
/*                         </div>*/
/* */
/* */
/*                         <div class="col-md-6">*/
/* */
/* */
/*                             {% include 'AdminBundle:proveedores:proveedores.html.twig' %}*/
/*                         </div>*/
/* */
/* */
/*                     </div>*/
/* */
/*                     <div class="row">*/
/*                         <div class="col-md-3 col-sm-3 col-xs-12 ">*/
/*                             <div class="form-group">*/
/*                                 <label for="numero" class="control-label">Número*/
/*                                 </label>*/
/*                                 <input type="number" step="any" min="0" class="form-control" id="numero" name="numero"  placeholder="0">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-md-3 col-sm-3 col-xs-12 ">*/
/*                             <div class="form-group">*/
/*                                 <label for="peso" class="control-label">Peso*/
/*                                 </label>*/
/*                                 <input type="number" step="any" min="0" class="form-control" id="peso" name="peso"  placeholder="0">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-md-3 col-sm-3 col-xs-12 ">*/
/*                             <div class="form-group">*/
/*                                 <label for="ancho" class="control-label">Ancho en Cms*/
/*                                 </label>*/
/*                                 <input type="number" step="any" min="0" class="form-control" id="ancho" name="ancho"  placeholder="0" >*/
/*                             </div>*/
/*                         </div><div class="col-md-3 col-sm-3 col-xs-12 ">*/
/*                             <div class="form-group">*/
/*                                 <label for="longitud" class="control-label">Longitud en Cms*/
/*                                 </label>*/
/*                                 <input type="number" step="any" min="0" class="form-control" id="longitud" name="longitud"  placeholder="0" >*/
/*                             </div>*/
/*                         </div>*/
/* */
/*                     </div>*/
/*                     <div class="row">*/
/*                         <div class="col-md-3" >*/
/*                             <div class="control-group">*/
/*                                 <label for="entrega" class="control-label">Persona que entrega*/
/*                                 </label>*/
/*                                 <input  type="text"  class="form-control" id="entrega" name="entrega" placeholder="Nombre completo">*/
/* */
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-md-3" >*/
/*                             <div class="control-group">*/
/*                                 <label for="longitud" class="control-label">Cantidad que ingresa*/
/*                                 </label>*/
/*                                 <input  type="number" type="any" min="0" class="form-control" id="cantidadingresa" name="cantidadingresa" placeholder="0">*/
/*                                 <input  type="hidden" type="any" min="0" class="form-control" id="valor" name="valor" placeholder="0">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-md-6" >*/
/*                             <div class="control-group">*/
/*                                 <label for="longitud" class="control-label">Cantidad de Ingreso por Unidad de Medida*/
/*                                 </label>*/
/*                                 <input  type="number" type="any" min="0" class="form-control" id="cantidadunidad" name="cantidadunidad" placeholder="0" disabled>*/
/*                                 <input  type="hidden" type="any" min="0" class="form-control" id="cantidadunidadh" name="cantidadunidadh" >*/
/*                             </div>*/
/*                         </div>*/
/*                                                */
/*                     </div>*/
/*                     <div class="row">*/
/*                         <div class="col-md-3" >*/
/*                             <div class="control-group">*/
/*                                 <label for="formaentrada" class="control-label">Forma de Entrada*/
/*                                 </label>*/
/*                                 <select name="formaentrada" id="formaentrada" class="form-control" data-width="100%">*/
/*                                     <option value="0" >Elija la forma de entrada </option>*/
/*                                     {% for formaentrada in formaentrada %}*/
/*                                         <option value="{{formaentrada.id}}" >{{formaentrada.formanentrada}}</option>*/
/*                                     {% endfor %}*/
/*                                 </select>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-md-3" >*/
/*                             <div class="control-group">*/
/*                                 <label for="cajas" class="control-label">Número de Cajas*/
/*                                 </label>*/
/*                                 <input  type="number" step="any" min="0"  class="form-control" id="cajas" name="cajas" placeholder="0" >*/
/*                             </div>*/
/*                         </div> */
/*                         <div class="col-md-3" >*/
/*                             <div class="control-group">*/
/*                                 <label for="paquetes" class="control-label">Número de Paquetes*/
/*                                 </label>*/
/*                                 <input  type="number" step="any" min="0"  class="form-control" id="paquetes" name="paquetes" placeholder="0" >*/
/*                             </div>*/
/*                         </div> */
/*                         <div class="col-md-3" >*/
/*                             <div class="control-group">*/
/*                                 <label for="unidades" class="control-label">Número de Unidades*/
/*                                 </label>*/
/*                                 <input  type="number" step="any" min="0"  class="form-control" id="unidades" name="unidades" placeholder="0"  >*/
/*                             </div>*/
/*                         </div> */
/* */
/*                     </div>*/
/* */
/* */
/*                     <div class="row">*/
/*                         <div class="col-md-6" >*/
/*                             <div class="control-group">*/
/*                                 <label for="fecharegistro" class="control-label">Fecha de Registro de Entrada*/
/*                                 </label>*/
/*                                 <input  type="text"   class="form-control" id="fecharegistro" name="fecharegistro" value="{{date().format("d/m/Y")}}" disabled>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-md-6" >*/
/*                             <div class="control-group">*/
/*                                 <label for="almacen" class="control-label">Almacen de Resguardo*/
/*                                 </label>*/
/*                                 <select name="almacen" id="almacen" class="form-control" data-width="100%">*/
/*                                     <option value="0" >Elija el almacen </option>*/
/*                                     {% for almacen in almacen %}*/
/*                                         <option value="{{almacen.id}}" >{{almacen.almacen}}</option>*/
/*                                     {% endfor %}*/
/*                                 </select>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/*                     <div class="row">*/
/*                         <div class="col-md-12">*/
/*                             <input type="hidden" id="urlinsertarentrada" value="{{path('crear_entrada')}}"/>*/
/*                             <input   {% if 'ESCRITURA' in ppm %} id="btn_agregar_entrada" class="btn-lg col-md-2 text-center pull-right " type="submit"  {% else %}  class="btn-lg col-md-2 text-center pull-right boton_disable"  {% endif %} value="Agregar Producto" /> */
/*                         </div>*/
/*                     </div>*/
/* */
/*                 </form>*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="row">*/
/*             <div class="col-md-12 tableblanco " >*/
/*                 <form action="{{path('entradasfecha')}}"  method="post">*/
/*                 <table border="0" cellspacing="5" cellpadding="5" style="padding-bottom: 14px;">*/
/*                     <tbody><tr>*/
/*                             <td>Fecha de inicio:</td>*/
/*                             <td><input type="text" class="form-control datepicker"  id="min" name="min"   value=""></td>*/
/*                             */
/*                             <td>Fecha de término:</td>*/
/*                             <td><input type="text" class="form-control datepicker"  id="max" name="max"   value=""></td>*/
/*                            */
/*                             <td><input type="submit" value="Generar reporte" class="btn-lg linkload"></td>*/
/*                         </tr>*/
/*                         <tr>*/
/*                             <td colspan="2">{% if min is defined %} {{min|date('d/m/Y')}} {% endif %}</td>*/
/*                              <td colspan="2">{% if max is defined %} {{max|date('d/m/Y')}} {% endif %}</td>*/
/*                         </tr>*/
/*                     </tbody></table>*/
/*                 </form>*/
/*             </div>*/
/*             <div class="col-md-12 tableblanco" >*/
/*                 <table id="tabla_entradas" class="display nowrap table table-striped  " cellspacing="0" width="100%">*/
/*                     <thead>*/
/*                         <tr>*/
/*                             <th class="text-center">Entrada</th>*/
/*                             <th class="text-center">Fecha de Registro</th>*/
/*                             <th class="text-center">Producto</th> */
/*                             <th class="text-center">Modelo</th>*/
/*                             <th class="text-center">Color</th>*/
/*                             <th class="text-center">Ancho</th>*/
/*                             <th class="text-center">Largo</th>*/
/*                             <th class="text-center">Peso</th>*/
/*                             <th class="text-center">Número</th> */
/* */
/*                             <th class="text-center">Caja</th>*/
/*                             <th class="text-center">Paquete</th>*/
/*                             <th class="text-center">Unidades</th>*/
/*                             <th class="text-center">Total Ingreso Unidades</th>*/
/*                             <th class="text-center">Total Ingreso p/u de medida</th>*/
/*                             <th class="text-center">Código(s)</th>*/
/* */
/*                         </tr>*/
/*                     </thead>*/
/*                     <tbody>*/
/*                         {% for entradas in entradas %} */
/*                             <tr>*/
/*                                 <td class="text-center">{{entradas.id}}</td>*/
/*                                 <td class="text-center">{{entradas.fecharegistro|date('d/m/Y')}}</td>*/
/*                                 <td class="text-center">{{entradas.producto.nombre}}</td>*/
/*                                 <td class="text-center">{{entradas.producto.modelo.nombre}}</td>*/
/*                                 <td class="text-center">{{entradas.producto.color.nombre}}</td>*/
/*                                 <td class="text-center">{{entradas.ancho}}</td>*/
/*                                 <td class="text-center">{{entradas.longitud}}</td>*/
/*                                 <td class="text-center">{{entradas.peso}}</td>*/
/*                                 <td class="text-center">{{entradas.numero}}</td>*/
/* */
/*                                 <td class="text-center">{{entradas.cajas}}</td>*/
/*                                 <td class="text-center">{{entradas.paquetes}}</td>*/
/*                                 <td class="text-center">{{entradas.unidades}}</td>*/
/*                                 <td class="text-center">{{entradas.cantidadingresa}}</td>*/
/*                                 <td class="text-center">{{entradas.cantidadunidad}} </td>*/
/* */
/*                                 <td class="text-center">*/
/*                                     <button class="btn btn-lg-tabla detalle_entradas" type="button" data-toggle="modal" data-path="{{path('detalle_entrada')}}" data-id="{{entradas.id}}" data-target="#detalleentradas" > Detalles    </button>*/
/*                                 </td>*/
/* */
/* */
/*                             </tr>*/
/*                         {% endfor %}*/
/*                     </tbody>*/
/*                 </table>*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="row">*/
/*             {% include "AdminBundle:entradas:detalleentradas.html.twig" %}*/
/* */
/* */
/*         </div>*/
/*     </div>*/
/*             */
/*             <script>*/
/*   */
/*     $.datepicker.regional['es'] = {*/
/*         closeText: 'Cerrar',*/
/*         prevText: '< Ant',*/
/*         nextText: 'Sig >',*/
/*         currentText: 'Hoy',*/
/*         monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],*/
/*         monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],*/
/*         dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],*/
/*         dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],*/
/*         dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],*/
/*         weekHeader: 'Sm',*/
/*         dateFormat: 'dd/mm/yy',*/
/*         firstDay: 1,*/
/*         isRTL: false,*/
/*         showMonthAfterYear: false,*/
/*         yearSuffix: ''*/
/*     };*/
/*     $.datepicker.setDefaults($.datepicker.regional['es']);*/
/*     $(function () {*/
/*         $(".datepicker").datepicker();*/
/*         */
/*     });*/
/* </script>*/
/*             */
/* {% endblock %}*/
