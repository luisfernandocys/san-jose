<?php

/* AdminBundle:Productos:productos.html.twig */
class __TwigTemplate_446107981abc560c827863c8d86bd929b6494eb482aaa234beb3f06fbaa914bd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "AdminBundle:Productos:productos.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'header' => array($this, 'block_header'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_51b2f815f9362ec2fd788dc1a1f790a234667dd8d9467d18de67f3aa068bccec = $this->env->getExtension("native_profiler");
        $__internal_51b2f815f9362ec2fd788dc1a1f790a234667dd8d9467d18de67f3aa068bccec->enter($__internal_51b2f815f9362ec2fd788dc1a1f790a234667dd8d9467d18de67f3aa068bccec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AdminBundle:Productos:productos.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_51b2f815f9362ec2fd788dc1a1f790a234667dd8d9467d18de67f3aa068bccec->leave($__internal_51b2f815f9362ec2fd788dc1a1f790a234667dd8d9467d18de67f3aa068bccec_prof);

    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        $__internal_1bdbd56b639f28221f50a9b992de4027bbe69d90c6c6c8e9ada29ae1ec498417 = $this->env->getExtension("native_profiler");
        $__internal_1bdbd56b639f28221f50a9b992de4027bbe69d90c6c6c8e9ada29ae1ec498417->enter($__internal_1bdbd56b639f28221f50a9b992de4027bbe69d90c6c6c8e9ada29ae1ec498417_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 3
        echo "    Productos
";
        
        $__internal_1bdbd56b639f28221f50a9b992de4027bbe69d90c6c6c8e9ada29ae1ec498417->leave($__internal_1bdbd56b639f28221f50a9b992de4027bbe69d90c6c6c8e9ada29ae1ec498417_prof);

    }

    // line 5
    public function block_header($context, array $blocks = array())
    {
        $__internal_33b4fd633683bb4a38b6536ac8c6e93ba34a2b8c363ad5a797e40f4f4c81032c = $this->env->getExtension("native_profiler");
        $__internal_33b4fd633683bb4a38b6536ac8c6e93ba34a2b8c363ad5a797e40f4f4c81032c->enter($__internal_33b4fd633683bb4a38b6536ac8c6e93ba34a2b8c363ad5a797e40f4f4c81032c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 6
        echo "
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/admin/js/productos.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/admin/js/productosfotos.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/JsBarcode.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/jQuery.print.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/CODE128.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/CODE128.js"), "html", null, true);
        echo "\"></script>
    <script src=\"https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js\"></script>
    <script src=\"https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js\"></script>
    <script src=\"https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js\"></script>
    <script src=\"https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js\"></script>
    <script src=\"https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js\"></script>
    <script src=\"https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js\"></script>
    <script src=\"https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js\"></script>

    <link rel=\"stylesheet\" type=\"text/css\" href=\"https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css\">


";
        
        $__internal_33b4fd633683bb4a38b6536ac8c6e93ba34a2b8c363ad5a797e40f4f4c81032c->leave($__internal_33b4fd633683bb4a38b6536ac8c6e93ba34a2b8c363ad5a797e40f4f4c81032c_prof);

    }

    // line 29
    public function block_body($context, array $blocks = array())
    {
        $__internal_d2140cdda13ca59270213685ed5e1a140e972142fa80564fa4b70f0412e2da56 = $this->env->getExtension("native_profiler");
        $__internal_d2140cdda13ca59270213685ed5e1a140e972142fa80564fa4b70f0412e2da56->enter($__internal_d2140cdda13ca59270213685ed5e1a140e972142fa80564fa4b70f0412e2da56_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 30
        echo "    <div class=\"container\">
        <div class=\"row\">

            <h1 class=\"text-center header\">Registro de Productos</h1>
            ";
        // line 34
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 35
            echo "
                <div class=\"alert alert-success\">
                    ";
            // line 37
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "
                </div>

            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 41
        echo "
            ";
        // line 42
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "warning"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 43
            echo "
                <div class=\"alert alert-danger\">
                    ";
            // line 45
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "
                </div>

            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "        </div>
    </div>

    <div class=\"container main\">

        <div class=\"row\">
            <div class=\"col-md-2 col-md-offset-5 btn btn-lg\" id=\"divnuevoproducto\">Nuevo Producto</div>
            <div class=\"col-md-12\" id=\"nuevoproductodiv\" style=\"display: none;\">
                <form role=\"form\" id=\"productoform\" method=\"POST\" enctype=\"multipart/form-data\" action=\"";
        // line 57
        echo $this->env->getExtension('routing')->getPath("crear_producto");
        echo "\">

                    <div class=\"row\">
                        <div class=\"col-md-12\">

                            <fieldset>
                                <legend>Usuario que captura</legend>
                            </fieldset>
                        </div>
                        <div class=\"col-md-6\">
                            <div class=\"form-group\">

                                <input type=\"text\" class=\"form-control\" disabled id=\"usuario\" name=\"usuario\" placeholder=\"Usuario que captura\" value=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "nombre", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "apellidop", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "apellidom", array()), "html", null, true);
        echo "\">
                            </div>
                        </div>
                        <div class=\"col-md-6\">
                            <div class=\"form-group\">

                                <input type=\"text\" class=\"form-control\" disabled id=\"puesto\" name=\"puesto\" placeholder=\"Puesto del usuario\" value=\"Puesto: ";
        // line 75
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "puesto", array()), "html", null, true);
        echo "\">
                            </div>
                        </div>


                    </div>

                    <!--<div class=\"row\">
                        <div class=\"col-md-12\">

                            <fieldset>
                                <legend>Proveedores</legend>
                            </fieldset>
                        </div>
                    ";
        // line 89
        $this->loadTemplate("AdminBundle:proveedores:proveedores.html.twig", "AdminBundle:Productos:productos.html.twig", 89)->display($context);
        // line 90
        echo "

                </div>-->

                    <div class=\"row\">
                        <div class=\"col-md-12\">

                            <fieldset>
                                <legend>Datos del Producto</legend>
                                <div class=\"control-group\">

                                </div>

                            </fieldset>

                        </div>

                        <div class=\"col-md-6 col-sm-6 col-xs-12 \">
                            <div class=\"form-group\">

                                <input type=\"text\" class=\"form-control\" id=\"producto\" name=\"producto\" placeholder=\"Nombre del producto\">
                            </div>
                        </div>
                        <div class=\"col-md-6 col-sm-6 col-xs-12 \">
                            <div class=\"form-group\">

                                <input type=\"text\" class=\"form-control\" id=\"otrosnombres\" name=\"otrosnombres\" placeholder=\"Otros nombres\">
                            </div>
                        </div>
                    </div>
                    <!--<div class=\"row\">
                        <div class=\"col-md-5\" >
                            <div class=\"control-group\">

                                <select name=\"claseproducto_ddl\" id=\"claseproducto_ddl\" class=\"form-control\" data-width=\"100%\" data-path=\"";
        // line 124
        echo $this->env->getExtension('routing')->getPath("clase_tipos");
        echo "\">
                                    <option value=\"99\" >Default - Clase </option>
                    ";
        // line 126
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["clase"]);
        foreach ($context['_seq'] as $context["_key"] => $context["clase"]) {
            // line 127
            echo "                        <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["clase"], "id", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["clase"], "nombre", array()), "html", null, true);
            echo " </option>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['clase'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 129
        echo "                </select>

            </div>
        </div>
        <div class=\"col-md-1\"><img src=\"";
        // line 133
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("imagenes/loadingddl.gif"), "html", null, true);
        echo "\" id=\"loadingddl\" class=\"\" style=\"width:20px;height: 20px; display: none;\"/>  </div>
        <div class=\"col-md-6\" style=\"\" id=\"div_tipoproducto_ddl\"  >
            <div class=\"control-group\">

                <select name=\"tipoproducto_ddl\" id=\"tipoproducto_ddl\" class=\"form-control\" data-width=\"100%\" >
                    <option value=\"999\" >Default - Tipo </option>

                </select>


            </div>
        </div>

    </div>-->
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <div class=\"control-group\">
                                <input type=\"text\" class=\"form-control\" id=\"marca\" name=\"marca\" placeholder=\"Marca\">
                            </div>
                        </div>
                        <div class=\"col-md-3\">
                            <div class=\"control-group\">
                                <select name=\"modelo\" id=\"modelo\" class=\"form-control\" data-width=\"100%\" >
                                    <option value=\"0\" >Elige el modelo del Producto </option>
                                    ";
        // line 157
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["modelo"]);
        foreach ($context['_seq'] as $context["_key"] => $context["modelo"]) {
            // line 158
            echo "                                        <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["modelo"], "id", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["modelo"], "nombre", array()), "html", null, true);
            echo " </option>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['modelo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 160
        echo "                                    <option value=\"9999\" > - Agregar un nuevo Modelo </option>
                                </select>

                            </div>
                            <div class=\"control-group\" id=\"divnuevomodelo\" style=\"display:none;\">
                                <input type=\"text\" class=\"form-control\" id=\"modelonuevo\" name=\"modelonuevo\" placeholder=\"Ingresa el nuevo modelo\">
                                <img src=\"";
        // line 166
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("imagenes/loadingddl.gif"), "html", null, true);
        echo "\" id=\"loadingmodelo\" class=\"\" style=\"width:20px;height: 20px; display: none;\" />
                                <input   ";
        // line 167
        if (twig_in_filter("ESCRITURA", (isset($context["ppm"]) ? $context["ppm"] : $this->getContext($context, "ppm")))) {
            echo " id=\"btn_agregar_modelo\" class=\"btn-lg col-md-12 text-center  \" data-path=\"";
            echo $this->env->getExtension('routing')->getPath("agregar_modelo");
            echo "\" type=\"submit\"  ";
        } else {
            echo "  class=\"btn-lg col-md-12 text-center  boton_disable\"  ";
        }
        echo " value=\"Agregar Modelo\" /> 
                            </div>
                        </div>
                        <!--                        <div class=\"col-md-3\">
                                                    <div class=\"control-group\">
                                                        <input type=\"number\" step=\"any\" min=\"0\" class=\"form-control\"  id=\"numero\" name=\"numero\" placeholder=\"Número\">
                                                    </div>
                                                </div>
                                                <div class=\"col-md-3\">
                                                    <div class=\"control-group\">
                        
                                                        <select name=\"unidadmedida_ddl\" id=\"unidadmedida_ddl\" class=\"form-control\" data-width=\"100%\" >
                                                            <option value=\"0\" >Eliga la unidad de medida </option>
                        ";
        // line 180
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["unidad"]);
        foreach ($context['_seq'] as $context["_key"] => $context["unidad"]) {
            // line 181
            echo "                            <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["unidad"], "id", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["unidad"], "nombre", array()), "html", null, true);
            echo " </option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['unidad'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 183
        echo "                    </select>

                </div>
            </div>-->
                        <div class=\"col-md-6\">
                            <div class=\"control-group\">

                                <select name=\"color\" id=\"color\" class=\"form-control\" data-width=\"100%\" >
                                    <option value=\"0\" >Elige el color del Producto </option>
                                    ";
        // line 192
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["color"]);
        foreach ($context['_seq'] as $context["_key"] => $context["color"]) {
            // line 193
            echo "                                        <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["color"], "id", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["color"], "nombre", array()), "html", null, true);
            echo " </option>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['color'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 195
        echo "                                    <option value=\"9999\" > - Agregar un nuevo Color </option>

                                </select>
                            </div>
                            <div class=\"control-group\" id=\"divnuevocolor\" style=\"display:none;\">
                                <input type=\"text\" class=\"form-control\" id=\"colornuevo\" name=\"colornuevo\" placeholder=\"Ingresa el nuevo color\">
                                <img src=\"";
        // line 201
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("imagenes/loadingddl.gif"), "html", null, true);
        echo "\" id=\"loadingcolor\" class=\"\" style=\"width:20px;height: 20px; display: none;\" />
                                <input   ";
        // line 202
        if (twig_in_filter("ESCRITURA", (isset($context["ppm"]) ? $context["ppm"] : $this->getContext($context, "ppm")))) {
            echo " id=\"btn_agregar_color\" class=\"btn-lg col-md-12 text-center  \" data-path=\"";
            echo $this->env->getExtension('routing')->getPath("agregar_color");
            echo "\" type=\"submit\"  ";
        } else {
            echo "  class=\"btn-lg col-md-12 text-center  boton_disable\"  ";
        }
        echo " value=\"Agregar Color\" /> 
                            </div>
                        </div>
                    </div>

                    <div class=\"row\">
                        <!--  <div class=\"col-md-3\">
                              <div class=\"control-group\">
                                  Peso en Kg.
                                  <input  type=\"number\" step=\"any\" min=\"0\" value=\"0\" class=\"form-control\" id=\"peso\" name=\"peso\" placeholder=\"Peso en kg.\">
                              </div>
                          </div>
                          <div class=\"col-md-3\">
                              <div class=\"control-group\">
                                  Ancho en cm.
                                  <input  type=\"number\" step=\"any\" min=\"0\" value=\"0\" class=\"form-control\" id=\"ancho\" name=\"ancho\" placeholder=\"Ancho en cm.\">
                              </div>
                          </div>
                          <div class=\"col-md-3\">
                              <div class=\"control-group\">
                                  Longitud en cm.
                                  <input  type=\"number\" step=\"any\" min=\"0\" value=\"0\"   class=\"form-control\" id=\"longitud\" name=\"longitud\" placeholder=\"Longitud en cm.\">
                              </div>
                          </div>-->

                    </div>

                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <div class=\"control-group\">

                                <select name=\"tipotejido_ddl\" id=\"tipotejido_ddl\" class=\"form-control\" data-width=\"100%\" >
                                    <option value=\"0\" >Eliga el tipo de tejido </option>
                                    ";
        // line 235
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["tipot"]);
        foreach ($context['_seq'] as $context["_key"] => $context["tipot"]) {
            // line 236
            echo "                                        <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["tipot"], "id", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["tipot"], "nombre", array()), "html", null, true);
            echo " </option>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tipot'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 238
        echo "                                </select>

                            </div>
                        </div>
                        <div class=\"col-md-3\">
                            <div class=\"control-group\">
                                <input  type=\"text\"  class=\"form-control\" id=\"densidad\" name=\"densidad\" placeholder=\"Densidad.\">
                            </div>
                        </div>
                        <div class=\"col-md-3\">
                            <div class=\"control-group\">
                                <input  type=\"text\"  class=\"form-control\" id=\"hilados\" name=\"hilados\" placeholder=\"Hilados.\">
                            </div>
                        </div>
                        <div class=\"col-md-3\">
                            <div class=\"control-group\">
                                <input  type=\"text\"  class=\"form-control\" id=\"fibras\" name=\"fibras\" placeholder=\"Fibras.\">
                            </div>
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"col-md-6\">
                            <div class=\"control-group\">
                                <input  type=\"text\" class=\"form-control\" id=\"composicion\" name=\"composicion\" placeholder=\"Composicion.\">
                            </div>
                        </div>
                        <div class=\"col-md-6\">
                            <div class=\"control-group\">
                                <input  type=\"text\" class=\"form-control\" id=\"usos\" name=\"usos\" placeholder=\"Principales usos.\">
                            </div>
                        </div>

                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-6 \">

                            <div class=\"input-group image-preview\">
                                <input type=\"text\" class=\"form-control image-preview-filename\"  disabled=\"disabled\" id=\"fototexto\" name=\"fototexto\"> <!-- don't give a name === doesn't send on POST/GET -->
                                <span class=\"input-group-btn\">
                                    <!-- image-preview-clear button -->
                                    <button type=\"button\" class=\"btn btn-default image-preview-clear\" style=\"display:none;\">
                                        <span class=\"glyphicon glyphicon-remove\"></span> Limpiar
                                    </button>
                                    <!-- image-preview-input -->
                                    <div class=\"btn btn-default image-preview-input\">
                                        <span class=\"glyphicon glyphicon-folder-open\"></span>
                                        <span class=\"image-preview-input-title\">Buscar Archivo</span>
                                        <input type=\"file\"  id=\"file_producto\" name=\"input-file-preview\" /> <!-- rename it -->
                                    </div>
                                </span>
                            </div><!-- /input-group image-preview [TO HERE]--> 

                        </div>
                        <div class=\"col-md-6\">
                            <div class=\"control-group\">
                                <textarea class=\"form-control\" rows=\"14\" id=\"adicional\" name=\"adicional\" placeholder=\"Información adicional\"></textarea>
                            </div>
                        </div>
                    </div>


                    <div class=\"row\">
                        <div class=\"col-md-12\">
                            <input type=\"hidden\" id=\"urlinsertarproducto\" value=\"";
        // line 302
        echo $this->env->getExtension('routing')->getPath("crear_producto");
        echo "\"/>
                            <input   ";
        // line 303
        if (twig_in_filter("ESCRITURA", (isset($context["ppm"]) ? $context["ppm"] : $this->getContext($context, "ppm")))) {
            echo " id=\"btn_agregar_producto\" class=\"btn-lg col-md-2 text-center pull-right \" type=\"submit\"  ";
        } else {
            echo "  class=\"btn-lg col-md-2 text-center pull-right boton_disable\"  ";
        }
        echo " value=\"Agregar Producto\" /> 
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <div class=\"row\">
            <h1 class=\"text-center header\">Lista de Productos</h1>
            <div class=\"col-md-12 tableblanco \" >
                <form action=\"";
        // line 313
        echo $this->env->getExtension('routing')->getPath("productosfecha");
        echo "\"  method=\"post\">
                <table border=\"0\" cellspacing=\"5\" cellpadding=\"5\" style=\"padding-bottom: 14px;\">
                    <tbody><tr>
                            <td>Fecha de inicio:</td>
                            <td><input type=\"text\" class=\"form-control datepicker\"  id=\"min\" name=\"min\"   value=\"\"></td>
                            
                            <td>Fecha de término:</td>
                            <td><input type=\"text\" class=\"form-control datepicker\"  id=\"max\" name=\"max\"   value=\"\"></td>
                           
                            <td><input type=\"submit\" value=\"Generar reporte\" class=\"btn-lg linkload\"></td>
                        </tr>
                        <tr>
                            <td colspan=\"2\">";
        // line 325
        if (array_key_exists("min", $context)) {
            echo " ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["min"]) ? $context["min"] : $this->getContext($context, "min")), "d/m/Y"), "html", null, true);
            echo " ";
        }
        echo "</td>
                             <td colspan=\"2\">";
        // line 326
        if (array_key_exists("max", $context)) {
            echo " ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["max"]) ? $context["max"] : $this->getContext($context, "max")), "d/m/Y"), "html", null, true);
            echo " ";
        }
        echo "</td>
                        </tr>
                    </tbody></table>
                </form>
            </div>
              <div class=\"col-md-12 tableblanco \" >
                <table id=\"tabla_productos\" class=\"display nowrap table table-striped  \" cellspacing=\"0\" width=\"100%\">
                    <thead>
                        <tr>
                            <th>Producto</th>
                            <th>Modelo</th>
                            <th>Color</th>
                            <!-- <th>Ancho</th>
                             <th>Largo</th>
                             <th>Peso</th>
                             <th>No.</th>
                             <th>Proveedor</th>-->
                            <th>Folio</th>
                            <th>Fecha</th>
                            <th>Acciones</th>


                        </tr>
                    </thead>
                    <tbody>


                        ";
        // line 353
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["productos"]);
        foreach ($context['_seq'] as $context["_key"] => $context["productos"]) {
            // line 354
            echo "                            <tr>
                                <td style=\"vertical-align: middle;\">";
            // line 355
            echo twig_escape_filter($this->env, $this->getAttribute($context["productos"], "nombre", array()), "html", null, true);
            echo "</td>
                                <td style=\"vertical-align: middle;\">";
            // line 356
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["productos"], "modelo", array()), "nombre", array()), "html", null, true);
            echo "</td>
                                <td style=\"vertical-align: middle;\">";
            // line 357
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["productos"], "color", array()), "nombre", array()), "html", null, true);
            echo "</td>


                                <td style=\"vertical-align: middle;\">";
            // line 360
            echo twig_escape_filter($this->env, $this->getAttribute($context["productos"], "folio", array()), "html", null, true);
            echo "</td>
                                <td style=\"vertical-align: middle;\">";
            // line 361
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["productos"], "fecha", array()), "d/m/Y"), "html", null, true);
            echo "</td>
                                <td class='text-center' style=\"vertical-align: middle;\">
                                    <button class=\"btn btn-lg-tabla detalle_producto\" type=\"button\" data-toggle=\"modal\" data-path=\"";
            // line 363
            echo $this->env->getExtension('routing')->getPath("detalle_producto");
            echo "\" data-id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["productos"], "id", array()), "html", null, true);
            echo "\" data-target=\"#detalleproductos\" > Detalles    </button>

                                </td>

                            </tr>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['productos'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 369
        echo "

                    </tbody>
                </table>
            </div>
        </div>

        <div class=\"row\">
            ";
        // line 377
        $this->loadTemplate("AdminBundle:Productos:detalleproductos.html.twig", "AdminBundle:Productos:productos.html.twig", 377)->display($context);
        // line 378
        echo "

        </div>



    </div>

    <!--  <table>
<tr>
<td style='padding:10px; text-align:center; font-size:15px; font-family:Arial,Helvetica;'>
       <a href='http://www.tec-it.com' title='Barcode Software by TEC-IT'>
               
       </a>
       <br/>
       <a href='http://www.tec-it.com' title='Barcode Software by TEC-IT'>Barcode Software</a>
</td>
<td>
   <div id=\"printable1\">

       <img src='http://barcode.tec-it.com/barcode.ashx?data=mi-info-aqui&code=Code128&dpi=96' alt='Barcode Generator TEC-IT'/>
   </div>
   <button class=\"print\" data-id=\"printable1\"> Print this </button>

</td>
</tr>
</table>-->




    <!-- <a id=\"download\" download=\"barcode.png\">Download as image</a>
      <div id=\"printable4\">
      <canvas id=\"demo\"  style=\"display: none;\"></canvas>
      <div id=\"aa\"></div>
      </div>
      <button class=\"print\" data-id=\"printable4\"> Print this </button>-->
    
    <script>
  
    \$.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '< Ant',
        nextText: 'Sig >',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    \$.datepicker.setDefaults(\$.datepicker.regional['es']);
    \$(function () {
        \$(\".datepicker\").datepicker();
        
    });
</script>
";
        
        $__internal_d2140cdda13ca59270213685ed5e1a140e972142fa80564fa4b70f0412e2da56->leave($__internal_d2140cdda13ca59270213685ed5e1a140e972142fa80564fa4b70f0412e2da56_prof);

    }

    public function getTemplateName()
    {
        return "AdminBundle:Productos:productos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  657 => 378,  655 => 377,  645 => 369,  631 => 363,  626 => 361,  622 => 360,  616 => 357,  612 => 356,  608 => 355,  605 => 354,  601 => 353,  567 => 326,  559 => 325,  544 => 313,  527 => 303,  523 => 302,  457 => 238,  446 => 236,  442 => 235,  400 => 202,  396 => 201,  388 => 195,  377 => 193,  373 => 192,  362 => 183,  351 => 181,  347 => 180,  325 => 167,  321 => 166,  313 => 160,  302 => 158,  298 => 157,  271 => 133,  265 => 129,  254 => 127,  250 => 126,  245 => 124,  209 => 90,  207 => 89,  190 => 75,  177 => 69,  162 => 57,  152 => 49,  142 => 45,  138 => 43,  134 => 42,  131 => 41,  121 => 37,  117 => 35,  113 => 34,  107 => 30,  101 => 29,  80 => 13,  76 => 12,  72 => 11,  68 => 10,  64 => 9,  59 => 7,  56 => 6,  50 => 5,  42 => 3,  36 => 2,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* {% block title %}*/
/*     Productos*/
/* {% endblock %}*/
/* {% block header %}*/
/* */
/*     <script src="{{ asset('bundles/admin/js/productos.js') }}"></script>*/
/* */
/*     <script src="{{ asset('bundles/admin/js/productosfotos.js') }}"></script>*/
/*     <script src="{{ asset('js/JsBarcode.js') }}"></script>*/
/*     <script src="{{ asset('js/jQuery.print.js') }}"></script>*/
/*     <script src="{{ asset('js/CODE128.js') }}"></script>*/
/*     <script src="{{ asset('js/CODE128.js') }}"></script>*/
/*     <script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>*/
/*     <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>*/
/*     <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>*/
/*     <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>*/
/*     <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>*/
/*     <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>*/
/*     <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>*/
/*     <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>*/
/* */
/*     <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">*/
/* */
/* */
/* {% endblock %}*/
/* */
/* */
/* {% block body %}*/
/*     <div class="container">*/
/*         <div class="row">*/
/* */
/*             <h1 class="text-center header">Registro de Productos</h1>*/
/*             {% for flashMessage in app.session.flashbag.get('notice') %}*/
/* */
/*                 <div class="alert alert-success">*/
/*                     {{ flashMessage }}*/
/*                 </div>*/
/* */
/*             {% endfor %}*/
/* */
/*             {% for flashMessage in app.session.flashbag.get('warning') %}*/
/* */
/*                 <div class="alert alert-danger">*/
/*                     {{ flashMessage }}*/
/*                 </div>*/
/* */
/*             {% endfor %}*/
/*         </div>*/
/*     </div>*/
/* */
/*     <div class="container main">*/
/* */
/*         <div class="row">*/
/*             <div class="col-md-2 col-md-offset-5 btn btn-lg" id="divnuevoproducto">Nuevo Producto</div>*/
/*             <div class="col-md-12" id="nuevoproductodiv" style="display: none;">*/
/*                 <form role="form" id="productoform" method="POST" enctype="multipart/form-data" action="{{path('crear_producto')}}">*/
/* */
/*                     <div class="row">*/
/*                         <div class="col-md-12">*/
/* */
/*                             <fieldset>*/
/*                                 <legend>Usuario que captura</legend>*/
/*                             </fieldset>*/
/*                         </div>*/
/*                         <div class="col-md-6">*/
/*                             <div class="form-group">*/
/* */
/*                                 <input type="text" class="form-control" disabled id="usuario" name="usuario" placeholder="Usuario que captura" value="{{app.user.nombre}} {{app.user.apellidop}} {{app.user.apellidom}}">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-md-6">*/
/*                             <div class="form-group">*/
/* */
/*                                 <input type="text" class="form-control" disabled id="puesto" name="puesto" placeholder="Puesto del usuario" value="Puesto: {{app.user.puesto}}">*/
/*                             </div>*/
/*                         </div>*/
/* */
/* */
/*                     </div>*/
/* */
/*                     <!--<div class="row">*/
/*                         <div class="col-md-12">*/
/* */
/*                             <fieldset>*/
/*                                 <legend>Proveedores</legend>*/
/*                             </fieldset>*/
/*                         </div>*/
/*                     {% include 'AdminBundle:proveedores:proveedores.html.twig' %}*/
/* */
/* */
/*                 </div>-->*/
/* */
/*                     <div class="row">*/
/*                         <div class="col-md-12">*/
/* */
/*                             <fieldset>*/
/*                                 <legend>Datos del Producto</legend>*/
/*                                 <div class="control-group">*/
/* */
/*                                 </div>*/
/* */
/*                             </fieldset>*/
/* */
/*                         </div>*/
/* */
/*                         <div class="col-md-6 col-sm-6 col-xs-12 ">*/
/*                             <div class="form-group">*/
/* */
/*                                 <input type="text" class="form-control" id="producto" name="producto" placeholder="Nombre del producto">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-md-6 col-sm-6 col-xs-12 ">*/
/*                             <div class="form-group">*/
/* */
/*                                 <input type="text" class="form-control" id="otrosnombres" name="otrosnombres" placeholder="Otros nombres">*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <!--<div class="row">*/
/*                         <div class="col-md-5" >*/
/*                             <div class="control-group">*/
/* */
/*                                 <select name="claseproducto_ddl" id="claseproducto_ddl" class="form-control" data-width="100%" data-path="{{path('clase_tipos')}}">*/
/*                                     <option value="99" >Default - Clase </option>*/
/*                     {% for clase in clase %}*/
/*                         <option value="{{clase.id}}" >{{clase.nombre}} </option>*/
/*                     {% endfor %}*/
/*                 </select>*/
/* */
/*             </div>*/
/*         </div>*/
/*         <div class="col-md-1"><img src="{{asset('imagenes/loadingddl.gif')}}" id="loadingddl" class="" style="width:20px;height: 20px; display: none;"/>  </div>*/
/*         <div class="col-md-6" style="" id="div_tipoproducto_ddl"  >*/
/*             <div class="control-group">*/
/* */
/*                 <select name="tipoproducto_ddl" id="tipoproducto_ddl" class="form-control" data-width="100%" >*/
/*                     <option value="999" >Default - Tipo </option>*/
/* */
/*                 </select>*/
/* */
/* */
/*             </div>*/
/*         </div>*/
/* */
/*     </div>-->*/
/*                     <div class="row">*/
/*                         <div class="col-md-3">*/
/*                             <div class="control-group">*/
/*                                 <input type="text" class="form-control" id="marca" name="marca" placeholder="Marca">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-md-3">*/
/*                             <div class="control-group">*/
/*                                 <select name="modelo" id="modelo" class="form-control" data-width="100%" >*/
/*                                     <option value="0" >Elige el modelo del Producto </option>*/
/*                                     {% for modelo in modelo %}*/
/*                                         <option value="{{modelo.id}}" >{{modelo.nombre}} </option>*/
/*                                     {% endfor %}*/
/*                                     <option value="9999" > - Agregar un nuevo Modelo </option>*/
/*                                 </select>*/
/* */
/*                             </div>*/
/*                             <div class="control-group" id="divnuevomodelo" style="display:none;">*/
/*                                 <input type="text" class="form-control" id="modelonuevo" name="modelonuevo" placeholder="Ingresa el nuevo modelo">*/
/*                                 <img src="{{asset('imagenes/loadingddl.gif')}}" id="loadingmodelo" class="" style="width:20px;height: 20px; display: none;" />*/
/*                                 <input   {% if 'ESCRITURA' in ppm %} id="btn_agregar_modelo" class="btn-lg col-md-12 text-center  " data-path="{{path('agregar_modelo')}}" type="submit"  {% else %}  class="btn-lg col-md-12 text-center  boton_disable"  {% endif %} value="Agregar Modelo" /> */
/*                             </div>*/
/*                         </div>*/
/*                         <!--                        <div class="col-md-3">*/
/*                                                     <div class="control-group">*/
/*                                                         <input type="number" step="any" min="0" class="form-control"  id="numero" name="numero" placeholder="Número">*/
/*                                                     </div>*/
/*                                                 </div>*/
/*                                                 <div class="col-md-3">*/
/*                                                     <div class="control-group">*/
/*                         */
/*                                                         <select name="unidadmedida_ddl" id="unidadmedida_ddl" class="form-control" data-width="100%" >*/
/*                                                             <option value="0" >Eliga la unidad de medida </option>*/
/*                         {% for unidad in unidad %}*/
/*                             <option value="{{unidad.id}}" >{{unidad.nombre}} </option>*/
/*                         {% endfor %}*/
/*                     </select>*/
/* */
/*                 </div>*/
/*             </div>-->*/
/*                         <div class="col-md-6">*/
/*                             <div class="control-group">*/
/* */
/*                                 <select name="color" id="color" class="form-control" data-width="100%" >*/
/*                                     <option value="0" >Elige el color del Producto </option>*/
/*                                     {% for color in color %}*/
/*                                         <option value="{{color.id}}" >{{color.nombre}} </option>*/
/*                                     {% endfor %}*/
/*                                     <option value="9999" > - Agregar un nuevo Color </option>*/
/* */
/*                                 </select>*/
/*                             </div>*/
/*                             <div class="control-group" id="divnuevocolor" style="display:none;">*/
/*                                 <input type="text" class="form-control" id="colornuevo" name="colornuevo" placeholder="Ingresa el nuevo color">*/
/*                                 <img src="{{asset('imagenes/loadingddl.gif')}}" id="loadingcolor" class="" style="width:20px;height: 20px; display: none;" />*/
/*                                 <input   {% if 'ESCRITURA' in ppm %} id="btn_agregar_color" class="btn-lg col-md-12 text-center  " data-path="{{path('agregar_color')}}" type="submit"  {% else %}  class="btn-lg col-md-12 text-center  boton_disable"  {% endif %} value="Agregar Color" /> */
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <div class="row">*/
/*                         <!--  <div class="col-md-3">*/
/*                               <div class="control-group">*/
/*                                   Peso en Kg.*/
/*                                   <input  type="number" step="any" min="0" value="0" class="form-control" id="peso" name="peso" placeholder="Peso en kg.">*/
/*                               </div>*/
/*                           </div>*/
/*                           <div class="col-md-3">*/
/*                               <div class="control-group">*/
/*                                   Ancho en cm.*/
/*                                   <input  type="number" step="any" min="0" value="0" class="form-control" id="ancho" name="ancho" placeholder="Ancho en cm.">*/
/*                               </div>*/
/*                           </div>*/
/*                           <div class="col-md-3">*/
/*                               <div class="control-group">*/
/*                                   Longitud en cm.*/
/*                                   <input  type="number" step="any" min="0" value="0"   class="form-control" id="longitud" name="longitud" placeholder="Longitud en cm.">*/
/*                               </div>*/
/*                           </div>-->*/
/* */
/*                     </div>*/
/* */
/*                     <div class="row">*/
/*                         <div class="col-md-3">*/
/*                             <div class="control-group">*/
/* */
/*                                 <select name="tipotejido_ddl" id="tipotejido_ddl" class="form-control" data-width="100%" >*/
/*                                     <option value="0" >Eliga el tipo de tejido </option>*/
/*                                     {% for tipot in tipot %}*/
/*                                         <option value="{{tipot.id}}" >{{tipot.nombre}} </option>*/
/*                                     {% endfor %}*/
/*                                 </select>*/
/* */
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-md-3">*/
/*                             <div class="control-group">*/
/*                                 <input  type="text"  class="form-control" id="densidad" name="densidad" placeholder="Densidad.">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-md-3">*/
/*                             <div class="control-group">*/
/*                                 <input  type="text"  class="form-control" id="hilados" name="hilados" placeholder="Hilados.">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-md-3">*/
/*                             <div class="control-group">*/
/*                                 <input  type="text"  class="form-control" id="fibras" name="fibras" placeholder="Fibras.">*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <div class="row">*/
/*                         <div class="col-md-6">*/
/*                             <div class="control-group">*/
/*                                 <input  type="text" class="form-control" id="composicion" name="composicion" placeholder="Composicion.">*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-md-6">*/
/*                             <div class="control-group">*/
/*                                 <input  type="text" class="form-control" id="usos" name="usos" placeholder="Principales usos.">*/
/*                             </div>*/
/*                         </div>*/
/* */
/*                     </div>*/
/*                     <div class="row">*/
/*                         <div class="col-md-6 ">*/
/* */
/*                             <div class="input-group image-preview">*/
/*                                 <input type="text" class="form-control image-preview-filename"  disabled="disabled" id="fototexto" name="fototexto"> <!-- don't give a name === doesn't send on POST/GET -->*/
/*                                 <span class="input-group-btn">*/
/*                                     <!-- image-preview-clear button -->*/
/*                                     <button type="button" class="btn btn-default image-preview-clear" style="display:none;">*/
/*                                         <span class="glyphicon glyphicon-remove"></span> Limpiar*/
/*                                     </button>*/
/*                                     <!-- image-preview-input -->*/
/*                                     <div class="btn btn-default image-preview-input">*/
/*                                         <span class="glyphicon glyphicon-folder-open"></span>*/
/*                                         <span class="image-preview-input-title">Buscar Archivo</span>*/
/*                                         <input type="file"  id="file_producto" name="input-file-preview" /> <!-- rename it -->*/
/*                                     </div>*/
/*                                 </span>*/
/*                             </div><!-- /input-group image-preview [TO HERE]--> */
/* */
/*                         </div>*/
/*                         <div class="col-md-6">*/
/*                             <div class="control-group">*/
/*                                 <textarea class="form-control" rows="14" id="adicional" name="adicional" placeholder="Información adicional"></textarea>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/* */
/* */
/*                     <div class="row">*/
/*                         <div class="col-md-12">*/
/*                             <input type="hidden" id="urlinsertarproducto" value="{{path('crear_producto')}}"/>*/
/*                             <input   {% if 'ESCRITURA' in ppm %} id="btn_agregar_producto" class="btn-lg col-md-2 text-center pull-right " type="submit"  {% else %}  class="btn-lg col-md-2 text-center pull-right boton_disable"  {% endif %} value="Agregar Producto" /> */
/*                         </div>*/
/*                     </div>*/
/* */
/*                 </form>*/
/*             </div>*/
/*         </div>*/
/*         <div class="row">*/
/*             <h1 class="text-center header">Lista de Productos</h1>*/
/*             <div class="col-md-12 tableblanco " >*/
/*                 <form action="{{path('productosfecha')}}"  method="post">*/
/*                 <table border="0" cellspacing="5" cellpadding="5" style="padding-bottom: 14px;">*/
/*                     <tbody><tr>*/
/*                             <td>Fecha de inicio:</td>*/
/*                             <td><input type="text" class="form-control datepicker"  id="min" name="min"   value=""></td>*/
/*                             */
/*                             <td>Fecha de término:</td>*/
/*                             <td><input type="text" class="form-control datepicker"  id="max" name="max"   value=""></td>*/
/*                            */
/*                             <td><input type="submit" value="Generar reporte" class="btn-lg linkload"></td>*/
/*                         </tr>*/
/*                         <tr>*/
/*                             <td colspan="2">{% if min is defined %} {{min|date('d/m/Y')}} {% endif %}</td>*/
/*                              <td colspan="2">{% if max is defined %} {{max|date('d/m/Y')}} {% endif %}</td>*/
/*                         </tr>*/
/*                     </tbody></table>*/
/*                 </form>*/
/*             </div>*/
/*               <div class="col-md-12 tableblanco " >*/
/*                 <table id="tabla_productos" class="display nowrap table table-striped  " cellspacing="0" width="100%">*/
/*                     <thead>*/
/*                         <tr>*/
/*                             <th>Producto</th>*/
/*                             <th>Modelo</th>*/
/*                             <th>Color</th>*/
/*                             <!-- <th>Ancho</th>*/
/*                              <th>Largo</th>*/
/*                              <th>Peso</th>*/
/*                              <th>No.</th>*/
/*                              <th>Proveedor</th>-->*/
/*                             <th>Folio</th>*/
/*                             <th>Fecha</th>*/
/*                             <th>Acciones</th>*/
/* */
/* */
/*                         </tr>*/
/*                     </thead>*/
/*                     <tbody>*/
/* */
/* */
/*                         {% for productos in productos %}*/
/*                             <tr>*/
/*                                 <td style="vertical-align: middle;">{{productos.nombre}}</td>*/
/*                                 <td style="vertical-align: middle;">{{productos.modelo.nombre}}</td>*/
/*                                 <td style="vertical-align: middle;">{{productos.color.nombre}}</td>*/
/* */
/* */
/*                                 <td style="vertical-align: middle;">{{productos.folio}}</td>*/
/*                                 <td style="vertical-align: middle;">{{productos.fecha|date('d/m/Y')}}</td>*/
/*                                 <td class='text-center' style="vertical-align: middle;">*/
/*                                     <button class="btn btn-lg-tabla detalle_producto" type="button" data-toggle="modal" data-path="{{path('detalle_producto')}}" data-id="{{productos.id}}" data-target="#detalleproductos" > Detalles    </button>*/
/* */
/*                                 </td>*/
/* */
/*                             </tr>*/
/*                         {% endfor %}*/
/* */
/* */
/*                     </tbody>*/
/*                 </table>*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="row">*/
/*             {% include "AdminBundle:Productos:detalleproductos.html.twig" %}*/
/* */
/* */
/*         </div>*/
/* */
/* */
/* */
/*     </div>*/
/* */
/*     <!--  <table>*/
/* <tr>*/
/* <td style='padding:10px; text-align:center; font-size:15px; font-family:Arial,Helvetica;'>*/
/*        <a href='http://www.tec-it.com' title='Barcode Software by TEC-IT'>*/
/*                */
/*        </a>*/
/*        <br/>*/
/*        <a href='http://www.tec-it.com' title='Barcode Software by TEC-IT'>Barcode Software</a>*/
/* </td>*/
/* <td>*/
/*    <div id="printable1">*/
/* */
/*        <img src='http://barcode.tec-it.com/barcode.ashx?data=mi-info-aqui&code=Code128&dpi=96' alt='Barcode Generator TEC-IT'/>*/
/*    </div>*/
/*    <button class="print" data-id="printable1"> Print this </button>*/
/* */
/* </td>*/
/* </tr>*/
/* </table>-->*/
/* */
/* */
/* */
/* */
/*     <!-- <a id="download" download="barcode.png">Download as image</a>*/
/*       <div id="printable4">*/
/*       <canvas id="demo"  style="display: none;"></canvas>*/
/*       <div id="aa"></div>*/
/*       </div>*/
/*       <button class="print" data-id="printable4"> Print this </button>-->*/
/*     */
/*     <script>*/
/*   */
/*     $.datepicker.regional['es'] = {*/
/*         closeText: 'Cerrar',*/
/*         prevText: '< Ant',*/
/*         nextText: 'Sig >',*/
/*         currentText: 'Hoy',*/
/*         monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],*/
/*         monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],*/
/*         dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],*/
/*         dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],*/
/*         dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],*/
/*         weekHeader: 'Sm',*/
/*         dateFormat: 'dd/mm/yy',*/
/*         firstDay: 1,*/
/*         isRTL: false,*/
/*         showMonthAfterYear: false,*/
/*         yearSuffix: ''*/
/*     };*/
/*     $.datepicker.setDefaults($.datepicker.regional['es']);*/
/*     $(function () {*/
/*         $(".datepicker").datepicker();*/
/*         */
/*     });*/
/* </script>*/
/* {% endblock %}*/
/* */
