<?php

/* @Twig/Exception/exception.html.twig */
class __TwigTemplate_a10e65bc1006fc4b419b0fb9f3e7b9eb9cac6e9c6168a7b254196b3cad1d2aa4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7223ec7cd63cc59c360c30b65efed570a755db5bdcfd668f3dd19a27a883724f = $this->env->getExtension("native_profiler");
        $__internal_7223ec7cd63cc59c360c30b65efed570a755db5bdcfd668f3dd19a27a883724f->enter($__internal_7223ec7cd63cc59c360c30b65efed570a755db5bdcfd668f3dd19a27a883724f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.html.twig"));

        // line 2
        echo "<h1>Algo malo acaba de ocurrir.</h1>
<h2>Error:";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text"))), "html", null, true);
        echo "</h2>
<h2>";
        // line 4
        echo $this->env->getExtension('code')->formatFileFromText(nl2br(twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true)));
        echo "</h2>

<h1><a href=\"";
        // line 6
        echo $this->env->getExtension('routing')->getPath("admin_homepage");
        echo "\">Regresa al menú principal</a></h1>";
        
        $__internal_7223ec7cd63cc59c360c30b65efed570a755db5bdcfd668f3dd19a27a883724f->leave($__internal_7223ec7cd63cc59c360c30b65efed570a755db5bdcfd668f3dd19a27a883724f_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  36 => 6,  31 => 4,  25 => 3,  22 => 2,);
    }
}
/* {# empty Twig template #}*/
/* <h1>Algo malo acaba de ocurrir.</h1>*/
/* <h2>Error:{{status_code}} {{status_text|upper}}</h2>*/
/* <h2>{{exception.message|nl2br|format_file_from_text}}</h2>*/
/* */
/* <h1><a href="{{path('admin_homepage')}}">Regresa al menú principal</a></h1>*/
