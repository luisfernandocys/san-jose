<?php

/* AdminBundle:proveedores:proveedores.html.twig */
class __TwigTemplate_9f39112a0f51c88476dc09a3ef791a69f5f9d04d984c4d2f67420d67257d630f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_472a886b179c4d8ed4c3bcc11f5035f22cac5160e6d004352b2587ac982aa43e = $this->env->getExtension("native_profiler");
        $__internal_472a886b179c4d8ed4c3bcc11f5035f22cac5160e6d004352b2587ac982aa43e->enter($__internal_472a886b179c4d8ed4c3bcc11f5035f22cac5160e6d004352b2587ac982aa43e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AdminBundle:proveedores:proveedores.html.twig"));

        // line 2
        echo "
<div id=\"proveedor1\">
    <div class=\"col-md-6 col-sm-6 col-xs-12\">
        <div class=\"control-group\">
            Proveedor:
            <select name=\"proveedor_ddl1\" id=\"proveedor_ddl1\" class=\"form-control\" data-width=\"100%\" >
                <option value=\"0\" >Eliga el proveedor 1</option>
                ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["proveedor"]);
        foreach ($context['_seq'] as $context["_key"] => $context["proveedor"]) {
            // line 10
            echo "                    <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["proveedor"], "id", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["proveedor"], "empresa", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["proveedor"], "apellidos", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["proveedor"], "nombre", array()), "html", null, true);
            echo "</option>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['proveedor'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "            </select>
        </div>
    </div>
    <div class=\"col-md-2 col-sm-2 col-xs-4\">
        <div class=\"control-group\">
            Precio 1:
            <input  type=\"number\" step=\"any\" min=\"0\" value=\"0\" class=\"form-control\" id=\"precio11\" name=\"precio11\" placeholder=\"precio 1\">
        </div>
    </div>
    <div class=\"col-md-2 col-sm-2 col-xs-4\">
        <div class=\"control-group\">
            Precio 2:
            <input  type=\"number\" step=\"any\" min=\"0\" value=\"0\" class=\"form-control\" id=\"precio12\" name=\"precio12\" placeholder=\"precio 2\">
        </div>
    </div>
    <div class=\"col-md-2 col-sm-2 col-xs-4\">
        <div class=\"control-group\">
            Precio 3:
            <input  type=\"number\" step=\"any\" min=\"0\" value=\"0\" class=\"form-control\" id=\"precio13\" name=\"precio13\" placeholder=\"precio 3\">
        </div>
    </div>
    <label style=\"color:green; cursor: pointer; display:none;\"  id=\"addp1\">Agregar otro proveedor</label>
</div>


<div id=\"proveedor2\" class=\"col-md-12\" style=\"display:none;\">
    <div class=\"col-md-6 col-sm-6 col-xs-6\">
        <div class=\"control-group\">
            <select name=\"proveedor_ddl2\" id=\"proveedor_ddl2\" class=\"form-control\" data-width=\"100%\" >
                <option value=\"0\" >Eliga el proveedor 2</option>
                ";
        // line 42
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["proveedor"]);
        foreach ($context['_seq'] as $context["_key"] => $context["proveedor"]) {
            // line 43
            echo "                    <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["proveedor"], "id", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["proveedor"], "empresa", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["proveedor"], "apellidos", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["proveedor"], "nombre", array()), "html", null, true);
            echo "</option>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['proveedor'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "            </select>
        </div>
    </div>
    <div class=\"col-md-2 col-sm-2 col-xs-2\">
        <div class=\"control-group\">
            <input  type=\"number\" step=\"any\" min=\"0\" value=\"0\" class=\"form-control\" id=\"precio21\" name=\"precio21\" placeholder=\"precio 1\">
        </div>
    </div>
    <div class=\"col-md-2 col-sm-2 col-xs-2\">
        <div class=\"control-group\">
            <input  type=\"number\" step=\"any\" min=\"0\" value=\"0\" class=\"form-control\" id=\"precio22\" name=\"precio22\" placeholder=\"precio 2\">
        </div>
    </div>
    <div class=\"col-md-2 col-sm-2 col-xs-2\">
        <div class=\"control-group\">
            <input  type=\"number\" step=\"any\" min=\"0\" value=\"0\" class=\"form-control\" id=\"precio23\" name=\"precio23\" placeholder=\"precio 3\">
        </div>
    </div>
    <label style=\"color:green; cursor: pointer;\" id=\"addp2\">Agregar otro proveedor</label>
</div>

<div id=\"proveedor3\" class=\"col-md-12\" style=\"display:none;\">
    <div class=\"col-md-6 col-sm-6 col-xs-6\">
        <div class=\"control-group\">
            <select name=\"proveedor_ddl3\" id=\"proveedor_ddl3\" class=\"form-control\" data-width=\"100%\" >
                <option value=\"0\" >Eliga el proveedor 3</option>
                ";
        // line 71
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["proveedor"]);
        foreach ($context['_seq'] as $context["_key"] => $context["proveedor"]) {
            // line 72
            echo "                    <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["proveedor"], "id", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["proveedor"], "empresa", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["proveedor"], "apellidos", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["proveedor"], "nombre", array()), "html", null, true);
            echo "</option>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['proveedor'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 74
        echo "            </select>
        </div>
    </div>
    <div class=\"col-md-2 col-sm-2 col-xs-2\">
        <div class=\"control-group\">
            <input  type=\"number\" step=\"any\" min=\"0\" value=\"0\" class=\"form-control\" id=\"precio31\" name=\"precio31\" placeholder=\"precio 1\">
        </div>
    </div>
    <div class=\"col-md-2 col-sm-2 col-xs-2\">
        <div class=\"control-group\">
            <input  type=\"number\" step=\"any\" min=\"0\" value=\"0\" class=\"form-control\" id=\"precio32\" name=\"precio32\" placeholder=\"precio 2\">
        </div>
    </div>
    <div class=\"col-md-2 col-sm-2 col-xs-2\">
        <div class=\"control-group\">
            <input  type=\"number\" step=\"any\" min=\"0\" value=\"0\" class=\"form-control\" id=\"precio33\" name=\"precio33\" placeholder=\"precio 3\">
        </div>
    </div>
    <label style=\"color:green; cursor: pointer;\" id=\"addp3\">Agregar otro proveedor</label>
</div>

<div id=\"proveedor4\" class=\"col-md-12\" style=\"display:none;\">
    <div class=\"col-md-6 col-sm-6 col-xs-6\">
        <div class=\"control-group\">
            <select name=\"proveedor_ddl4\" id=\"proveedor_ddl4\" class=\"form-control\" data-width=\"100%\" >
                <option value=\"0\" >Eliga el proveedor 4</option>
                ";
        // line 100
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["proveedor"]);
        foreach ($context['_seq'] as $context["_key"] => $context["proveedor"]) {
            // line 101
            echo "                    <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["proveedor"], "id", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["proveedor"], "empresa", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["proveedor"], "apellidos", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["proveedor"], "nombre", array()), "html", null, true);
            echo "</option>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['proveedor'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 103
        echo "            </select>
        </div>
    </div>
    <div class=\"col-md-2 col-sm-2 col-xs-2\">
        <div class=\"control-group\">
            <input  type=\"number\" step=\"any\" min=\"0\" value=\"0\"  class=\"form-control\" id=\"precio41\" name=\"precio41\" placeholder=\"precio 1\">
        </div>
    </div>
    <div class=\"col-md-2 col-sm-2 col-xs-2\">
        <div class=\"control-group\">
            <input  type=\"number\" step=\"any\" min=\"0\" value=\"0\" class=\"form-control\" id=\"precio42\" name=\"precio42\" placeholder=\"precio 2\">
        </div>
    </div>
    <div class=\"col-md-2 col-sm-2 col-xs-2\">
        <div class=\"control-group\">
            <input  type=\"number\" step=\"any\" min=\"0\" value=\"0\" class=\"form-control\" id=\"precio43\" name=\"precio43\" placeholder=\"precio 3\">
        </div>
    </div>

</div>";
        
        $__internal_472a886b179c4d8ed4c3bcc11f5035f22cac5160e6d004352b2587ac982aa43e->leave($__internal_472a886b179c4d8ed4c3bcc11f5035f22cac5160e6d004352b2587ac982aa43e_prof);

    }

    public function getTemplateName()
    {
        return "AdminBundle:proveedores:proveedores.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  195 => 103,  180 => 101,  176 => 100,  148 => 74,  133 => 72,  129 => 71,  101 => 45,  86 => 43,  82 => 42,  50 => 12,  35 => 10,  31 => 9,  22 => 2,);
    }
}
/* {# empty Twig template #}*/
/* */
/* <div id="proveedor1">*/
/*     <div class="col-md-6 col-sm-6 col-xs-12">*/
/*         <div class="control-group">*/
/*             Proveedor:*/
/*             <select name="proveedor_ddl1" id="proveedor_ddl1" class="form-control" data-width="100%" >*/
/*                 <option value="0" >Eliga el proveedor 1</option>*/
/*                 {% for proveedor in proveedor %}*/
/*                     <option value="{{proveedor.id}}" >{{proveedor.empresa}} - {{proveedor.apellidos}} {{proveedor.nombre}}</option>*/
/*                 {% endfor %}*/
/*             </select>*/
/*         </div>*/
/*     </div>*/
/*     <div class="col-md-2 col-sm-2 col-xs-4">*/
/*         <div class="control-group">*/
/*             Precio 1:*/
/*             <input  type="number" step="any" min="0" value="0" class="form-control" id="precio11" name="precio11" placeholder="precio 1">*/
/*         </div>*/
/*     </div>*/
/*     <div class="col-md-2 col-sm-2 col-xs-4">*/
/*         <div class="control-group">*/
/*             Precio 2:*/
/*             <input  type="number" step="any" min="0" value="0" class="form-control" id="precio12" name="precio12" placeholder="precio 2">*/
/*         </div>*/
/*     </div>*/
/*     <div class="col-md-2 col-sm-2 col-xs-4">*/
/*         <div class="control-group">*/
/*             Precio 3:*/
/*             <input  type="number" step="any" min="0" value="0" class="form-control" id="precio13" name="precio13" placeholder="precio 3">*/
/*         </div>*/
/*     </div>*/
/*     <label style="color:green; cursor: pointer; display:none;"  id="addp1">Agregar otro proveedor</label>*/
/* </div>*/
/* */
/* */
/* <div id="proveedor2" class="col-md-12" style="display:none;">*/
/*     <div class="col-md-6 col-sm-6 col-xs-6">*/
/*         <div class="control-group">*/
/*             <select name="proveedor_ddl2" id="proveedor_ddl2" class="form-control" data-width="100%" >*/
/*                 <option value="0" >Eliga el proveedor 2</option>*/
/*                 {% for proveedor in proveedor %}*/
/*                     <option value="{{proveedor.id}}" >{{proveedor.empresa}} - {{proveedor.apellidos}} {{proveedor.nombre}}</option>*/
/*                 {% endfor %}*/
/*             </select>*/
/*         </div>*/
/*     </div>*/
/*     <div class="col-md-2 col-sm-2 col-xs-2">*/
/*         <div class="control-group">*/
/*             <input  type="number" step="any" min="0" value="0" class="form-control" id="precio21" name="precio21" placeholder="precio 1">*/
/*         </div>*/
/*     </div>*/
/*     <div class="col-md-2 col-sm-2 col-xs-2">*/
/*         <div class="control-group">*/
/*             <input  type="number" step="any" min="0" value="0" class="form-control" id="precio22" name="precio22" placeholder="precio 2">*/
/*         </div>*/
/*     </div>*/
/*     <div class="col-md-2 col-sm-2 col-xs-2">*/
/*         <div class="control-group">*/
/*             <input  type="number" step="any" min="0" value="0" class="form-control" id="precio23" name="precio23" placeholder="precio 3">*/
/*         </div>*/
/*     </div>*/
/*     <label style="color:green; cursor: pointer;" id="addp2">Agregar otro proveedor</label>*/
/* </div>*/
/* */
/* <div id="proveedor3" class="col-md-12" style="display:none;">*/
/*     <div class="col-md-6 col-sm-6 col-xs-6">*/
/*         <div class="control-group">*/
/*             <select name="proveedor_ddl3" id="proveedor_ddl3" class="form-control" data-width="100%" >*/
/*                 <option value="0" >Eliga el proveedor 3</option>*/
/*                 {% for proveedor in proveedor %}*/
/*                     <option value="{{proveedor.id}}" >{{proveedor.empresa}} - {{proveedor.apellidos}} {{proveedor.nombre}}</option>*/
/*                 {% endfor %}*/
/*             </select>*/
/*         </div>*/
/*     </div>*/
/*     <div class="col-md-2 col-sm-2 col-xs-2">*/
/*         <div class="control-group">*/
/*             <input  type="number" step="any" min="0" value="0" class="form-control" id="precio31" name="precio31" placeholder="precio 1">*/
/*         </div>*/
/*     </div>*/
/*     <div class="col-md-2 col-sm-2 col-xs-2">*/
/*         <div class="control-group">*/
/*             <input  type="number" step="any" min="0" value="0" class="form-control" id="precio32" name="precio32" placeholder="precio 2">*/
/*         </div>*/
/*     </div>*/
/*     <div class="col-md-2 col-sm-2 col-xs-2">*/
/*         <div class="control-group">*/
/*             <input  type="number" step="any" min="0" value="0" class="form-control" id="precio33" name="precio33" placeholder="precio 3">*/
/*         </div>*/
/*     </div>*/
/*     <label style="color:green; cursor: pointer;" id="addp3">Agregar otro proveedor</label>*/
/* </div>*/
/* */
/* <div id="proveedor4" class="col-md-12" style="display:none;">*/
/*     <div class="col-md-6 col-sm-6 col-xs-6">*/
/*         <div class="control-group">*/
/*             <select name="proveedor_ddl4" id="proveedor_ddl4" class="form-control" data-width="100%" >*/
/*                 <option value="0" >Eliga el proveedor 4</option>*/
/*                 {% for proveedor in proveedor %}*/
/*                     <option value="{{proveedor.id}}" >{{proveedor.empresa}} - {{proveedor.apellidos}} {{proveedor.nombre}}</option>*/
/*                 {% endfor %}*/
/*             </select>*/
/*         </div>*/
/*     </div>*/
/*     <div class="col-md-2 col-sm-2 col-xs-2">*/
/*         <div class="control-group">*/
/*             <input  type="number" step="any" min="0" value="0"  class="form-control" id="precio41" name="precio41" placeholder="precio 1">*/
/*         </div>*/
/*     </div>*/
/*     <div class="col-md-2 col-sm-2 col-xs-2">*/
/*         <div class="control-group">*/
/*             <input  type="number" step="any" min="0" value="0" class="form-control" id="precio42" name="precio42" placeholder="precio 2">*/
/*         </div>*/
/*     </div>*/
/*     <div class="col-md-2 col-sm-2 col-xs-2">*/
/*         <div class="control-group">*/
/*             <input  type="number" step="any" min="0" value="0" class="form-control" id="precio43" name="precio43" placeholder="precio 3">*/
/*         </div>*/
/*     </div>*/
/* */
/* </div>*/
