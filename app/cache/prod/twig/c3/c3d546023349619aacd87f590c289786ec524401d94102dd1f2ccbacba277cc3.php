<?php

/* LoginBundle:security:login.html.twig */
class __TwigTemplate_84a1c22285cab950fd0127082fcfab231484d82946e77f955418e663d72c6ed9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_495c30cc40e9d902d4145a4fb97600e2b569f972f6aa98156becd770a72ac1c7 = $this->env->getExtension("native_profiler");
        $__internal_495c30cc40e9d902d4145a4fb97600e2b569f972f6aa98156becd770a72ac1c7->enter($__internal_495c30cc40e9d902d4145a4fb97600e2b569f972f6aa98156becd770a72ac1c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "LoginBundle:security:login.html.twig"));

        // line 1
        echo "
<html>
    <head>
        <meta charset=\"UTF-8\" />

        <meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;\">
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <script src=\"//code.jquery.com/jquery-1.11.2.min.js\"></script>
        <script src=\"//code.jquery.com/jquery-migrate-1.2.1.min.js\"></script>
        <script src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bootstrap/js/bootstrap.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/login.js"), "html", null, true);
        echo "\"></script>

        <link href=";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bootstrap/css/bootstrap.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\"/>
        <link href=";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/login.css"), "html", null, true);
        echo " rel=\"stylesheet\" type=\"text/css\"/>
    </head>
    <body>   
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-6 col-md-offset-3\">
                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading\">
                            <span class=\"glyphicon glyphicon-lock\"></span> Login</div>
                        <div class=\"panel-body\">
                          
                                

                                <form action=\"";
        // line 27
        echo $this->env->getExtension('routing')->getPath("login");
        echo "\" method=\"post\" class=\"form-horizontal\">
                                    <div class=\"form-group\">
                                    <label for=\"inputEmail3\" class=\"col-sm-3 control-label\">
                                        Usuario</label><div class=\"col-sm-9\">
                                            <input type=\"text\" id=\"username\" class=\"form-control\" name=\"_username\" value=\"";
        // line 31
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\" /></div>
                                    </div>
                                    <div class=\"form-group\">
                                    <label for=\"inputEmail3\" class=\"col-sm-3 control-label\">
                                        Password</label>
                                        <div class=\"col-sm-9\">
                                    <input type=\"password\" id=\"password\" class=\"form-control\" name=\"_password\" />
                                        </div></div>
                                    ";
        // line 44
        echo "                                    <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('form')->renderCsrfToken("authenticate"), "html", null, true);
        echo "\">
                                      <div class=\"form-group last\">
                        <div class=\"col-sm-offset-3 col-sm-9 \">
                            <button class=\"btn-log pull-right loginbtn\" id=\"loginbtn\" type=\"submit\">login</button>
                        </div>
                    </div>
                                    
                                </form>
                                ";
        // line 52
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 53
            echo "
                                    <div class=\"alert alert-success\">
                                        ";
            // line 55
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "
                                    </div>

                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 59
        echo "
                                ";
        // line 60
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "warning"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 61
            echo "
                                    <div class=\"alert alert-danger\">
                                        ";
            // line 63
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "
                                    </div>

                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "                           

                        </div>
                    </div>
                </div>
            </div>

<div id=\"loading\">
    <img src=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("imagenes/loading.gif"), "html", null, true);
        echo "\" alt=\"loading\" style=\"width: 120px; position: absolute; top: 50%; left: 50%; margin-top: -60px; margin-left: -80px;\"/>
</div>
    </body></html>";
        
        $__internal_495c30cc40e9d902d4145a4fb97600e2b569f972f6aa98156becd770a72ac1c7->leave($__internal_495c30cc40e9d902d4145a4fb97600e2b569f972f6aa98156becd770a72ac1c7_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_dc9fe09c1193ad106c5118d7fb527bb0bf2e648b5a6fe0df6bf20f628ae100a1 = $this->env->getExtension("native_profiler");
        $__internal_dc9fe09c1193ad106c5118d7fb527bb0bf2e648b5a6fe0df6bf20f628ae100a1->enter($__internal_dc9fe09c1193ad106c5118d7fb527bb0bf2e648b5a6fe0df6bf20f628ae100a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Usuarios";
        
        $__internal_dc9fe09c1193ad106c5118d7fb527bb0bf2e648b5a6fe0df6bf20f628ae100a1->leave($__internal_dc9fe09c1193ad106c5118d7fb527bb0bf2e648b5a6fe0df6bf20f628ae100a1_prof);

    }

    public function getTemplateName()
    {
        return "LoginBundle:security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  155 => 7,  145 => 75,  135 => 67,  125 => 63,  121 => 61,  117 => 60,  114 => 59,  104 => 55,  100 => 53,  96 => 52,  84 => 44,  73 => 31,  66 => 27,  50 => 14,  46 => 13,  41 => 11,  37 => 10,  31 => 7,  23 => 1,);
    }
}
/* */
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/* */
/*         <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">*/
/*         <title>{% block title %}Usuarios{% endblock %}</title>*/
/*         <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>*/
/*         <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>*/
/*         <script src="{{ asset('bootstrap/js/bootstrap.js') }}"></script>*/
/*         <script src="{{ asset('js/login.js') }}"></script>*/
/* */
/*         <link href={{ asset('bootstrap/css/bootstrap.css') }} rel="stylesheet" type="text/css"/>*/
/*         <link href={{ asset('css/login.css') }} rel="stylesheet" type="text/css"/>*/
/*     </head>*/
/*     <body>   */
/*         <div class="container">*/
/*             <div class="row">*/
/*                 <div class="col-md-6 col-md-offset-3">*/
/*                     <div class="panel panel-default">*/
/*                         <div class="panel-heading">*/
/*                             <span class="glyphicon glyphicon-lock"></span> Login</div>*/
/*                         <div class="panel-body">*/
/*                           */
/*                                 */
/* */
/*                                 <form action="{{ path('login') }}" method="post" class="form-horizontal">*/
/*                                     <div class="form-group">*/
/*                                     <label for="inputEmail3" class="col-sm-3 control-label">*/
/*                                         Usuario</label><div class="col-sm-9">*/
/*                                             <input type="text" id="username" class="form-control" name="_username" value="{{ last_username }}" /></div>*/
/*                                     </div>*/
/*                                     <div class="form-group">*/
/*                                     <label for="inputEmail3" class="col-sm-3 control-label">*/
/*                                         Password</label>*/
/*                                         <div class="col-sm-9">*/
/*                                     <input type="password" id="password" class="form-control" name="_password" />*/
/*                                         </div></div>*/
/*                                     {#*/
/*                                         If you want to control the URL the user*/
/*                                         is redirected to on success (more details below)*/
/*                                         <input type="hidden" name="_target_path" value="/account" />*/
/*                                     #}*/
/*                                     <input type="hidden" name="_csrf_token" value="{{ csrf_token('authenticate') }}">*/
/*                                       <div class="form-group last">*/
/*                         <div class="col-sm-offset-3 col-sm-9 ">*/
/*                             <button class="btn-log pull-right loginbtn" id="loginbtn" type="submit">login</button>*/
/*                         </div>*/
/*                     </div>*/
/*                                     */
/*                                 </form>*/
/*                                 {% for flashMessage in app.session.flashbag.get('notice') %}*/
/* */
/*                                     <div class="alert alert-success">*/
/*                                         {{ flashMessage }}*/
/*                                     </div>*/
/* */
/*                                 {% endfor %}*/
/* */
/*                                 {% for flashMessage in app.session.flashbag.get('warning') %}*/
/* */
/*                                     <div class="alert alert-danger">*/
/*                                         {{ flashMessage }}*/
/*                                     </div>*/
/* */
/*                                 {% endfor %}*/
/*                            */
/* */
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/* */
/* <div id="loading">*/
/*     <img src="{{asset('imagenes/loading.gif')}}" alt="loading" style="width: 120px; position: absolute; top: 50%; left: 50%; margin-top: -60px; margin-left: -80px;"/>*/
/* </div>*/
/*     </body></html>*/
