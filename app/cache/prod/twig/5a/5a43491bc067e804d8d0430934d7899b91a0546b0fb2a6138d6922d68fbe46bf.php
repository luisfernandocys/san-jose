<?php

/* AdminBundle:salidas:salidas.html.twig */
class __TwigTemplate_96ee26586fb547b8acf4f7130f644ea80bfd54d62141dc93b10f3e866db4eaec extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "AdminBundle:salidas:salidas.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'header' => array($this, 'block_header'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7681342cdb50e9c4a399e4f8da3a82bd27d8358084f82e1f25781b37de2531b9 = $this->env->getExtension("native_profiler");
        $__internal_7681342cdb50e9c4a399e4f8da3a82bd27d8358084f82e1f25781b37de2531b9->enter($__internal_7681342cdb50e9c4a399e4f8da3a82bd27d8358084f82e1f25781b37de2531b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AdminBundle:salidas:salidas.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7681342cdb50e9c4a399e4f8da3a82bd27d8358084f82e1f25781b37de2531b9->leave($__internal_7681342cdb50e9c4a399e4f8da3a82bd27d8358084f82e1f25781b37de2531b9_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_dbf9efb0f8727ccf0c9863e9edeacea8a2f6c56ea21171acbca11f6c2324d1d6 = $this->env->getExtension("native_profiler");
        $__internal_dbf9efb0f8727ccf0c9863e9edeacea8a2f6c56ea21171acbca11f6c2324d1d6->enter($__internal_dbf9efb0f8727ccf0c9863e9edeacea8a2f6c56ea21171acbca11f6c2324d1d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "    Salidas
";
        
        $__internal_dbf9efb0f8727ccf0c9863e9edeacea8a2f6c56ea21171acbca11f6c2324d1d6->leave($__internal_dbf9efb0f8727ccf0c9863e9edeacea8a2f6c56ea21171acbca11f6c2324d1d6_prof);

    }

    // line 6
    public function block_header($context, array $blocks = array())
    {
        $__internal_c9fcd981175c8b1b939e8bd7382f59e74b0a8137316f636c4378fefc6436a663 = $this->env->getExtension("native_profiler");
        $__internal_c9fcd981175c8b1b939e8bd7382f59e74b0a8137316f636c4378fefc6436a663->enter($__internal_c9fcd981175c8b1b939e8bd7382f59e74b0a8137316f636c4378fefc6436a663_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 7
        echo "
    <script src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/admin/js/salidas.js"), "html", null, true);
        echo "\"></script>
    
    <script src=\"https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js\"></script>
    <script src=\"https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js\"></script>
    <script src=\"https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js\"></script>
    <script src=\"https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js\"></script>
    <script src=\"https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js\"></script>
    <script src=\"https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js\"></script>
    <script src=\"https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js\"></script>

    <link rel=\"stylesheet\" type=\"text/css\" href=\"https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css\">


    <style>

        fieldset.scheduler-border {
            border: 1px groove #ddd !important;
            padding: 0 1.4em 1.4em 1.4em !important;
            margin: 0 0 1.5em 0 !important;
            -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
        }

        legend.scheduler-border {
            font-size: 1.2em !important;
            font-weight: bold !important;
            text-align: left !important;
            width:auto;
            padding:0 10px;
            border-bottom:none;
        }

    </style>
";
        
        $__internal_c9fcd981175c8b1b939e8bd7382f59e74b0a8137316f636c4378fefc6436a663->leave($__internal_c9fcd981175c8b1b939e8bd7382f59e74b0a8137316f636c4378fefc6436a663_prof);

    }

    // line 43
    public function block_body($context, array $blocks = array())
    {
        $__internal_f6db92b7ef2d34ba732ba6c8c9d0047267dca842cea73697f581bb0c89fe7873 = $this->env->getExtension("native_profiler");
        $__internal_f6db92b7ef2d34ba732ba6c8c9d0047267dca842cea73697f581bb0c89fe7873->enter($__internal_f6db92b7ef2d34ba732ba6c8c9d0047267dca842cea73697f581bb0c89fe7873_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 44
        echo "    <div class=\"container\">
        <div class=\"row\">

            <h1 class=\"text-center header\">Registrar Salida</h1>




            ";
        // line 52
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 53
            echo "
                <div class=\"alert alert-success\">
                    ";
            // line 55
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "
                </div>

            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 59
        echo "
            ";
        // line 60
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "warning"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 61
            echo "
                <div class=\"alert alert-danger\">
                    ";
            // line 63
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "
                </div>

            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "        </div>
    </div>

    <div class=\"container main\">
        <div class=\"row\">
            <div class=\"col-md-12\">
                <form role=\"form\" id=\"salidasform\" action=\"";
        // line 73
        echo $this->env->getExtension('routing')->getPath("crear_salidas");
        echo "\" method=\"POST\" >
                    <div class=\"row\">

                        <div class=\"form-group\">
                            <label for=\"cb\" class=\"col-md-2 control-label\">Código de Barras:</label>
                            <div class=\"col-md-8\"><input class=\"form-control\" type=\"text\" name=\"cb\"  id=\"cb\"  />
                                <input type=\"hidden\" name=\"iddetalleentrada\"  data-id=\"\" id=\"iddetalleentrada\"/>
                                <input type=\"hidden\" name=\"idproducto\" data-id=\"idproducto\" id=\"idproducto\"/>
                                <input type=\"hidden\" name=\"folio\" data-folio=\"\" id=\"folio\"/>
                            </div> 
                            <div class=\"col-md-2\">

                                <input   ";
        // line 85
        if (twig_in_filter("ESCRITURA", (isset($context["ppm"]) ? $context["ppm"] : $this->getContext($context, "ppm")))) {
            echo " id=\"btn_agregar_salida\" class=\"btn  text-center  \" type=\"submit\"  ";
        } else {
            echo "  class=\"btn boton_disable\"  ";
        }
        echo " value=\"Agregar Salida\" /> 
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-12\">
                <fieldset class=\"scheduler-border\">
                    <legend class=\"scheduler-border\">Datos del producto</legend>
                    <div class=\"col-md-3\">
                        <label class=\"control-label col-md-6\">Producto:</label>
                        <label class=\"control-label col-md-6\" id=\"lbproducto\"></label>
                    </div>
                    <div class=\"col-md-3\">
                        <label class=\"control-label col-md-6\">Valor:</label>
                        <label class=\"control-label col-md-6\" id=\"lbvalor\"></label>
                    </div>
                    <div class=\"col-md-3\">
                        <label class=\"control-label col-md-6\">Fecha de Alta:</label>
                        <label class=\"control-label col-md-6\" id=\"lbfecha\"></label>
                    </div>
                    <div class=\"col-md-3\">
                        <label class=\"control-label col-md-6\">Status:</label>
                        <label class=\"control-label col-md-6\" id=\"lbstatus\"></label>
                    </div>
                </fieldset>
            </div>
            <input type=\"hidden\"  data-path=\"";
        // line 115
        echo $this->env->getExtension('routing')->getPath("detalle_entrada_codigo");
        echo "\" id=\"path_entrada\"/>

        </div>
        <div class=\"row\">
            <h1 class=\"text-center header\">Salidas Registradas</h1>
        </div>

        <div class=\"row\">
<div class=\"col-md-12 tableblanco \" >
                <form action=\"";
        // line 124
        echo $this->env->getExtension('routing')->getPath("salidasfecha");
        echo "\"  method=\"post\">
                <table border=\"0\" cellspacing=\"5\" cellpadding=\"5\" style=\"padding-bottom: 14px;\">
                    <tbody><tr>
                            <td>Fecha de inicio:</td>
                            <td><input type=\"text\" class=\"form-control datepicker\"  id=\"min\" name=\"min\"   value=\"\"></td>
                            
                            <td>Fecha de término:</td>
                            <td><input type=\"text\" class=\"form-control datepicker\"  id=\"max\" name=\"max\"   value=\"\"></td>
                           
                            <td><input type=\"submit\" value=\"Generar reporte\" class=\"btn-lg linkload\"></td>
                        </tr>
                        <tr>
                            <td colspan=\"2\">";
        // line 136
        if (array_key_exists("min", $context)) {
            echo " ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["min"]) ? $context["min"] : $this->getContext($context, "min")), "d/m/Y"), "html", null, true);
            echo " ";
        }
        echo "</td>
                             <td colspan=\"2\">";
        // line 137
        if (array_key_exists("max", $context)) {
            echo " ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["max"]) ? $context["max"] : $this->getContext($context, "max")), "d/m/Y"), "html", null, true);
            echo " ";
        }
        echo "</td>
                        </tr>
                    </tbody></table>
                </form>
            </div>
            <div class=\"col-md-10 col-md-offset-1 tableblanco\" >
                <table id=\"tabla_salidas\" class=\"display nowrap table table-striped  \" cellspacing=\"0\" width=\"100%\">
                    <thead>
                        <tr>
                            <th class=\"text-center\">Salida</th>
                            <th class=\"text-center\">Fecha de Registro</th>
                            <th class=\"text-center\">Producto</th>  
                            <th class=\"text-center\">Modelo</th>
                            <th class=\"text-center\">Color</th>
                            <th class=\"text-center\">Ancho</th>
                            <th class=\"text-center\">Largo</th>
                            <th class=\"text-center\">Peso</th>
                            <th class=\"text-center\">Número</th>
                            <th class=\"text-center\">Código</th>

                        </tr>
                    </thead>
                    <tbody>
                        ";
        // line 160
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["salidas"]);
        foreach ($context['_seq'] as $context["_key"] => $context["salidas"]) {
            echo " 
                            <tr>
                                <td class=\"text-center\">";
            // line 162
            echo twig_escape_filter($this->env, $this->getAttribute($context["salidas"], "id", array()), "html", null, true);
            echo "</td>
                                <td class=\"text-center\">";
            // line 163
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["salidas"], "fecha", array()), "d/m/Y H:i:s"), "html", null, true);
            echo "</td>
                                <td class=\"text-center\">";
            // line 164
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($context["salidas"], "detalleentrada", array()), "entrada", array()), "producto", array()), "nombre", array()), "html", null, true);
            echo "</td>
                                <td class=\"text-center\">";
            // line 165
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($context["salidas"], "detalleentrada", array()), "entrada", array()), "producto", array()), "modelo", array()), "nombre", array()), "html", null, true);
            echo "</td>
                                <td class=\"text-center\">";
            // line 166
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($context["salidas"], "detalleentrada", array()), "entrada", array()), "producto", array()), "color", array()), "nombre", array()), "html", null, true);
            echo "</td>
                                
                                <td class=\"text-center\">";
            // line 168
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["salidas"], "detalleentrada", array()), "entrada", array()), "ancho", array()), "html", null, true);
            echo "</td>
                                <td class=\"text-center\">";
            // line 169
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["salidas"], "detalleentrada", array()), "entrada", array()), "longitud", array()), "html", null, true);
            echo "</td>
                                <td class=\"text-center\">";
            // line 170
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["salidas"], "detalleentrada", array()), "entrada", array()), "peso", array()), "html", null, true);
            echo "</td>
                                <td class=\"text-center\">";
            // line 171
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["salidas"], "detalleentrada", array()), "entrada", array()), "numero", array()), "html", null, true);
            echo "</td>
                                
                                <td class=\"text-center\">";
            // line 173
            echo twig_escape_filter($this->env, $this->getAttribute($context["salidas"], "codigo", array()), "html", null, true);
            echo "</td>



                            </tr>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['salidas'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 179
        echo "                    </tbody>
                </table>
            </div>
        </div>

        <div class=\"row\">
            ";
        // line 185
        $this->loadTemplate("AdminBundle:entradas:detalleentradas.html.twig", "AdminBundle:salidas:salidas.html.twig", 185)->display($context);
        // line 186
        echo "

        </div>
    </div>
                   <script>
  
    \$.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '< Ant',
        nextText: 'Sig >',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    \$.datepicker.setDefaults(\$.datepicker.regional['es']);
    \$(function () {
        \$(\".datepicker\").datepicker();
        
    });
</script>
";
        
        $__internal_f6db92b7ef2d34ba732ba6c8c9d0047267dca842cea73697f581bb0c89fe7873->leave($__internal_f6db92b7ef2d34ba732ba6c8c9d0047267dca842cea73697f581bb0c89fe7873_prof);

    }

    public function getTemplateName()
    {
        return "AdminBundle:salidas:salidas.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  348 => 186,  346 => 185,  338 => 179,  326 => 173,  321 => 171,  317 => 170,  313 => 169,  309 => 168,  304 => 166,  300 => 165,  296 => 164,  292 => 163,  288 => 162,  281 => 160,  251 => 137,  243 => 136,  228 => 124,  216 => 115,  179 => 85,  164 => 73,  156 => 67,  146 => 63,  142 => 61,  138 => 60,  135 => 59,  125 => 55,  121 => 53,  117 => 52,  107 => 44,  101 => 43,  59 => 8,  56 => 7,  50 => 6,  42 => 4,  36 => 3,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block title %}*/
/*     Salidas*/
/* {% endblock %}*/
/* {% block header %}*/
/* */
/*     <script src="{{ asset('bundles/admin/js/salidas.js') }}"></script>*/
/*     */
/*     <script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>*/
/*     <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>*/
/*     <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>*/
/*     <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>*/
/*     <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>*/
/*     <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>*/
/*     <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>*/
/*     <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>*/
/* */
/*     <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">*/
/* */
/* */
/*     <style>*/
/* */
/*         fieldset.scheduler-border {*/
/*             border: 1px groove #ddd !important;*/
/*             padding: 0 1.4em 1.4em 1.4em !important;*/
/*             margin: 0 0 1.5em 0 !important;*/
/*             -webkit-box-shadow:  0px 0px 0px 0px #000;*/
/*             box-shadow:  0px 0px 0px 0px #000;*/
/*         }*/
/* */
/*         legend.scheduler-border {*/
/*             font-size: 1.2em !important;*/
/*             font-weight: bold !important;*/
/*             text-align: left !important;*/
/*             width:auto;*/
/*             padding:0 10px;*/
/*             border-bottom:none;*/
/*         }*/
/* */
/*     </style>*/
/* {% endblock %}*/
/* {% block body %}*/
/*     <div class="container">*/
/*         <div class="row">*/
/* */
/*             <h1 class="text-center header">Registrar Salida</h1>*/
/* */
/* */
/* */
/* */
/*             {% for flashMessage in app.session.flashbag.get('notice') %}*/
/* */
/*                 <div class="alert alert-success">*/
/*                     {{ flashMessage }}*/
/*                 </div>*/
/* */
/*             {% endfor %}*/
/* */
/*             {% for flashMessage in app.session.flashbag.get('warning') %}*/
/* */
/*                 <div class="alert alert-danger">*/
/*                     {{ flashMessage }}*/
/*                 </div>*/
/* */
/*             {% endfor %}*/
/*         </div>*/
/*     </div>*/
/* */
/*     <div class="container main">*/
/*         <div class="row">*/
/*             <div class="col-md-12">*/
/*                 <form role="form" id="salidasform" action="{{path('crear_salidas')}}" method="POST" >*/
/*                     <div class="row">*/
/* */
/*                         <div class="form-group">*/
/*                             <label for="cb" class="col-md-2 control-label">Código de Barras:</label>*/
/*                             <div class="col-md-8"><input class="form-control" type="text" name="cb"  id="cb"  />*/
/*                                 <input type="hidden" name="iddetalleentrada"  data-id="" id="iddetalleentrada"/>*/
/*                                 <input type="hidden" name="idproducto" data-id="idproducto" id="idproducto"/>*/
/*                                 <input type="hidden" name="folio" data-folio="" id="folio"/>*/
/*                             </div> */
/*                             <div class="col-md-2">*/
/* */
/*                                 <input   {% if 'ESCRITURA' in ppm %} id="btn_agregar_salida" class="btn  text-center  " type="submit"  {% else %}  class="btn boton_disable"  {% endif %} value="Agregar Salida" /> */
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/* */
/*                 </form>*/
/*             </div>*/
/*         </div>*/
/*         <div class="row">*/
/*             <div class="col-md-12">*/
/*                 <fieldset class="scheduler-border">*/
/*                     <legend class="scheduler-border">Datos del producto</legend>*/
/*                     <div class="col-md-3">*/
/*                         <label class="control-label col-md-6">Producto:</label>*/
/*                         <label class="control-label col-md-6" id="lbproducto"></label>*/
/*                     </div>*/
/*                     <div class="col-md-3">*/
/*                         <label class="control-label col-md-6">Valor:</label>*/
/*                         <label class="control-label col-md-6" id="lbvalor"></label>*/
/*                     </div>*/
/*                     <div class="col-md-3">*/
/*                         <label class="control-label col-md-6">Fecha de Alta:</label>*/
/*                         <label class="control-label col-md-6" id="lbfecha"></label>*/
/*                     </div>*/
/*                     <div class="col-md-3">*/
/*                         <label class="control-label col-md-6">Status:</label>*/
/*                         <label class="control-label col-md-6" id="lbstatus"></label>*/
/*                     </div>*/
/*                 </fieldset>*/
/*             </div>*/
/*             <input type="hidden"  data-path="{{path('detalle_entrada_codigo')}}" id="path_entrada"/>*/
/* */
/*         </div>*/
/*         <div class="row">*/
/*             <h1 class="text-center header">Salidas Registradas</h1>*/
/*         </div>*/
/* */
/*         <div class="row">*/
/* <div class="col-md-12 tableblanco " >*/
/*                 <form action="{{path('salidasfecha')}}"  method="post">*/
/*                 <table border="0" cellspacing="5" cellpadding="5" style="padding-bottom: 14px;">*/
/*                     <tbody><tr>*/
/*                             <td>Fecha de inicio:</td>*/
/*                             <td><input type="text" class="form-control datepicker"  id="min" name="min"   value=""></td>*/
/*                             */
/*                             <td>Fecha de término:</td>*/
/*                             <td><input type="text" class="form-control datepicker"  id="max" name="max"   value=""></td>*/
/*                            */
/*                             <td><input type="submit" value="Generar reporte" class="btn-lg linkload"></td>*/
/*                         </tr>*/
/*                         <tr>*/
/*                             <td colspan="2">{% if min is defined %} {{min|date('d/m/Y')}} {% endif %}</td>*/
/*                              <td colspan="2">{% if max is defined %} {{max|date('d/m/Y')}} {% endif %}</td>*/
/*                         </tr>*/
/*                     </tbody></table>*/
/*                 </form>*/
/*             </div>*/
/*             <div class="col-md-10 col-md-offset-1 tableblanco" >*/
/*                 <table id="tabla_salidas" class="display nowrap table table-striped  " cellspacing="0" width="100%">*/
/*                     <thead>*/
/*                         <tr>*/
/*                             <th class="text-center">Salida</th>*/
/*                             <th class="text-center">Fecha de Registro</th>*/
/*                             <th class="text-center">Producto</th>  */
/*                             <th class="text-center">Modelo</th>*/
/*                             <th class="text-center">Color</th>*/
/*                             <th class="text-center">Ancho</th>*/
/*                             <th class="text-center">Largo</th>*/
/*                             <th class="text-center">Peso</th>*/
/*                             <th class="text-center">Número</th>*/
/*                             <th class="text-center">Código</th>*/
/* */
/*                         </tr>*/
/*                     </thead>*/
/*                     <tbody>*/
/*                         {% for salidas in salidas %} */
/*                             <tr>*/
/*                                 <td class="text-center">{{salidas.id}}</td>*/
/*                                 <td class="text-center">{{salidas.fecha|date('d/m/Y H:i:s')}}</td>*/
/*                                 <td class="text-center">{{salidas.detalleentrada.entrada.producto.nombre}}</td>*/
/*                                 <td class="text-center">{{salidas.detalleentrada.entrada.producto.modelo.nombre}}</td>*/
/*                                 <td class="text-center">{{salidas.detalleentrada.entrada.producto.color.nombre}}</td>*/
/*                                 */
/*                                 <td class="text-center">{{salidas.detalleentrada.entrada.ancho}}</td>*/
/*                                 <td class="text-center">{{salidas.detalleentrada.entrada.longitud}}</td>*/
/*                                 <td class="text-center">{{salidas.detalleentrada.entrada.peso}}</td>*/
/*                                 <td class="text-center">{{salidas.detalleentrada.entrada.numero}}</td>*/
/*                                 */
/*                                 <td class="text-center">{{salidas.codigo}}</td>*/
/* */
/* */
/* */
/*                             </tr>*/
/*                         {% endfor %}*/
/*                     </tbody>*/
/*                 </table>*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="row">*/
/*             {% include "AdminBundle:entradas:detalleentradas.html.twig" %}*/
/* */
/* */
/*         </div>*/
/*     </div>*/
/*                    <script>*/
/*   */
/*     $.datepicker.regional['es'] = {*/
/*         closeText: 'Cerrar',*/
/*         prevText: '< Ant',*/
/*         nextText: 'Sig >',*/
/*         currentText: 'Hoy',*/
/*         monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],*/
/*         monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],*/
/*         dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],*/
/*         dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],*/
/*         dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],*/
/*         weekHeader: 'Sm',*/
/*         dateFormat: 'dd/mm/yy',*/
/*         firstDay: 1,*/
/*         isRTL: false,*/
/*         showMonthAfterYear: false,*/
/*         yearSuffix: ''*/
/*     };*/
/*     $.datepicker.setDefaults($.datepicker.regional['es']);*/
/*     $(function () {*/
/*         $(".datepicker").datepicker();*/
/*         */
/*     });*/
/* </script>*/
/* {% endblock %}*/
