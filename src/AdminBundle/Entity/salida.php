<?php

namespace AdminBundle\Entity;

/**
 * salida
 */
class salida
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $producto;

    /**
     * @var string
     */
    private $folio;
    
    /**
     * @var datetime
     */
    private $fecha;

    /**
     * @var string
     */
    private $codigo;

    /**
     * @var int
     */
    private $detalleentrada;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set producto
     *
     * @param integer $producto
     *
     * @return salida
     */
    public function setProducto($producto)
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return int
     */
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * Set folio
     *
     * @param string $folio
     *
     * @return salida
     */
    public function setFolio($folio)
    {
        $this->folio = $folio;

        return $this;
    }

    /**
     * Get folio
     *
     * @return string
     */
    public function getFolio()
    {
        return $this->folio;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return salida
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set detalleentrada
     *
     * @param integer $detalleentrada
     *
     * @return salida
     */
    public function setDetalleentrada($detalleentrada)
    {
        $this->detalleentrada = $detalleentrada;

        return $this;
    }

    /**
     * Get detalleentrada
     *
     * @return int
     */
    public function getDetalleentrada()
    {
        return $this->detalleentrada;
    }
    
    /**
     * Set fecha
     *
     * @param datetime $fecha
     *
     * @return salida
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return datetime
     */
    public function getFecha()
    {
        return $this->fecha;
    }
}
