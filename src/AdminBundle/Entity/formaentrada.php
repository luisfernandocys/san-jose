<?php

namespace AdminBundle\Entity;

/**
 * formaentrada
 */
class formaentrada
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $formanentrada;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set formanentrada
     *
     * @param string $formanentrada
     *
     * @return formaentrada
     */
    public function setFormanentrada($formanentrada)
    {
        $this->formanentrada = $formanentrada;

        return $this;
    }

    /**
     * Get formanentrada
     *
     * @return string
     */
    public function getFormanentrada()
    {
        return $this->formanentrada;
    }
}
