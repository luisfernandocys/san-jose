<?php

namespace AdminBundle\Entity;

/**
 * tipo
 */
class tipo
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $clase;

    /**
     * @var string
     */
    private $tipo;

    /**
     * @var int
     */
    private $dfolio;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set clase
     *
     * @param integer $clase
     *
     * @return tipo
     */
    public function setClase($clase)
    {
        $this->clase = $clase;

        return $this;
    }

    /**
     * Get clase
     *
     * @return int
     */
    public function getClase()
    {
        return $this->clase;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return tipo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set dfolio
     *
     * @param integer $dfolio
     *
     * @return subclase
     */
    public function setDfolio($dfolio)
    {
        $this->dfolio = $dfolio;

        return $this;
    }

    /**
     * Get dfolio
     *
     * @return int
     */
    public function getDfolio()
    {
        return $this->dfolio;
    }
}
