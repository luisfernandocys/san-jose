<?php

namespace AdminBundle\Entity;

/**
 * Producto
 */
class Producto
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $folio;

    /**
     * @var \AdminBundle\Entity\clase
     */
    private $clase;

    /**
     * @var int
     */
    private $tipo;

    /**
     * @var string
     */
    private $nombre;
    /**
     * @var string
     */
    private $codigo;

    /**
     * @var string
     */
    private $otrosnombres;

    /**
     * @var string
     */
    private $marca;

    /**
     * @var int
     */
    private $modelo;

    /**
     * @var int
     */
    private $unidad;

    /**
     * @var int
     */
    private $numero;

    /**
     * @var int
     */
    private $peso;

    /**
     * @var int
     */
    private $ancho;

    /**
     * @var int
     */
    private $longitud;

    /**
     * @var int
     */
    private $color;

    /**
     * @var int
     */
    private $tipotejido;

    /**
     * @var int
     */
    private $densidad;

    /**
     * @var int
     */
    private $hilados;

    /**
     * @var int
     */
    private $fibras;

    /**
     * @var string
     */
    private $composicion;

    /**
     * @var string
     */
    private $principalesusos;

    /**
     * @var int
     */
    private $proveedor;

    /**
     * @var int
     */
    private $precioadquisicion;

    /**
     * @var string
     */
    private $informacionadicional;

    /**
     * @var string
     */
    private $foto;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return Producto
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }
    
    /**
     * Set folio
     *
     * @param string $folio
     *
     * @return Producto
     */
    public function setFolio($folio)
    {
        $this->folio = $folio;

        return $this;
    }

    /**
     * Get folio
     *
     * @return string
     */
    public function getFolio()
    {
        return $this->folio;
    }

    /**
     * Set idmodulo
     *
     * @param \AdminBundle\Entity\clase $clase
     *
     * @return Producto
     */
    public function setClase(\AdminBundle\Entity\clase $clase = null)
    {
        $this->clase = $clase;

        return $this;
    }

    /**
     * Get clase
     *
     * @return \AdminBundle\Entity\clase
     */
    public function getClase()
    {
        return $this->clase;
    }

    /**
     * Set tipo
     *
     * @param integer $tipo
     *
     * @return Producto
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return int
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Producto
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set otrosnombres
     *
     * @param string $otrosnombres
     *
     * @return Producto
     */
    public function setOtrosnombres($otrosnombres)
    {
        $this->otrosnombres = $otrosnombres;

        return $this;
    }

    /**
     * Get otrosnombres
     *
     * @return string
     */
    public function getOtrosnombres()
    {
        return $this->otrosnombres;
    }

    /**
     * Set marca
     *
     * @param string $marca
     *
     * @return Producto
     */
    public function setMarca($marca)
    {
        $this->marca = $marca;

        return $this;
    }

    /**
     * Get marca
     *
     * @return string
     */
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * Set modelo
     *
     * @param int $modelo
     *
     * @return Producto
     */
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;

        return $this;
    }

    /**
     * Get modelo
     *
     * @return int
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * Set unidad
     *
     * @param integer $unidad
     *
     * @return Producto
     */
    public function setUnidad($unidad)
    {
        $this->unidad = $unidad;

        return $this;
    }

    /**
     * Get unidad
     *
     * @return int
     */
    public function getUnidad()
    {
        return $this->unidad;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return Producto
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return int
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set peso
     *
     * @param integer $peso
     *
     * @return Producto
     */
    public function setPeso($peso)
    {
        $this->peso = $peso;

        return $this;
    }

    /**
     * Get peso
     *
     * @return int
     */
    public function getPeso()
    {
        return $this->peso;
    }

    /**
     * Set ancho
     *
     * @param integer $ancho
     *
     * @return Producto
     */
    public function setAncho($ancho)
    {
        $this->ancho = $ancho;

        return $this;
    }

    /**
     * Get ancho
     *
     * @return int
     */
    public function getAncho()
    {
        return $this->ancho;
    }

    /**
     * Set longitud
     *
     * @param integer $longitud
     *
     * @return Producto
     */
    public function setLongitud($longitud)
    {
        $this->longitud = $longitud;

        return $this;
    }

    /**
     * Get longitud
     *
     * @return int
     */
    public function getLongitud()
    {
        return $this->longitud;
    }

    /**
     * Set color
     *
     * @param int $color
     *
     * @return Producto
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return int
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set tipotejido
     *
     * @param integer $tipotejido
     *
     * @return Producto
     */
    public function setTipotejido($tipotejido)
    {
        $this->tipotejido = $tipotejido;

        return $this;
    }

    /**
     * Get tipotejido
     *
     * @return int
     */
    public function getTipotejido()
    {
        return $this->tipotejido;
    }

    /**
     * Set densidad
     *
     * @param integer $densidad
     *
     * @return Producto
     */
    public function setDensidad($densidad)
    {
        $this->densidad = $densidad;

        return $this;
    }

    /**
     * Get densidad
     *
     * @return int
     */
    public function getDensidad()
    {
        return $this->densidad;
    }

    /**
     * Set hilados
     *
     * @param integer $hilados
     *
     * @return Producto
     */
    public function setHilados($hilados)
    {
        $this->hilados = $hilados;

        return $this;
    }

    /**
     * Get hilados
     *
     * @return int
     */
    public function getHilados()
    {
        return $this->hilados;
    }

    /**
     * Set fibras
     *
     * @param integer $fibras
     *
     * @return Producto
     */
    public function setFibras($fibras)
    {
        $this->fibras = $fibras;

        return $this;
    }

    /**
     * Get fibras
     *
     * @return int
     */
    public function getFibras()
    {
        return $this->fibras;
    }

    /**
     * Set composicion
     *
     * @param string $composicion
     *
     * @return Producto
     */
    public function setComposicion($composicion)
    {
        $this->composicion = $composicion;

        return $this;
    }

    /**
     * Get composicion
     *
     * @return string
     */
    public function getComposicion()
    {
        return $this->composicion;
    }

    /**
     * Set principalesusos
     *
     * @param string $principalesusos
     *
     * @return Producto
     */
    public function setPrincipalesusos($principalesusos)
    {
        $this->principalesusos = $principalesusos;

        return $this;
    }

    /**
     * Get principalesusos
     *
     * @return string
     */
    public function getPrincipalesusos()
    {
        return $this->principalesusos;
    }

    /**
     * Set proveedor
     *
     * @param integer $proveedor
     *
     * @return Producto
     */
    public function setProveedor($proveedor)
    {
        $this->proveedor = $proveedor;

        return $this;
    }

    /**
     * Get proveedor
     *
     * @return int
     */
    public function getProveedor()
    {
        return $this->proveedor;
    }

    /**
     * Set precioadquisicion
     *
     * @param integer $precioadquisicion
     *
     * @return Producto
     */
    public function setPrecioadquisicion($precioadquisicion)
    {
        $this->precioadquisicion = $precioadquisicion;

        return $this;
    }

    /**
     * Get precioadquisicion
     *
     * @return int
     */
    public function getPrecioadquisicion()
    {
        return $this->precioadquisicion;
    }

    /**
     * Set informacionadicional
     *
     * @param string $informacionadicional
     *
     * @return Producto
     */
    public function setInformacionadicional($informacionadicional)
    {
        $this->informacionadicional = $informacionadicional;

        return $this;
    }

    /**
     * Get informacionadicional
     *
     * @return string
     */
    public function getInformacionadicional()
    {
        return $this->informacionadicional;
    }

    /**
     * Set foto
     *
     * @param string $foto
     *
     * @return Producto
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;

        return $this;
    }

    /**
     * Get foto
     *
     * @return string
     */
    public function getFoto()
    {
        return $this->foto;
    }
    /**
     * @var \DateTime
     */
    private $fecha;


    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Producto
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }
    /**
     * @var \AdminBundle\Entity\ProductoProveedor
     */
    private $pp;


    /**
     * Set pp
     *
     * @param \AdminBundle\Entity\ProductoProveedor $pp
     *
     * @return Producto
     */
    public function setPp(\AdminBundle\Entity\ProductoProveedor $pp = null)
    {
        $this->pp = $pp;

        return $this;
    }

    /**
     * Get pp
     *
     * @return \AdminBundle\Entity\ProductoProveedor
     */
    public function getPp()
    {
        return $this->pp;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pp = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add pp
     *
     * @param \AdminBundle\Entity\ProductoProveedor $pp
     *
     * @return Producto
     */
    public function addPp(\AdminBundle\Entity\ProductoProveedor $pp)
    {
        $this->pp[] = $pp;

        return $this;
    }

    /**
     * Remove pp
     *
     * @param \AdminBundle\Entity\ProductoProveedor $pp
     */
    public function removePp(\AdminBundle\Entity\ProductoProveedor $pp)
    {
        $this->pp->removeElement($pp);
    }
    /**
     * @var \LoginBundle\Entity\Usuario
     */
    private $usuario;


    /**
     * Set usuario
     *
     * @param \LoginBundle\Entity\Usuario $usuario
     *
     * @return Producto
     */
    public function setUsuario(\LoginBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \LoginBundle\Entity\Usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}
