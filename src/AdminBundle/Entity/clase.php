<?php

namespace AdminBundle\Entity;

/**
 * clase
 */
class clase {

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var int
     */
    private $dfolio;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return clase
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre() {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return clase
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set dfolio
     *
     * @param integer $dfolio
     *
     * @return subclase
     */
    public function setDfolio($dfolio)
    {
        $this->dfolio = $dfolio;

        return $this;
    }

    /**
     * Get dfolio
     *
     * @return int
     */
    public function getDfolio()
    {
        return $this->dfolio;
    }
}
