<?php

namespace AdminBundle\Entity;

/**
 * UsuarioRoles
 */
class UsuarioRoles
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var \AdminBundle\Entity\Modulos
     */
    private $idmodulo;

    /**
     * @var \AdminBundle\Entity\Usuario
     */
    private $idusuario;

    /**
     * @var \AdminBundle\Entity\Roles
     */
    private $idrol;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return UsuarioRoles
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set idmodulo
     *
     * @param \AdminBundle\Entity\Modulos $idmodulo
     *
     * @return UsuarioRoles
     */
    public function setIdmodulo(\AdminBundle\Entity\Modulos $idmodulo = null)
    {
        $this->idmodulo = $idmodulo;

        return $this;
    }

    /**
     * Get idmodulo
     *
     * @return \AdminBundle\Entity\Modulos
     */
    public function getIdmodulo()
    {
        return $this->idmodulo;
    }

    /**
     * Set idusuario
     *
     * @param \AdminBundle\Entity\Usuario $idusuario
     *
     * @return UsuarioRoles
     */
    public function setIdusuario(\AdminBundle\Entity\Usuario $idusuario = null)
    {
        $this->idusuario = $idusuario;

        return $this;
    }

    /**
     * Get idusuario
     *
     * @return \AdminBundle\Entity\Usuario
     */
    public function getIdusuario()
    {
        return $this->idusuario;
    }

    /**
     * Set idrol
     *
     * @param \AdminBundle\Entity\Roles $idrol
     *
     * @return UsuarioRoles
     */
    public function setIdrol(\AdminBundle\Entity\Roles $idrol = null)
    {
        $this->idrol = $idrol;

        return $this;
    }

    /**
     * Get idrol
     *
     * @return \AdminBundle\Entity\Roles
     */
    public function getIdrol()
    {
        return $this->idrol;
    }
}

