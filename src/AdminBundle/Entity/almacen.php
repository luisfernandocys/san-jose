<?php

namespace AdminBundle\Entity;

/**
 * almacen
 */
class almacen
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $almacen;

    /**
     * @var string
     */
    private $descripcion;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set almacen
     *
     * @param string $almacen
     *
     * @return almacen
     */
    public function setAlmacen($almacen)
    {
        $this->almacen = $almacen;

        return $this;
    }

    /**
     * Get almacen
     *
     * @return string
     */
    public function getAlmacen()
    {
        return $this->almacen;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return almacen
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
}
