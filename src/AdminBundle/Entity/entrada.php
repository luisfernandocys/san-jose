<?php

namespace AdminBundle\Entity;

/**
 * entrada
 */
class entrada
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $producto;

    /**
     * @var string
     */
    private $codigo;
    
    /**
     * @var int
     */
    private $formaentrada;

    /**
     * @var float
     */
    private $cantidadingresa;
    /**
     * @var float
     */
    private $cantidadunidad;

    /**
     * @var int
     */
    private $cajas;

    /**
     * @var int
     */
    private $paquetes;

    /**
     * @var float
     */
    private $unidades;

    /**
     * @var \DateTime
     */
    private $fecharegistro;

    /**
     * @var int
     */
    private $proveedor;

    /**
     * @var int
     */
    private $adquirido;

    /**
     * @var \DateTime
     */
    private $fechaadquisicion;

    /**
     * @var int
     */
    private $almacen;

    /**
     * @var float
     */
    private $existencia;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set producto
     *
     * @param integer $producto
     *
     * @return entrada
     */
    public function setProducto($producto)
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return int
     */
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * Set formaentrada
     *
     * @param integer $formaentrada
     *
     * @return entrada
     */
    public function setFormaentrada($formaentrada)
    {
        $this->formaentrada = $formaentrada;

        return $this;
    }

    /**
     * Get formaentrada
     *
     * @return int
     */
    public function getFormaentrada()
    {
        return $this->formaentrada;
    }

    /**
     * Set cantidadingresa
     *
     * @param float $cantidadingresa
     *
     * @return entrada
     */
    public function setCantidadingresa($cantidadingresa)
    {
        $this->cantidadingresa = $cantidadingresa;

        return $this;
    }

    /**
     * Get cantidadingresa
     *
     * @return float
     */
    public function getCantidadingresa()
    {
        return $this->cantidadingresa;
    }
    /**
     * Set cantidadunidad
     *
     * @param float $cantidadunidad
     *
     * @return entrada
     */
    public function setCantidadunidad($cantidadunidad)
    {
        $this->cantidadunidad = $cantidadunidad;

        return $this;
    }

    /**
     * Get cantidadunidad
     *
     * @return float
     */
    public function getCantidadunidad()
    {
        return $this->cantidadunidad;
    }

    /**
     * Set cajas
     *
     * @param integer $cajas
     *
     * @return entrada
     */
    public function setCajas($cajas)
    {
        $this->cajas = $cajas;

        return $this;
    }

    /**
     * Get cajas
     *
     * @return int
     */
    public function getCajas()
    {
        return $this->cajas;
    }

    /**
     * Set paquetes
     *
     * @param integer $paquetes
     *
     * @return entrada
     */
    public function setPaquetes($paquetes)
    {
        $this->paquetes = $paquetes;

        return $this;
    }

    /**
     * Get paquetes
     *
     * @return int
     */
    public function getPaquetes()
    {
        return $this->paquetes;
    }

    /**
     * Set unidades
     *
     * @param float $unidades
     *
     * @return entrada
     */
    public function setUnidades($unidades)
    {
        $this->unidades = $unidades;

        return $this;
    }

    /**
     * Get unidades
     *
     * @return float
     */
    public function getUnidades()
    {
        return $this->unidades;
    }

    /**
     * Set fecharegistro
     *
     * @param \DateTime $fecharegistro
     *
     * @return entrada
     */
    public function setFecharegistro($fecharegistro)
    {
        $this->fecharegistro = $fecharegistro;

        return $this;
    }

    /**
     * Get fecharegistro
     *
     * @return \DateTime
     */
    public function getFecharegistro()
    {
        return $this->fecharegistro;
    }

    /**
     * Set proveedor
     *
     * @param integer $proveedor
     *
     * @return entrada
     */
    public function setProveedor($proveedor)
    {
        $this->proveedor = $proveedor;

        return $this;
    }

    /**
     * Get proveedor
     *
     * @return int
     */
    public function getProveedor()
    {
        return $this->proveedor;
    }

    /**
     * Set adquirido
     *
     * @param integer $adquirido
     *
     * @return entrada
     */
    public function setAdquirido($adquirido)
    {
        $this->adquirido = $adquirido;

        return $this;
    }

    /**
     * Get adquirido
     *
     * @return int
     */
    public function getAdquirido()
    {
        return $this->adquirido;
    }

    /**
     * Set fechaadquisicion
     *
     * @param \DateTime $fechaadquisicion
     *
     * @return entrada
     */
    public function setFechaadquisicion($fechaadquisicion)
    {
        $this->fechaadquisicion = $fechaadquisicion;

        return $this;
    }

    /**
     * Get fechaadquisicion
     *
     * @return \DateTime
     */
    public function getFechaadquisicion()
    {
        return $this->fechaadquisicion;
    }

    /**
     * Set almacen
     *
     * @param integer $almacen
     *
     * @return entrada
     */
    public function setAlmacen($almacen)
    {
        $this->almacen = $almacen;

        return $this;
    }

    /**
     * Get almacen
     *
     * @return int
     */
    public function getAlmacen()
    {
        return $this->almacen;
    }

    /**
     * Set existencia
     *
     * @param float $existencia
     *
     * @return entrada
     */
    public function setExistencia($existencia)
    {
        $this->existencia = $existencia;

        return $this;
    }

    /**
     * Get existencia
     *
     * @return float
     */
    public function getExistencia()
    {
        return $this->existencia;
    }
    
    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return entrada
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }
    /**
     * @var string
     */
    private $entrega;


    /**
     * Set entrega
     *
     * @param string $entrega
     *
     * @return entrada
     */
    public function setEntrega($entrega)
    {
        $this->entrega = $entrega;

        return $this;
    }

    /**
     * Get entrega
     *
     * @return string
     */
    public function getEntrega()
    {
        return $this->entrega;
    }
    /**
     * @var float
     */
    private $numero;

    /**
     * @var float
     */
    private $peso;

    /**
     * @var float
     */
    private $ancho;

    /**
     * @var float
     */
    private $longitud;

    /**
     * @var string
     */
    private $densidad;


    /**
     * Set numero
     *
     * @param float $numero
     *
     * @return entrada
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return float
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set peso
     *
     * @param float $peso
     *
     * @return entrada
     */
    public function setPeso($peso)
    {
        $this->peso = $peso;

        return $this;
    }

    /**
     * Get peso
     *
     * @return float
     */
    public function getPeso()
    {
        return $this->peso;
    }

    /**
     * Set ancho
     *
     * @param float $ancho
     *
     * @return entrada
     */
    public function setAncho($ancho)
    {
        $this->ancho = $ancho;

        return $this;
    }

    /**
     * Get ancho
     *
     * @return float
     */
    public function getAncho()
    {
        return $this->ancho;
    }

    /**
     * Set longitud
     *
     * @param float $longitud
     *
     * @return entrada
     */
    public function setLongitud($longitud)
    {
        $this->longitud = $longitud;

        return $this;
    }

    /**
     * Get longitud
     *
     * @return float
     */
    public function getLongitud()
    {
        return $this->longitud;
    }

    /**
     * Set densidad
     *
     * @param string $densidad
     *
     * @return entrada
     */
    public function setDensidad($densidad)
    {
        $this->densidad = $densidad;

        return $this;
    }

    /**
     * Get densidad
     *
     * @return string
     */
    public function getDensidad()
    {
        return $this->densidad;
    }
    /**
     * @var \AdminBundle\Entity\Unidad
     */
    private $unidad;


    /**
     * Set unidad
     *
     * @param \AdminBundle\Entity\Unidad $unidad
     *
     * @return entrada
     */
    public function setUnidad(\AdminBundle\Entity\Unidad $unidad = null)
    {
        $this->unidad = $unidad;

        return $this;
    }

    /**
     * Get unidad
     *
     * @return \AdminBundle\Entity\Unidad
     */
    public function getUnidad()
    {
        return $this->unidad;
    }
}
