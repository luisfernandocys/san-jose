<?php

namespace AdminBundle\Entity;

/**
 * Proveedor
 */
class Proveedor {

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $apellidos;

    /**
     * @var string
     */
    private $empresa;

    /**
     * @var string
     */
    private $rfc;

    /**
     * @var string
     */
    private $folio;

    /**
     * @var string
     */
    private $tipo;

    /**
     * @var string
     */
    private $correo;

    /**
     * @var string
     */
    private $pagina;

    /**
     * @var string
     */
    private $calle;

    /**
     * @var string
     */
    private $numero;

    /**
     * @var string
     */
    private $colonia;

    /**
     * @var string
     */
    private $municipio;

    /**
     * @var string
     */
    private $estado;

    /**
     * @var string
     */
    private $telefono;

    /**
     * @var string
     */
    private $contacto;

    /**
     * @var string
     */
    private $telcontacto;

    /**
     * @var string
     */
    private $mailcontacto;

    /**
     * @var int
     */
    private $rama;

    /**
     * @var string
     */
    private $tamanio;

    /**
     * @var string
     */
    private $actividad;

    /**
     * @var string
     */
    private $servicios;

    /**
     * @var string
     */
    private $titular;

    /**
     * @var string
     */
    private $rfccuenta;

    /**
     * @var string
     */
    private $cuenta;

    /**
     * @var string
     */
    private $banco;

    /**
     * @var string
     */
    private $clabe;

    /**
     * @var \DateTime
     */
    private $fecha;

    /**
     * @var int
     */
    private $dfolio;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Proveedor
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre() {
        return $this->nombre;
    }

    /**
     * Set apellidos
     *
     * @param string $apellidos
     *
     * @return Proveedor
     */
    public function setApellidos($apellidos) {
        $this->apellidos = $apellidos;

        return $this;
    }

    /**
     * Get apellidos
     *
     * @return string
     */
    public function getApellidos() {
        return $this->apellidos;
    }

    /**
     * Set empresa
     *
     * @param string $empresa
     *
     * @return Proveedor
     */
    public function setEmpresa($empresa) {
        $this->empresa = $empresa;

        return $this;
    }

    /**
     * Get empresa
     *
     * @return string
     */
    public function getEmpresa() {
        return $this->empresa;
    }

    /**
     * Set rfc
     *
     * @param string $rfc
     *
     * @return Proveedor
     */
    public function setRfc($rfc) {
        $this->rfc = $rfc;

        return $this;
    }

    /**
     * Get rfc
     *
     * @return string
     */
    public function getRfc() {
        return $this->rfc;
    }

    /**
     * Set folio
     *
     * @param string $folio
     *
     * @return Proveedor
     */
    public function setFolio($folio) {
        $this->folio = $folio;

        return $this;
    }

    /**
     * Get folio
     *
     * @return string
     */
    public function getFolio() {
        return $this->folio;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return Proveedor
     */
    public function setTipo($tipo) {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo() {
        return $this->tipo;
    }

    /**
     * Set correo
     *
     * @param string $correo
     *
     * @return Proveedor
     */
    public function setCorreo($correo) {
        $this->correo = $correo;

        return $this;
    }

    /**
     * Get correo
     *
     * @return string
     */
    public function getCorreo() {
        return $this->correo;
    }

    /**
     * Set pagina
     *
     * @param string $pagina
     *
     * @return Proveedor
     */
    public function setPagina($pagina) {
        $this->pagina = $pagina;

        return $this;
    }

    /**
     * Get pagina
     *
     * @return string
     */
    public function getPagina() {
        return $this->pagina;
    }

    /**
     * Set calle
     *
     * @param string $calle
     *
     * @return Proveedor
     */
    public function setCalle($calle) {
        $this->calle = $calle;

        return $this;
    }

    /**
     * Get calle
     *
     * @return string
     */
    public function getCalle() {
        return $this->calle;
    }

    /**
     * Set numero
     *
     * @param string $numero
     *
     * @return Proveedor
     */
    public function setNumero($numero) {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string
     */
    public function getNumero() {
        return $this->numero;
    }

    /**
     * Set colonia
     *
     * @param string $colonia
     *
     * @return Proveedor
     */
    public function setColonia($colonia) {
        $this->colonia = $colonia;

        return $this;
    }

    /**
     * Get colonia
     *
     * @return string
     */
    public function getColonia() {
        return $this->colonia;
    }

    /**
     * Set municipio
     *
     * @param string $municipio
     *
     * @return Proveedor
     */
    public function setMunicipio($municipio) {
        $this->municipio = $municipio;

        return $this;
    }

    /**
     * Get municipio
     *
     * @return string
     */
    public function getMunicipio() {
        return $this->municipio;
    }

    /**
     * Set estado
     *
     * @param string $estado
     *
     * @return Proveedor
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return Proveedor
     */
    public function setTelefono($telefono) {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono() {
        return $this->telefono;
    }

    /**
     * Set contacto
     *
     * @param string $contacto
     *
     * @return Proveedor
     */
    public function setContacto($contacto) {
        $this->contacto = $contacto;

        return $this;
    }

    /**
     * Get contacto
     *
     * @return string
     */
    public function getContacto() {
        return $this->contacto;
    }

    /**
     * Set telcontacto
     *
     * @param string $telcontacto
     *
     * @return Proveedor
     */
    public function setTelcontacto($telcontacto) {
        $this->telcontacto = $telcontacto;

        return $this;
    }

    /**
     * Get telcontacto
     *
     * @return string
     */
    public function getTelcontacto() {
        return $this->telcontacto;
    }

    /**
     * Set mailcontacto
     *
     * @param string $mailcontacto
     *
     * @return Proveedor
     */
    public function setMailcontacto($mailcontacto) {
        $this->mailcontacto = $mailcontacto;

        return $this;
    }

    /**
     * Get mailcontacto
     *
     * @return string
     */
    public function getMailcontacto() {
        return $this->mailcontacto;
    }

    /**
     * Set rama
     *
     * @param string $rama
     *
     * @return Proveedor
     */
    public function setRama($rama) {
        $this->rama = $rama;

        return $this;
    }

    /**
     * Get rama
     *
     * @return string
     */
    public function getRama() {
        return $this->rama;
    }

    /**
     * Set tamanio
     *
     * @param string $tamanio
     *
     * @return Proveedor
     */
    public function setTamanio($tamanio) {
        $this->tamanio = $tamanio;

        return $this;
    }

    /**
     * Get tamanio
     *
     * @return string
     */
    public function getTamanio() {
        return $this->tamanio;
    }

    /**
     * Set actividad
     *
     * @param string $actividad
     *
     * @return Proveedor
     */
    public function setActividad($actividad) {
        $this->actividad = $actividad;

        return $this;
    }

    /**
     * Get actividad
     *
     * @return string
     */
    public function getActividad() {
        return $this->actividad;
    }

    /**
     * Set servicios
     *
     * @param string $servicios
     *
     * @return Proveedor
     */
    public function setServicios($servicios) {
        $this->servicios = $servicios;

        return $this;
    }

    /**
     * Get servicios
     *
     * @return string
     */
    public function getServicios() {
        return $this->servicios;
    }

    /**
     * Set titular
     *
     * @param string $titular
     *
     * @return Proveedor
     */
    public function setTitular($titular) {
        $this->titular = $titular;

        return $this;
    }

    /**
     * Get titular
     *
     * @return string
     */
    public function getTitular() {
        return $this->titular;
    }

    /**
     * Set rfccuenta
     *
     * @param string $rfccuenta
     *
     * @return Proveedor
     */
    public function setRfccuenta($rfccuenta) {
        $this->rfccuenta = $rfccuenta;

        return $this;
    }

    /**
     * Get rfccuenta
     *
     * @return string
     */
    public function getRfccuenta() {
        return $this->rfccuenta;
    }

    /**
     * Set cuenta
     *
     * @param string $cuenta
     *
     * @return Proveedor
     */
    public function setCuenta($cuenta) {
        $this->cuenta = $cuenta;

        return $this;
    }

    /**
     * Get cuenta
     *
     * @return string
     */
    public function getCuenta() {
        return $this->cuenta;
    }

    /**
     * Set banco
     *
     * @param string $banco
     *
     * @return Proveedor
     */
    public function setBanco($banco) {
        $this->banco = $banco;

        return $this;
    }

    /**
     * Get banco
     *
     * @return string
     */
    public function getBanco() {
        return $this->banco;
    }

    /**
     * Set clabe
     *
     * @param string $clabe
     *
     * @return Proveedor
     */
    public function setClabe($clabe) {
        $this->clabe = $clabe;

        return $this;
    }

    /**
     * Get clabe
     *
     * @return string
     */
    public function getClabe() {
        return $this->clabe;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Proveedor
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha() {
        return $this->fecha;
    }

    /**
     * Set dfolio
     *
     * @param integer $dfolio
     *
     * @return subclase
     */
    public function setDfolio($dfolio) {
        $this->dfolio = $dfolio;

        return $this;
    }

    /**
     * Get dfolio
     *
     * @return int
     */
    public function getDfolio() {
        return $this->dfolio;
    }

}
