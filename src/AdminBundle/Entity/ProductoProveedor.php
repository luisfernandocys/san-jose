<?php

namespace AdminBundle\Entity;

/**
 * ProductoProveedor
 */
class ProductoProveedor
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $producto;

    /**
     * @var int
     */
    private $proveedor;

    /**
     * @var float
     */
    private $precio;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set producto
     *
     * @param integer $producto
     *
     * @return ProductoProveedor
     */
    public function setProducto($producto)
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return int
     */
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * Set proveedor
     *
     * @param integer $proveedor
     *
     * @return ProductoProveedor
     */
    public function setProveedor($proveedor)
    {
        $this->proveedor = $proveedor;

        return $this;
    }

    /**
     * Get proveedor
     *
     * @return int
     */
    public function getProveedor()
    {
        return $this->proveedor;
    }

    /**
     * Set precio
     *
     * @param float $precio
     *
     * @return ProductoProveedor
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return float
     */
    public function getPrecio()
    {
        return $this->precio;
    }
    /**
     * @var float
     */
    private $precio2;

    /**
     * @var float
     */
    private $precio3;


    /**
     * Set precio2
     *
     * @param float $precio2
     *
     * @return ProductoProveedor
     */
    public function setPrecio2($precio2)
    {
        $this->precio2 = $precio2;

        return $this;
    }

    /**
     * Get precio2
     *
     * @return float
     */
    public function getPrecio2()
    {
        return $this->precio2;
    }

    /**
     * Set precio3
     *
     * @param float $precio3
     *
     * @return ProductoProveedor
     */
    public function setPrecio3($precio3)
    {
        $this->precio3 = $precio3;

        return $this;
    }

    /**
     * Get precio3
     *
     * @return float
     */
    public function getPrecio3()
    {
        return $this->precio3;
    }
    /**
     * @var float
     */
    private $precio1;


    /**
     * Set precio1
     *
     * @param float $precio1
     *
     * @return ProductoProveedor
     */
    public function setPrecio1($precio1)
    {
        $this->precio1 = $precio1;

        return $this;
    }

    /**
     * Get precio1
     *
     * @return float
     */
    public function getPrecio1()
    {
        return $this->precio1;
    }
    /**
     * @var \AdminBundle\Entity\entrada
     */
    private $entrada;


    /**
     * Set entrada
     *
     * @param \AdminBundle\Entity\entrada $entrada
     *
     * @return ProductoProveedor
     */
    public function setEntrada(\AdminBundle\Entity\entrada $entrada = null)
    {
        $this->entrada = $entrada;

        return $this;
    }

    /**
     * Get entrada
     *
     * @return \AdminBundle\Entity\entrada
     */
    public function getEntrada()
    {
        return $this->entrada;
    }
}
