<?php

namespace AdminBundle\Entity;

/**
 * detalleentrada
 */
class detalleentrada
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $entrada;
    
    /**
     * @var int
     */
    private $vendido;
    /**
     * @var float
     */
    private $valor;

    /**
     * @var string
     */
    private $folio;

    /**
     * @var string
     */
    private $codigo;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entrada
     *
     * @param integer $entrada
     *
     * @return detalleentrada
     */
    public function setEntrada($entrada)
    {
        $this->entrada = $entrada;

        return $this;
    }

    /**
     * Get entrada
     *
     * @return int
     */
    public function getEntrada()
    {
        return $this->entrada;
    }
    
    ///////////
    /**
     * Set vendido
     *
     * @param integer $vendido
     *
     * @return detalleentrada
     */
    public function setVendido($vendido)
    {
        $this->vendido = $vendido;

        return $this;
    }

    /**
     * Get vendido
     *
     * @return int
     */
    public function getVendido()
    {
        return $this->vendido;
    }
    
    /**
     * Set valor
     *
     * @param integer $valor
     *
     * @return detalleentrada
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return int
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set folio
     *
     * @param string $folio
     *
     * @return detalleentrada
     */
    public function setFolio($folio)
    {
        $this->folio = $folio;

        return $this;
    }

    /**
     * Get folio
     *
     * @return string
     */
    public function getFolio()
    {
        return $this->folio;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return detalleentrada
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }
}
