<?php

namespace AdminBundle\Entity;

/**
 * codigo
 */
class codigo
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $producto;

    /**
     * @var int
     */
    private $consecutivo;
    

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set producto
     *
     * @param integer $producto
     *
     * @return codigo
     */
    public function setProducto($producto)
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return int
     */
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * Set consecutivo
     *
     * @param integer $consecutivo
     *
     * @return codigo
     */
    public function setConsecutivo($consecutivo)
    {
        $this->consecutivo = $consecutivo;

        return $this;
    }

    /**
     * Get consecutivo
     *
     * @return int
     */
    public function getConsecutivo()
    {
        return $this->consecutivo;
    }
    
    
}
