<?php

namespace AdminBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProductosControllerTest extends WebTestCase
{
    public function testProductos()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/productos');
    }

    public function testCrear()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/productos/crear');
    }

    public function testDetalle()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/productos/detalle/{id}');
    }

}
