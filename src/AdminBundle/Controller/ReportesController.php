<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AdminBundle\Entity\Producto;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use AdminBundle\Entity\entrada;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class ReportesController extends Controller {

    public function reportesAction() {
        $permisosmodulo = $this->permisos_por_modulo();

        if (!in_array('LECTURA', $permisosmodulo)) {
            throw new AccessDeniedException('No tienes permiso para ver esta página');
        }

        $em = $this->getDoctrine()->getManager();


        //if contador == 0
        //$entradas = $em->getRepository('AdminBundle:entrada')->entradasProducto(); //método anterior
        $entradas= $em->getRepository('AdminBundle:entrada')->entradasProductoDatos();
        //$existencias = $em->getRepository('AdminBundle:Producto')->existenciasProductos();
        $prod = array();
        $exist = array();
        $l = sizeof($entradas);
        for ($i = 0; $i < $l; $i++) {
            $productos[$i] = $entradas[$i]['producto'];
            $cantidadunidad[$i] = $em->getRepository('AdminBundle:entrada')->ReporteGeneralEntradas($entradas[$i]['producto']['id']);
            $presalidas[$i] = $em->getRepository('AdminBundle:salida')->ReporteGeneralSalidas($entradas[$i]['producto']['id']);
            if ($presalidas[$i][0][1]) {
                /*print_r('algo');
                print_r($salidas[$i][0]);*/
                $salidas[$i]=$presalidas[$i][0][1];
            } else {
                 
                 $salidas[$i]=0;
                
            }
            $egenerales[$i] = $cantidadunidad[$i][0][1]; //$egenerales es 'entradasgenerales'
            
            //existencias
            $prod[$i] = $em->getRepository('AdminBundle:Producto')->existenciasProductos($entradas[$i]['producto']['id']);
            

            $exist[$i] = intval(/*$prod[$i][0]['valor']) *  (*/$prod[$i][0][1]);
        }
        
         //$prods = $em->getRepository('AdminBundle:Producto')->productos();
        
        
       
        
      
        return $this->render('AdminBundle:Reportes:reportes.html.twig', array('productos' => $productos, 'entradasgenerales' => $egenerales,'salidasgenerales'=>$salidas,'existencias'=>$exist));
    }
    
    public function ppproveedorAction()
    {
        $em = $this->getDoctrine()->getManager();
        $proveedores = $em->getRepository('AdminBundle:Proveedor')->findAll();
        return $this->render('AdminBundle:Reportes:ppproveedor.html.twig',array('proveedor'=>$proveedores));
  
    }
    public function ppproveedordetalleAction(Request $request)
    {
        
        $em = $this->getDoctrine()->getManager();
        $repp = $em->getRepository('AdminBundle:ProductoProveedor');
        $post = $request->request->all();
       
        
        $detalle = $repp->detalleRppp($post['id']);
        
        $response = array("code" => 100, "success" => true, 'detalle' => $detalle);
        return new Response(json_encode($response), 200, Array('Content-Type', 'application/json;  charset=utf-8'));
         
   
    }
//PRECIO POR PRODUCTO
     public function ppproductoAction() //Pinta la vista
    {
        
        $em = $this->getDoctrine()->getManager();
        $productos = $em->getRepository('AdminBundle:Producto')->findAll();
        return $this->render('AdminBundle:Reportes:ppproducto.html.twig',array('producto'=>$productos));
  
    }
    
    public function ppproductodetalleAction(Request $request)
    {
        
        $em = $this->getDoctrine()->getManager();
        $repp = $em->getRepository('AdminBundle:ProductoProveedor');
        $post = $request->request->all();
       
        
        $detalle = $repp->detalleRppproducto($post['id']);
        
        $response = array("code" => 100, "success" => true, 'detalle' => $detalle);
        return new Response(json_encode($response), 200, Array('Content-Type', 'application/json;  charset=utf-8'));
         
   
    }
    //PRODUCTOS Y UNIDADES INGRESADOS
    public function puiAction()
    {
        $em = $this->getDoctrine()->getManager();
        $proveedores = $em->getRepository('AdminBundle:Proveedor')->findAll();
        $contprov = $em->getRepository('AdminBundle:Proveedor')->numProveedores();
        $reporte = $em->getRepository('AdminBundle:entrada')->entradasProveedorProducto();
        $totalprod = $em->getRepository('AdminBundle:entrada')->totalProducto();
       

        return $this->render('AdminBundle:Reportes:pui.html.twig',array('proveedor'=>$proveedores,'reporte'=>$reporte,'numprove'=>count($contprov),'numprod'=>count($totalprod)));
  
    }
    public function permisos_por_modulo() {
        #USUARIO ACTUAL Y SUS PERMISOS
        $currentuser = $this->get('security.context')->getToken()->getUser();

        $idmodulo = 8;
        $em = $this->getDoctrine()->getManager();
        $usuariopermiso = $em->getRepository('LoginBundle:Usuario')->permisospormodulo($currentuser->getId(), $idmodulo);

        $permisos = array();
        for ($i = 0; $i < count($usuariopermiso); $i++) {
            $permisos[$i] = $usuariopermiso[$i]['idrol']['nombre'];
        }

        return $permisos;
    }

}
