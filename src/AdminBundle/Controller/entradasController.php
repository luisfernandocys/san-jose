<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AdminBundle\Entity\Producto;
use AdminBundle\Entity\ProductoProveedor;
use AdminBundle\Entity\Proveedor;
use AdminBundle\Entity\Unidad;
use AdminBundle\Entity\codigo;
use AdminBundle\Entity\entrada;
use AdminBundle\Entity\detalleentrada;
use AdminBundle\Entity\formaentrada;
use DateTime;

class entradasController extends Controller {

    public function entradasAction() {
        $permisosmodulo = $this->permisos_por_modulo();

        if (!in_array('LECTURA', $permisosmodulo)) {
            throw new AccessDeniedException('No tienes permiso para ver esta página');
        }

        $em = $this->getDoctrine()->getManager();
        $repu = $em->getRepository('AdminBundle:Unidad');
        $repp = $em->getRepository('AdminBundle:Proveedor');
        $unidad = $repu->UnidadMedida();
        $proveedor = $repp->Proveedores();
        $formaentrada = $em->getRepository('AdminBundle:formaentrada')->findAll();
        $almacen = $em->getRepository('AdminBundle:almacen')->findAll();
        //para llenar ddl
        $productos = $em->getRepository('AdminBundle:Producto')->findAll();
        $entradas = $em->getRepository('AdminBundle:entrada')->findAll();
        


        return $this->render('AdminBundle:entradas:entradas.html.twig', array('entradas' => $entradas, 'almacen' => $almacen, 'productos' => $productos, 'ppm' => $permisosmodulo, 'unidad' => $unidad, 'proveedor' => $proveedor, 'formaentrada' => $formaentrada));
    }
    
    public function entradasfechasAction(Request $request) {

        $permisosmodulo = $this->permisos_por_modulo();

        if (!in_array('LECTURA', $permisosmodulo)) {
            throw new AccessDeniedException('No tienes permiso para ver esta página');
        }

        $em = $this->getDoctrine()->getManager();
        $repu = $em->getRepository('AdminBundle:Unidad');
        $repp = $em->getRepository('AdminBundle:Proveedor');
        $unidad = $repu->UnidadMedida();
        $proveedor = $repp->Proveedores();
        $formaentrada = $em->getRepository('AdminBundle:formaentrada')->findAll();
        $almacen = $em->getRepository('AdminBundle:almacen')->findAll();
        //para llenar ddl
        $productos = $em->getRepository('AdminBundle:Producto')->findAll();
        
        if ($request->getMethod() == 'POST') {


            $post = $request->request->all();


            $mina = $post['min'];
            $maxa = $post['max'];
            

            if (empty($mina) && empty($maxa)) {
                $entradas = $em->getRepository('AdminBundle:entrada')->findAll();
            } elseif(!empty($mina) && empty($maxa)) {
                //print_r($post);
                //print_r('inicio no vacio');

                $min = DateTime::createFromFormat('d/m/Y',$mina);
                
                $max =  date_create(date('Y/m/d'));
                $entradas = $em->getRepository('AdminBundle:entrada')->entradasfecha($min, $max);
                 return $this->render('AdminBundle:entradas:entradas.html.twig', array('entradas' => $entradas, 'almacen' => $almacen, 'productos' => $productos, 'ppm' => $permisosmodulo, 'unidad' => $unidad, 'proveedor' => $proveedor, 'formaentrada' => $formaentrada, 'min' => $min, 'max' => $max));
            }
            elseif((empty($mina) && !empty($maxa)))
            {
               // print_r($post);
                //print_r('final no vacio');

                $min = DateTime::createFromFormat('d/m/Y', '01/01/2000');
                $max = DateTime::createFromFormat('d/m/Y', $maxa);
                $entradas = $em->getRepository('AdminBundle:entrada')->entradasfecha($min, $max);
                 return $this->render('AdminBundle:entradas:entradas.html.twig', array('entradas' => $entradas, 'almacen' => $almacen, 'productos' => $productos, 'ppm' => $permisosmodulo, 'unidad' => $unidad, 'proveedor' => $proveedor, 'formaentrada' => $formaentrada, 'min' => $min, 'max' => $max));
            
            }else
            {
                //print_r($post);

               // print_r('los 2 seteados');

                $min = DateTime::createFromFormat('d/m/Y', $mina);
                $max = DateTime::createFromFormat('d/m/Y', $maxa);
                $entradas = $em->getRepository('AdminBundle:entrada')->entradasfecha($min, $max);
                 
                 return $this->render('AdminBundle:entradas:entradas.html.twig', array('entradas' => $entradas, 'almacen' => $almacen, 'productos' => $productos, 'ppm' => $permisosmodulo, 'unidad' => $unidad, 'proveedor' => $proveedor, 'formaentrada' => $formaentrada, 'min' => $min, 'max' => $max));
       
            
            }
        } else {
            //si no es post
           
            $entradas = $em->getRepository('AdminBundle:entrada')->findAll();
             return $this->render('AdminBundle:entradas:entradas.html.twig', array('entradas' => $entradas, 'almacen' => $almacen, 'productos' => $productos, 'ppm' => $permisosmodulo, 'unidad' => $unidad, 'proveedor' => $proveedor, 'formaentrada' => $formaentrada));
       
        }

        if (!in_array('LECTURA', $permisosmodulo)) {
            throw new AccessDeniedException('No tienes permiso para ver esta página');
        }

        




        return $this->render('AdminBundle:entradas:entradas.html.twig', array('entradas' => $entradas, 'almacen' => $almacen, 'productos' => $productos, 'ppm' => $permisosmodulo, 'unidad' => $unidad, 'proveedor' => $proveedor, 'formaentrada' => $formaentrada));
       
    }

    public function crearAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if ($request->getMethod() == 'POST') {
            try {
                $em->getConnection()->beginTransaction();
                $post = $request->request->all();

                $entrada = new entrada();

                //totla de codigos
                $contcodigo = 0;


                $producto = $post['producto'];
                $formaentrada = $post['formaentrada'];
                $almacen = $post['almacen'];
                $cantidadingresa = $post['cantidadingresa'];
                $cantidadunidad = $post['cantidadunidadh'];
                //$proveedor = $post['proveedor'];
                $entrega = $post['entrega'];
                //nuevos campos
                $proveedor = $post['proveedor_ddl1'];
                $numero = $post['numero'];
                $unidad = $post['unidadmedida_ddl'];
                $ancho = $post['ancho'];
                $longitud = $post['longitud'];
                $peso = $post['peso'];

                if ($almacen > 0 && $almacen < 9999) {
                    $entrada->setFechaadquisicion(date_create(date('Y/m/d H:i:s')));
                    $entrada->setAdquirido(1);
                    $entrada->setExistencia(1);
                }
                if ($formaentrada == 1) {
                    $unidades = $post['unidades'];
                    $entrada->setUnidades($unidades);
                    $contcodigo = $unidades;
                } else if ($formaentrada == 2) {
                    $unidades = $post['unidades'];
                    $entrada->setUnidades($unidades);
                    $paquetes = $post['paquetes'];
                    $entrada->setPaquetes($paquetes);
                    $contcodigo = $paquetes;
                } else {
                    $cajas = $post['cajas'];
                    $entrada->setCajas($cajas);
                    $unidades = $post['unidades'];
                    $entrada->setUnidades($unidades);
                    $paquetes = $post['paquetes'];
                    $entrada->setPaquetes($paquetes);
                    $contcodigo = $cajas;
                }
                // $fecharegistro = $post['fecharegistro'];
                //seteos faltantes
                $prod = $em->getRepository('AdminBundle:Producto')->findOneByid($producto);
                $entrada->setProducto($prod);
                $fe = $em->getRepository('AdminBundle:formaentrada')->findOneByid($formaentrada);
                $entrada->setFormaentrada($fe);
                $prov = $em->getRepository('AdminBundle:Proveedor')->findOneByid($proveedor);
                $entrada->setProveedor($prov);
                $alma = $em->getRepository('AdminBundle:almacen')->findOneByid($almacen);
                $entrada->setAlmacen($alma);
                $entrada->setEntrega($entrega);
                $entrada->setCantidadingresa($cantidadingresa);
                $entrada->setCantidadunidad($cantidadunidad);

                $entrada->setFecharegistro(date_create(date('Y/m/d H:i:s')));
                //nuevos campos
                $entrada->setNumero($numero);
                $un = $em->getRepository('AdminBundle:Unidad')->findOneByid($unidad);
                $entrada->setUnidad($un);

                $entrada->setAncho($ancho);
                $entrada->setLongitud($longitud);

                $entrada->setPeso($peso);
                $em->persist($entrada);
                $em->flush();


 //proveedores producto
            //proveedores inserción

            $proveedor[1] = $post['proveedor_ddl1'];
            //$proveedor[2] = $post['det_proveedor2'];
            //$proveedor[3] = $post['det_proveedor3'];
            //$proveedor[4] = $post['det_proveedor4'];

            $idinsertadopp = $entrada->getId();
            $preprov = "";
            //BORRAR MIS DETALLES PRODUCTO PROVEEDOR DE CIERTO PRODUCTO
            //$del = $this->getDoctrine()->getManager()->getRepository('AdminBundle:ProductoProveedor')->prodprov($id);

         //   foreach ($del as $del) {
           //     $em->remove($del);
           // }
           // $em->flush();
            for ($i = 1; $i <= 1; $i++) { //cuando sean 4 proveedores, se cambia aquí
                if ($proveedor[$i] > 0) {
                    $pp = new ProductoProveedor();
                    //$this->getDoctrine()->getManager()->getRepository('AdminBundle:Producto')->findOneByid($id);
                    $prov = $em->getRepository('AdminBundle:Proveedor')->findOneByid($proveedor[$i]);
                    $prodpp = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Entrada')->findOneByid($idinsertadopp);


                    //SECCION DE 3 PRECIOS POR PRODUCTO POR PROVEEDOR
                    $pp->setPrecio1($post['precio' . $i . "1"]);
                    $pp->setEntrada($prodpp);
                    $pp->setProveedor($prov);
                    $preprov+=$proveedor[$i]; //solo 1 vez para el folio
                    $em->persist($pp);
                    $em->flush();

                    $pp->setPrecio2($post['precio' . $i . "2"]);
                    $pp->setEntrada($prodpp);
                    $pp->setProveedor($prov);
                    //$preprov+=$proveedor[$i];
                    $em->persist($pp);
                    $em->flush();

                    $pp->setPrecio3($post['precio' . $i . "3"]);
                    $pp->setEntrada($prodpp);
                    $pp->setProveedor($prov);
                    //$preprov+=$proveedor[$i];
                    $em->persist($pp);
                    $em->flush();
                }
            }

/// DATOS PARA DETALLE ENTRADA y GENERACIÓN DE CÓDIGOS
                $identrada = $entrada->getId();
                $entradam = $this->getDoctrine()->getManager()->getRepository('AdminBundle:entrada')->findOneByid($identrada);
                $repp = $em->getRepository('AdminBundle:Producto');
                $detalle = $repp->datos_producto($producto);
                $valor = $post['valor'];
                //obtener consecutivo
                $repc = $em->getRepository('AdminBundle:codigo');
                $preconsecutivo = $repc->datos_codigo($producto);
                $consecutivo = $preconsecutivo[0]['consecutivo'] + 1; //obtengo el último consecutivo insertado y le agrego 1, si era 0 , iniciará en 1 el siguiente

                $prefolio = $detalle[0]['folio'];
                $precodigo = $detalle[0]['codigo'];
                //recortar folio y codig
                $largofolio = strlen($prefolio);
                $largocodigo = strlen($precodigo);

                $prefolio2 = substr($prefolio, 0, (intval($largofolio) - 5));
                $precodigo2 = substr($precodigo, 0, (intval($largocodigo) - 5));

                $nuevofolio = array();
                $nuevocodigo = array();
                for ($i = 0; $i < $contcodigo; $i++) {
                    $detalleentrada = new detalleentrada();
                    $nuevofolio[$i] = $prefolio2 . date('z') . '-' . (intval($consecutivo) . '-TSJ'); //aumento el consecutivo
                    $nuevocodigo[$i] = $precodigo2 . date('z') . '-' . (intval($consecutivo) . '-TSJ');
                    $detalleentrada->setEntrada($entradam);
                    $detalleentrada->setFolio($nuevofolio[$i]);
                    $detalleentrada->setCodigo($nuevocodigo[$i]);
                    $detalleentrada->setValor($valor);
                    $detalleentrada->setVendido(1);
                    $consecutivo++;

                    $em->persist($detalleentrada);
                    $em->flush();
                }
                $nuevoconsecutivo = intval($consecutivo) - intval(1);


                $objcodigo = $this->getDoctrine()->getManager()->getRepository('AdminBundle:codigo')->findOneByproducto($prod);
                $objcodigo->setProducto($prod);
                $objcodigo->setConsecutivo($nuevoconsecutivo);

                $em->flush();


                $exito = 'Entrada y detalles Agregados Correctamente';
                $em->getConnection()->commit();
                $this->get('session')->getFlashBag()->add(
                        'notice', $exito
                );
                $url = $this->generateUrl("entradas");
                return $this->redirect($url);
            } catch (\Doctrine\DBAL\Exception\UniqueConstraintViolationException $u) {
                $ex = get_class($u);
                $em->getConnection()->rollback();
                $error = 'Error, ya existe un folio con el valor agregado, favor de verificar la información';
                $this->get('session')->getFlashBag()->add(
                        'warning', $error
                );
                $url = $this->generateUrl("entradas");
            } catch (\Exception $e) {
                $ex = get_class($e);
                $em->getConnection()->rollback();
                $error = 'Ocurrió un error del tipo: ' . $ex . ' Verifique la información que teclea sea válida y/o Contacte al Administrador del sistema. ' . $e;
                $this->get('session')->getFlashBag()->add(
                        'warning', $error
                );
                $url = $this->generateUrl("entradas");
                return $this->redirect($url);
            }
        }
        $url = $this->generateUrl("entradas");
        return $this->redirect($url);
    }

    public function entradas_proveedorAction($id) {
        $em = $this->getDoctrine()->getManager();
        $repp = $em->getRepository('AdminBundle:entrada');


        $proveedor = $repp->entradas_proveedor($id);
        $response = array("code" => 100, "success" => true, 'detalle' => $proveedor);
        return new Response(json_encode($response), 200, Array('Content-Type', 'application/json;  charset=utf-8'));
    }

    public function detalleAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $repp = $em->getRepository('AdminBundle:detalleentrada');
        $post = $request->request->all();
        $id = $post['id'];
        $detalle = $repp->detalle_entrada($id);
        $response = array("code" => 100, "success" => true, 'detalle' => $detalle);
        return new Response(json_encode($response), 200, Array('Content-Type', 'application/json;  charset=utf-8'));
    }

    public function detallecodigoAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $repp = $em->getRepository('AdminBundle:detalleentrada');
        $post = $request->request->all();
        $codigo = $post['codigo'];
        $detalle = $repp->detalle_entradacodigo($codigo);
        $response = array("code" => 100, "success" => true, 'detalle' => $detalle);
        return new Response(json_encode($response), 200, Array('Content-Type', 'application/json;  charset=utf-8'));
    }

    public function permisos_por_modulo() {
        #USUARIO ACTUAL Y SUS PERMISOS
        $currentuser = $this->get('security.context')->getToken()->getUser();

        $idmodulo = 5;
        $em = $this->getDoctrine()->getManager();
        $usuariopermiso = $em->getRepository('LoginBundle:Usuario')->permisospormodulo($currentuser->getId(), $idmodulo);

        $permisos = array();
        for ($i = 0; $i < count($usuariopermiso); $i++) {
            $permisos[$i] = $usuariopermiso[$i]['idrol']['nombre'];
        }

        return $permisos;
    }

}
