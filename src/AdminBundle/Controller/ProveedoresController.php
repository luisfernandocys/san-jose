<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\HttpFoundation\RedirectResponse;
use LoginBundle\Entity\Usuario;
use LoginBundle\Entity\UsuarioRoles;
use AdminBundle\Entity\Modulos;
use AdminBundle\Entity\Proveedor;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ProveedoresController extends Controller {

    function proveedoresAction() {
        $permisosmodulo = $this->permisos_por_modulo();
        $em = $this->getDoctrine()->getManager();
        $proveedores = $em->getRepository('AdminBundle:Proveedor')->findAll();
        if (!in_array('LECTURA', $permisosmodulo)) {
            throw new AccessDeniedException('No tienes permiso para ver esta página');
        }
        return $this->render('AdminBundle:proveedores:index.html.twig', array('ppm' => $permisosmodulo, 'proveedores' => $proveedores));
    }

    public function permisos_por_modulo() {
        #USUARIO ACTUAL Y SUS PERMISOS
        $currentuser = $this->get('security.context')->getToken()->getUser();

        $idmodulo = 4;
        $em = $this->getDoctrine()->getManager();
        $usuariopermiso = $em->getRepository('LoginBundle:Usuario')->permisospormodulo($currentuser->getId(), $idmodulo);

        $permisos = array();
        for ($i = 0; $i < count($usuariopermiso); $i++) {
            $permisos[$i] = $usuariopermiso[$i]['idrol']['nombre'];
        }

        return $permisos;
    }

    public function crearAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        
        if ($request->getMethod() == 'POST') {
            try {
                $em->getConnection()->beginTransaction();
                $post = $request->request->all();
               
                $nombre=$post['nombre'];
                $apellidos=$post['apellidos'];
                $empresa=$post['empresa'];
                $rfc=$post['rfc'];
                $folio=$post['folio'];
                $tipo=$post['tipo'];
                $email=$post['email'];
                $pagina=$post['pagina'];
                $calle=$post['calle'];
                $numero=$post['numero'];
                $colonia=$post['colonia'];
                $municipio=$post['municipio'];
                $estado=$post['estado'];
                $telefono=$post['tel'];
                $contacto=$post['contacto'];
                $telcontacto=$post['telcontacto'];
                $mailcontacto=$post['mailcontacto'];
                $rama=$post['rama'];
                $tamanio=$post['tamanio'];
                $actividad=$post['actividad'];
                $servicios=$post['servicios'];
                $rfccuenta=$post['rfccuenta'];
                $cuenta=$post['cuenta'];
                $banco=$post['banco'];
                $clabe=$post['clabe'];
                $titular=$post['titular'];
                //la fecha se pone la del día de alta, directo en el seteo
                //seteos
                $prov = new Proveedor();
                $prov->setNombre($nombre);
                $prov->setApellidos($apellidos);
                $prov->setEmpresa($empresa);
                $prov->setRfc($rfc);
                $prov->setFolio($folio);
                $prov->setTipo($tipo);
                $prov->setCorreo($email);
                $prov->setPagina($pagina);
                $prov->setCalle($calle);
                $prov->setNumero($numero);
                $prov->setColonia($colonia);
                $prov->setMunicipio($municipio);
                $prov->setEstado($estado);
                $prov->setTelefono($telefono);
                $prov->setContacto($contacto);
                $prov->setTelcontacto($telcontacto);
                $prov->setMailcontacto($mailcontacto);
                $prov->setRama($rama);
                $prov->setTamanio($tamanio);
                $prov->setActividad($actividad);
                $prov->setServicios($servicios);
                $prov->setRfccuenta($rfccuenta);
                $prov->setCuenta($cuenta);
                $prov->setBanco($banco);
                $prov->setClabe($clabe);
                $prov->setTitular($titular);                
                $prov->setFecha(date_create(date('Y/m/d')));
               
                $em->persist($prov);
                $em->flush();
                $exito = 'proveedor Agregado Correctamente';
                $em->getConnection()->commit();
                $this->get('session')->getFlashBag()->add('notice', $exito);
                $url = $this->generateUrl("proveedores");
                return $this->redirect($url);
            } catch (\Exception $e) {
                $ex = get_class($e);
                $em->getConnection()->rollback();
                $error = 'Ocurrió un error del tipo: ' . $ex . ' Verifique la información que teclea sea válida y/o Contacte al Administrador del sistema. ' . $e;
                $this->get('session')->getFlashBag()->add(
                        'warning', $error
                );
                $url = $this->generateUrl("proveedores");
                return $this->redirect($url);
            }
        }
        $url = $this->generateUrl("proveedores");
        return $this->redirect($url);
    }

    public function detalleAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $repp = $em->getRepository('AdminBundle:Proveedor');
        $post = $request->request->all();
        $id = $post['id'];
        $detalle = $repp->detalleProveedor($id);
        $response = array("code" => 100, "success" => true, 'detalle' => $detalle[0]);
        return new Response(json_encode($response), 200, Array('Content-Type', 'application/json;  charset=utf-8'));
    }
    
    public function modificarAction() {
        $em = $this->getDoctrine()->getManager();

        try {
            $em->getConnection()->beginTransaction();
            $request = $this->getRequest();
            $post = $request->request->all();
                $nombre=$post['det_nombre'];
                $apellidos=$post['det_apellidos'];
                $empresa=$post['det_empresa'];
                $rfc=$post['det_rfc'];
                $folio=$post['det_folio'];
                $tipo=$post['det_tipo'];
                $email=$post['det_correo'];
                $pagina=$post['det_pagina'];
                $calle=$post['det_calle'];
                $numero=$post['det_numero'];
                $colonia=$post['det_colonia'];
                $municipio=$post['det_municipio'];
                $estado=$post['det_estado'];
                $telefono=$post['det_telefono'];
                $contacto=$post['det_contacto'];
                $telcontacto=$post['det_telcontacto'];
                $mailcontacto=$post['det_mailcontacto'];
                $rama=$post['det_rama'];
                $tamanio=$post['det_tamanio'];
                $actividad=$post['det_actividad'];
                $servicios=$post['det_servicios'];
                $rfccuenta=$post['det_rfccuenta'];
                $cuenta=$post['det_cuenta'];
                $banco=$post['det_banco'];
                $clabe=$post['det_clabe'];
                $titular=$post['det_titular'];
            
            
            $id = $post['idproveedormodificar'];
            $prov = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Proveedor')->findOneByid($id);
           
                $prov->setNombre($nombre);
                $prov->setApellidos($apellidos);
                $prov->setEmpresa($empresa);
                $prov->setRfc($rfc);
                $prov->setFolio($folio);
                $prov->setTipo($tipo);
                $prov->setCorreo($email);
                $prov->setPagina($pagina);
                $prov->setCalle($calle);
                $prov->setNumero($numero);
                $prov->setColonia($colonia);
                $prov->setMunicipio($municipio);
                $prov->setEstado($estado);
                $prov->setTelefono($telefono);
                $prov->setContacto($contacto);
                $prov->setTelcontacto($telcontacto);
                $prov->setMailcontacto($mailcontacto);
                $prov->setRama($rama);
                $prov->setTamanio($tamanio);
                $prov->setActividad($actividad);
                $prov->setServicios($servicios);
                $prov->setRfccuenta($rfccuenta);
                $prov->setCuenta($cuenta);
                $prov->setBanco($banco);
                $prov->setClabe($clabe);
                $prov->setTitular($titular);                
            
            $em->flush();
            $response = array("code" => 100, "success" => true, 'proveedor' => $prov, 'status' => 'ok');
            //you can return result as JSON
            $exito = "Proveedor modificado correctamente";

            $em->getConnection()->commit();
            $this->get('session')->getFlashBag()->add(
                    'notice', $exito
            );
           return new Response(json_encode($response), 200, Array('Content-Type', 'application/json;  charset=utf-8'));
        } catch (\Exception $e) {
            $ex = get_class($e);
            $em->getConnection()->rollback();
            $error = 'Ocurrió un error del tipo: ' . $ex . ' Verifique la información que teclea sea válida y/o Contacte al Administrador del sistema. ' . $e;
            $this->get('session')->getFlashBag()->add(
                    'warning', $error
            );
        }
    }

}
