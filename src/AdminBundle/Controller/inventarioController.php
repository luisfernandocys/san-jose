<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AdminBundle\Entity\Producto;
use AdminBundle\Entity\Proveedor;
use AdminBundle\Entity\tipo;
use AdminBundle\Entity\Tipotejido;
use AdminBundle\Entity\clase;
use AdminBundle\Entity\modelo;
use AdminBundle\Entity\color;
use AdminBundle\Entity\Unidad;
use AdminBundle\Entity\codigo;
use Symfony\Component\Validator\Constraints;

class inventarioController extends Controller {

    public function inventarioAction() {
        $permisosmodulo = $this->permisos_por_modulo();

        if (!in_array('LECTURA', $permisosmodulo)) {
            throw new AccessDeniedException('No tienes permiso para ver esta página');
        }

        $em = $this->getDoctrine()->getManager();


        //if contador == 0
        $productos = $em->getRepository('AdminBundle:Producto')->productos();
        $entradas= $em->getRepository('AdminBundle:entrada')->entradasProductoDatos();
        
        
        //$existencias = $em->getRepository('AdminBundle:Producto')->existenciasProductos();
        $prod = array();
        $exis = array();
        $datos = array();
        $otro = array();
        $l = sizeof($productos);
        
        for ($i = 0; $i < $l; $i++) {
            $prod[$i] = $em->getRepository('AdminBundle:Producto')->existenciasProductos($productos[$i]['id']);

            $exist[$i] = intval(/*$prod[$i][0]['valor'])* */ $prod[$i][0][1]);
             $datos = $em->getRepository('AdminBundle:Producto')->datosotros($productos[$i]['id']);
                      
        }    
        


        
       
        


        return $this->render('AdminBundle:inventario:inventario.html.twig', array('productos' => $entradas, 'exist' => $exist));
    }

    public function disponibleAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $repp = $em->getRepository('AdminBundle:Producto');

        $post = $request->request->all();
        $id = $post['id'];
        $detalle = $em->getRepository('AdminBundle:Producto')->existenciaProducto($id);
        $existencia = count($detalle);
        $valor = $em->getRepository('AdminBundle:Producto')->valorProducto($id);
        $response = array("code" => 100, "success" => true, 'existencia' => $existencia, 'valor' => $valor);
        return new Response(json_encode($response), 200, Array('Content-Type', 'application/json;  charset=utf-8'));
    }
    
   

    public function permisos_por_modulo() {
        #USUARIO ACTUAL Y SUS PERMISOS
        $currentuser = $this->get('security.context')->getToken()->getUser();

        $idmodulo = 6;
        $em = $this->getDoctrine()->getManager();
        $usuariopermiso = $em->getRepository('LoginBundle:Usuario')->permisospormodulo($currentuser->getId(), $idmodulo);

        $permisos = array();
        for ($i = 0; $i < count($usuariopermiso); $i++) {
            $permisos[$i] = $usuariopermiso[$i]['idrol']['nombre'];
        }

        return $permisos;
    }

}
