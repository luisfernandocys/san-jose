<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\HttpFoundation\RedirectResponse;
use LoginBundle\Entity\Usuario;
use LoginBundle\Entity\UsuarioRoles;
use AdminBundle\Entity\Modulos;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class UsuariosController extends Controller {
    #Usuarios

    public function usuariosAction() {

        $permisosmodulo = $this->permisos_por_modulo();
       
        if (!in_array('LECTURA', $permisosmodulo) ) {
            throw new AccessDeniedException('No tienes permiso para ver esta página');
        }
        if (in_array('ESCRITURA', $permisosmodulo)) {
            $modulo2 = 'si';
        } else {
            $modulo2 = 'no';
        }


        $em = $this->getDoctrine()->getManager();
        $repu = $em->getRepository('LoginBundle:Usuario');
        $repm = $em->getRepository('AdminBundle:Modulos');
        $repp = $em->getRepository('AdminBundle:Modulos'); //lo saqué del mismo
        $usuarios = $repu->ObtenerUsuarios();
        $ups = $repu->UsuriosPermisosModulos();
        $cont = count($usuarios);

        $arreglo = array();

        for ($i = 0; $i < $cont; $i++) {
            $modulos = $repm->ObtenerModulosUsuario($usuarios[$i]['id']);

            $cont2 = count($modulos);
            $arreglo['usuario'][$i]['nombre'] = $usuarios[$i]['nombre'] . ' ' . $usuarios[$i]['apellidop'] . ' ' . $usuarios[$i]['apellidom'];
            for ($i = 0; $i < $cont; $i++) {
                $modulos = $repm->ObtenerModulosUsuario($usuarios[$i]['id']);
                $cont2 = count($modulos);
                $arreglo['usuario'][$i]['nombre'] = $usuarios[$i]['nombre'] . ' ' . $usuarios[$i]['apellidop'] . ' ' . $usuarios[$i]['apellidom'];
                $arreglo['usuario'][$i]['id'] = $usuarios[$i]['id'];
                for ($x = 0; $x < $cont2; $x++) { //for por módulo
                    #$arreglo['usuario'][$i]['modulo'][$x] = $modulos[$x]['idmodulo']['nombre'];
                    $tempnombre = $modulos[$x]['idmodulo']['nombre'];
                    $permisos = $repp->ObtenerPermisosModulos($modulos[$x]['idmodulo']['id'], $usuarios[$i]['id']);
                    $cont3 = count($permisos); //cuantos permisos hubo
                    for ($p = 0; $p < $cont3; $p++) {
                        $arreglo['usuario'][$i]['modulo'][$tempnombre]['permiso'][$p] = $permisos[$p]['idrol']['id']; //así se asigna
                    }
                    $arreglo['usuario'][$i]['modulo'][$tempnombre]['idmodulo'] = $modulos[$x]['idmodulo']['id'];
                }
            }
        }

        #  

        $request = $this->container->get('request');
        $currentRoute = $request->attributes->get('_route');
        $ruta = $this->get('router')->generate($currentRoute, array(), true);




        return $this->render('AdminBundle:usuarios:index.html.twig', array('usuarios_ddl' => $usuarios, 'usuarios' => $ups, 'modulos' => $modulos, 'permisos' => $permisos, 'ppm' => $permisosmodulo, 'modulo2' => $modulo2, 'tu' => $arreglo, 'ruta' => $ruta));
    }

    public function crearAction() {
        $em = $this->getDoctrine()->getManager();

        try {
            $em->getConnection()->beginTransaction();
            $request = $this->getRequest();
            $post = $request->request->all();
            $user = new Usuario();
            $nombre = $post['nombre'];
            $apellidop = $post['apellidop'];
            $apellidom = $post['apellidom'];
            $usuario = $post['usuario'];
            $password = $post['password'];
            $puesto = $post['puesto'];
            //$fecha = $post['fecha'];
            //$entrada->setFecha(date_create(date($fecha)));
            $user->setFechahabilitacion(date_create(date('Y/m/d')));
            $user->setNombre($nombre);
            $user->setApellidom($apellidom);
            $user->setApellidop($apellidop);
            $user->setUsuario($usuario);
            $user->setPuesto($puesto);
            //password codificado
            $pass = $this->get('security.password_encoder')->encodePassword($user, $password);
            $user->setPassword($pass);
            $em->persist($user);
            $em->flush();

            $ur = new UsuarioRoles();
            $ur->setIdusuario($em->getRepository('LoginBundle:Usuario')->find($user->getId()));
            $rol = $em->getRepository('LoginBundle:Roles')->find(4);
            $ur->setIdrol($rol);

            $modulo = $em->getRepository('AdminBundle:Modulos')->find(1);
            $ur->setIdmodulo($modulo);

            $em->persist($ur);
            $em->flush();
            $exito = 'Registro de Usuario Correcto';
            $em->getConnection()->commit();
            $this->get('session')->getFlashBag()->add(
                    'notice', $exito
            );

            $response = array("code" => 100, "success" => true, 'status' => 1, 'formulario' => $post);
            //you can return result as JSON
            return new Response(json_encode($response), 200, Array('Content-Type', 'application/json;  charset=utf-8'));
        } //try
        catch (\Exception $e) {
            $ex = get_class($e);
            $em->getConnection()->rollback();
            $error = 'Ocurrió un error del tipo: ' . $ex . ' Verifique la información que teclea sea válida y/o Contacte al Administrador del sistema. ' . $e;
            $this->get('session')->getFlashBag()->add(
                    'warning', $error
            );
            
        }
    }

    public function detalleAction($id) {



        $em = $this->getDoctrine()->getManager();
        $repu = $em->getRepository('LoginBundle:Usuario');
        $detalle = $repu->detalleUsuarios($id);


        $data1 = array($detalle[0]);

        $response = array("code" => 100, "success" => true, 'detalle' => $data1);
        return new Response(json_encode($response), 200, Array('Content-Type', 'application/json;  charset=utf-8'));
    }

    public function modificarAction() {
        $em = $this->getDoctrine()->getManager();

        try {
            $em->getConnection()->beginTransaction();
            $request = $this->getRequest();
            $post = $request->request->all();

            $id = $post['idusuariosmodificar'];
            $user = $this->getDoctrine()->getManager()->getRepository('LoginBundle:Usuario')->findOneByid($id);
            $nombre = $post['nombremodificar'];
            $apellidop = $post['apellidopmodificar'];
            $apellidom = $post['apellidommodificar'];
            $puesto = $post['puestomodificar'];
            $user->setNombre($nombre);
            $user->setApellidom($apellidom);
            $user->setApellidop($apellidop);
            $user->setPuesto($puesto);
            $em->flush();
            $response = array("code" => 100, "success" => true, 'usuario' => $user, 'status' => 'ok');
            //you can return result as JSON
            $exito = "Usuario modificado correctamente";

            $em->getConnection()->commit();
            $this->get('session')->getFlashBag()->add(
                    'notice', $exito
            );
            return new Response(json_encode($response), 200, Array('Content-Type', 'application/json;  charset=utf-8'));
        } catch (\Exception $e) {
            $ex = get_class($e);
            $em->getConnection()->rollback();
            $error = 'Ocurrió un error del tipo: ' . $ex . ' Verifique la información que teclea sea válida y/o Contacte al Administrador del sistema. ' . $e;
            $this->get('session')->getFlashBag()->add(
                    'warning', $error
            );
        }
    }

    public function eliminarAction() {
        
    }

    #permisos Usuarios

    public function permisosAction() {
        
    }

    public function permisosusuarioAction($id) {
        
    }

    public function permisoagregarAction() {
        $em = $this->getDoctrine()->getManager();

        try {
            $em->getConnection()->beginTransaction();
            $request = $this->getRequest();
            $post = $request->request->all();
            $usuario = $post['usuario'];
            $modulo = $post['modulo'];

            $user = $this->getDoctrine()->getManager()->getRepository('LoginBundle:Usuario')->findOneByid($usuario);
            $mod = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Modulos')->findOneByid($modulo);
            $rol = $this->getDoctrine()->getManager()->getRepository('LoginBundle:Roles')->findOneByid(1);
            $ur = new UsuarioRoles();
            $ur->setIdusuario($user);
            $ur->setIdrol($rol);
            $ur->setIdmodulo($mod);
            $em->persist($ur);
            $em->flush();
            
            $rol2 = $this->getDoctrine()->getManager()->getRepository('LoginBundle:Roles')->findOneByid(4);
            $ur2 = new UsuarioRoles();
            $ur2->setIdusuario($user);
            $ur2->setIdrol($rol2);
            $ur2->setIdmodulo($mod);
            $em->persist($ur2);
            $em->flush();

            $response = array("code" => 100, "success" => true, 'post' => $post, 'status' => 'ok');
            //you can return result as JSON
            $exito = "Permiso agregado correctamente";

            $em->getConnection()->commit();
            $this->get('session')->getFlashBag()->add(
                    'notice', $exito
            );
            return new Response(json_encode($response), 200, Array('Content-Type', 'application/json;  charset=utf-8'));
        } catch (\Exception $e) {
            $ex = get_class($e);
            $em->getConnection()->rollback();
            $error = 'Ocurrió un error del tipo: ' . $ex . ' Verifique la información que teclea sea válida y/o Contacte al Administrador del sistema. ' . $e;
            $this->get('session')->getFlashBag()->add(
                    'warning', $error
            );
        }
    }

    public function modulos_usuarioAction($id) {
        $em = $this->getDoctrine()->getManager();
        $repm = $em->getRepository('AdminBundle:Modulos');
        $modulos = $repm->ObtenerModulosUsuarioSinPermiso($id);


        $data1 = array($modulos);

        $response = array("code" => 100, "success" => true, 'modulos' => $data1[0]);
        return new Response(json_encode($response), 200, Array('Content-Type', 'application/json;  charset=utf-8'));
    }

    public function permisoseditarAction() {

        $em = $this->getDoctrine()->getManager();

        try {
            $em->getConnection()->beginTransaction();

            $request = $this->getRequest();
            $post = $request->request->all();
            $idmodulo = $post['idmodulo'];
            $idusuario = $post['idusuario'];
            $permisos = $post['permisos'];
          
            $cont = count($permisos);
            //objeto a borrar
            $repr = $this->getDoctrine()->getManager()->getRepository('LoginBundle:UsuarioRoles');
            $roles = $repr->registroroles($idusuario, $idmodulo);
            foreach ($roles as $roles) {
            $em->remove($roles);
            }
            $em->flush();
            $em->clear();
            //primero obtengo lo que se va borrar (arriba) luego agrego  (abajo) 
            for ($i = 0; $i < $cont; $i++) {
                $r = $permisos[$i];
                $rol = $this->getDoctrine()->getManager()->getRepository('LoginBundle:Roles')->findOneByid($r);
                $usuario = $this->getDoctrine()->getManager()->getRepository('LoginBundle:Usuario')->findOneByid($idusuario);
                $modulo = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Modulos')->findOneByid($idmodulo);
                $ur = new UsuarioRoles();
                $ur->setIdusuario($usuario);
                $ur->setIdrol($rol);
                $ur->setIdmodulo($modulo);
                $em->persist($ur);
                $em->flush();
                $em->clear();
            }
            
            
            
            $response = array("code" => 100, "success" => true, 'status' => 'ok');
            $exito = "Cambios agregados correctamente";
            $em->getConnection()->commit();
            $this->get('session')->getFlashBag()->add('notice', $exito);
            return new Response(json_encode($response), 200, Array('Content-Type', 'application/json;  charset=utf-8'));
        } catch (\Exception $e) {
            $ex = get_class($e);
            $em->getConnection()->rollback();
            $error ='Ocurrió un error del tipo: ' . $ex . ' Verifique la información que teclea sea válida y/o Contacte al Administrador del sistema. ' . $e;
            $this->get('session')->getFlashBag()->add(
                    'warning', $error
            );
        }
    }

    public function permisos_por_modulo() {
        #USUARIO ACTUAL Y SUS PERMISOS
        $currentuser = $this->get('security.context')->getToken()->getUser();
        
        $idmodulo = 2;
        $em = $this->getDoctrine()->getManager();
        $usuariopermiso = $em->getRepository('LoginBundle:Usuario')->permisospormodulo($currentuser->getId(), 2);
       
        $permisos = array();
        for ($i = 0; $i < count($usuariopermiso); $i++) {
            $permisos[$i] = $usuariopermiso[$i]['idrol']['nombre'];
        }

        return $permisos;
    }

}
