<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use AdminBundle\Entity\salida;
use Symfony\Component\HttpFoundation\Request;
use Datetime;
class SalidasController extends Controller
{
    public function salidasAction() {
        $permisosmodulo = $this->permisos_por_modulo();

        if (!in_array('LECTURA', $permisosmodulo)) {
            throw new AccessDeniedException('No tienes permiso para ver esta página');
        }

        $em = $this->getDoctrine()->getManager();
        $repu = $em->getRepository('AdminBundle:Unidad');
        $repp = $em->getRepository('AdminBundle:Proveedor');
        $unidad = $repu->UnidadMedida();
        $proveedor = $repp->Proveedores();
        $formaentrada = $em->getRepository('AdminBundle:formaentrada')->findAll();
        $almacen = $em->getRepository('AdminBundle:almacen')->findAll();
        //para llenar ddl
        $productos = $em->getRepository('AdminBundle:Producto')->findAll();
        $salidas = $em->getRepository('AdminBundle:salida')->Salidas();
        


        return $this->render('AdminBundle:salidas:salidas.html.twig', array('salidas' => $salidas, 'almacen' => $almacen, 'productos' => $productos, 'ppm' => $permisosmodulo, 'unidad' => $unidad, 'proveedor' => $proveedor, 'formaentrada' => $formaentrada));
    }
    
    
    public function salidasfechasAction(Request $request) {

        $permisosmodulo = $this->permisos_por_modulo();

        if (!in_array('LECTURA', $permisosmodulo)) {
            throw new AccessDeniedException('No tienes permiso para ver esta página');
        }

        $em = $this->getDoctrine()->getManager();
        $repu = $em->getRepository('AdminBundle:Unidad');
        $repp = $em->getRepository('AdminBundle:Proveedor');
        $unidad = $repu->UnidadMedida();
        $proveedor = $repp->Proveedores();
        $formaentrada = $em->getRepository('AdminBundle:formaentrada')->findAll();
        $almacen = $em->getRepository('AdminBundle:almacen')->findAll();
        //para llenar ddl
        $productos = $em->getRepository('AdminBundle:Producto')->findAll();
        $salidas = $em->getRepository('AdminBundle:salida')->Salidas();
        
        if ($request->getMethod() == 'POST') {


            $post = $request->request->all();


            $mina = $post['min'];
            $maxa = $post['max'];
            

            if (empty($mina) && empty($maxa)) {
                $salidas = $em->getRepository('AdminBundle:salida')->Salidas();
            } elseif(!empty($mina) && empty($maxa)) {
               // print_r($post);
               // print_r('inicio no vacio');

                $min = DateTime::createFromFormat('d/m/Y',$mina);
                
                $max =  date_create(date('Y/m/d'));
                $salidas = $em->getRepository('AdminBundle:salida')->salidasfecha($min, $max);
                 return $this->render('AdminBundle:salidas:salidas.html.twig', array('salidas' => $salidas, 'almacen' => $almacen, 'productos' => $productos, 'ppm' => $permisosmodulo, 'unidad' => $unidad, 'proveedor' => $proveedor, 'formaentrada' => $formaentrada, 'min' => $min, 'max' => $max));
            }
            elseif((empty($mina) && !empty($maxa)))
            {
                //print_r($post);
                //print_r('final no vacio');

                $min = DateTime::createFromFormat('d/m/Y', '01/01/2000');
                $max = DateTime::createFromFormat('d/m/Y', $maxa);
                $salidas = $em->getRepository('AdminBundle:salida')->salidasfecha($min, $max);
                 return $this->render('AdminBundle:salidas:salidas.html.twig', array('salidas' => $salidas, 'almacen' => $almacen, 'productos' => $productos, 'ppm' => $permisosmodulo, 'unidad' => $unidad, 'proveedor' => $proveedor, 'formaentrada' => $formaentrada, 'min' => $min, 'max' => $max));
            
            }else
            {
                //print_r($post);

                //print_r('los 2 seteados');

                $min = DateTime::createFromFormat('d/m/Y', $mina);
                $max = DateTime::createFromFormat('d/m/Y', $maxa);
                $salidas = $em->getRepository('AdminBundle:salida')->salidasfecha($min, $max);
                 
                 return $this->render('AdminBundle:salidas:salidas.html.twig', array('salidas' => $salidas, 'almacen' => $almacen, 'productos' => $productos, 'ppm' => $permisosmodulo, 'unidad' => $unidad, 'proveedor' => $proveedor, 'formaentrada' => $formaentrada,'min' => $min, 'max' => $max));
       
            
            }
        } else {
            //si no es post
           
            $salidas = $em->getRepository('AdminBundle:salida')->findAll();
             return $this->render('AdminBundle:salidas:salidas.html.twig', array('salidas' => $salidas, 'almacen' => $almacen, 'productos' => $productos, 'ppm' => $permisosmodulo, 'unidad' => $unidad, 'proveedor' => $proveedor, 'formaentrada' => $formaentrada));
       
        }


        




        return $this->render('AdminBundle:salidas:salidas.html.twig', array('salidas' => $salidas, 'almacen' => $almacen, 'productos' => $productos, 'ppm' => $permisosmodulo, 'unidad' => $unidad, 'proveedor' => $proveedor, 'formaentrada' => $formaentrada));
       
    }
    
    
    
    public function crearAction(Request $request) {
         $em = $this->getDoctrine()->getManager();
        if ($request->getMethod() == 'POST') {
            try {
                $em->getConnection()->beginTransaction();
                $post = $request->request->all();
                
                $salida = new salida();

                $cb = $post['cb'];
                $folio=$post['folio'];
                $producto = $post['idproducto'];
                $prod = $em->getRepository('AdminBundle:Producto')->findOneByid($producto);
                $detalle=$post['iddetalleentrada'];
                $det = $em->getRepository('AdminBundle:detalleentrada')->findOneByid($detalle);
                
                
                $salida->setCodigo($cb);
                $salida->setFolio($folio);
                $salida->setProducto($prod);
                $salida->setDetalleentrada($det);
                $salida->setFecha(date_create(date('Y/m/d H:i:s')));
                //update detalle
                $det2 = $this->getDoctrine()->getManager()->getRepository('AdminBundle:detalleentrada')->find($detalle);
                $det2->setVendido(0);
                
                $em->persist($salida);
                $em->flush();
                
                $exito = 'Salida Agregada Correctamente';
                $em->getConnection()->commit();
                $this->get('session')->getFlashBag()->add(
                        'notice', $exito
                );
                $url = $this->generateUrl("salidas");
                return $this->redirect($url);
            } catch (\Doctrine\DBAL\Exception\UniqueConstraintViolationException $u) {
                $ex = get_class($u);
                $em->getConnection()->rollback();
                $error = 'Error, ya existe un folio con el valor agregado, favor de verificar la información';
                $this->get('session')->getFlashBag()->add(
                        'warning', $error
                );
                $url = $this->generateUrl("salidas");
            } catch (\Exception $e) {
                $ex = get_class($e);
                $em->getConnection()->rollback();
                $error = 'Ocurrió un error del tipo: ' . $ex . ' Verifique la información que teclea sea válida y/o Contacte al Administrador del sistema. ' . $e;
                $this->get('session')->getFlashBag()->add(
                        'warning', $error
                );
                $url = $this->generateUrl("salidas");
                return $this->redirect($url);
            }
        }
        $url = $this->generateUrl("salidas");
        return $this->redirect($url);
    }
    
     public function permisos_por_modulo() {
        #USUARIO ACTUAL Y SUS PERMISOS
        $currentuser = $this->get('security.context')->getToken()->getUser();

        $idmodulo = 7;
        $em = $this->getDoctrine()->getManager();
        $usuariopermiso = $em->getRepository('LoginBundle:Usuario')->permisospormodulo($currentuser->getId(), $idmodulo);

        $permisos = array();
        for ($i = 0; $i < count($usuariopermiso); $i++) {
            $permisos[$i] = $usuariopermiso[$i]['idrol']['nombre'];
        }

        return $permisos;
    }
}
