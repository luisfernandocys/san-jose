<?php

namespace AdminBundle\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class InicioController extends Controller
{
    public function indexAction()
    {
        if (false === $this->get('security.context')->isGranted(array('ROLE_ADMIN','ROLE_USER'))) {
          throw new AccessDeniedException();
     }
     
     $user = $this->get('security.context')->getToken()->getUser();
          
             $em = $this->getDoctrine()->getManager();
            $usuarios = $em->getRepository('LoginBundle:Usuario')->permisos($user->getId());
           
        return $this->render('AdminBundle:inicio:index.html.twig');
    }
}
