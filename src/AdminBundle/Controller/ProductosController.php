<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AdminBundle\Entity\Producto;
use AdminBundle\Entity\ProductoProveedor;
use AdminBundle\Entity\Proveedor;
use AdminBundle\Entity\tipo;
use AdminBundle\Entity\Tipotejido;
use AdminBundle\Entity\clase;
use AdminBundle\Entity\modelo;
use AdminBundle\Entity\color;
use AdminBundle\Entity\Unidad;
use AdminBundle\Entity\codigo;
use DateTime;

class ProductosController extends Controller {

    public function productosAction() {
        $permisosmodulo = $this->permisos_por_modulo();

        if (!in_array('LECTURA', $permisosmodulo)) {
            throw new AccessDeniedException('No tienes permiso para ver esta página');
        }

        $em = $this->getDoctrine()->getManager();
        $repc = $em->getRepository('AdminBundle:clase');
        $rept = $em->getRepository('AdminBundle:tipo');
        $repu = $em->getRepository('AdminBundle:Unidad');
        $reptt = $em->getRepository('AdminBundle:Tipotejido');
        $repp = $em->getRepository('AdminBundle:Proveedor');
        $repm = $em->getRepository('AdminBundle:modelo');
        $repcl = $em->getRepository('AdminBundle:color');
        $clase = $repc->ClaseProducto();
        $tipo = $rept->TipoProducto();
        $unidad = $repu->UnidadMedida();
        $tipot = $reptt->TipoTejido();
        $proveedor = $repp->Proveedores();
        $modelo = $repm->Modelo();
        $color = $repcl->Color();

        //para llenar tabla
        //$productos = $em->getRepository('AdminBundle:Producto')->findAll();
        $productos = $em->getRepository('AdminBundle:Producto')->productosp();


        return $this->render('AdminBundle:Productos:productos.html.twig', array('productos' => $productos, 'ppm' => $permisosmodulo, 'clase' => $clase, 'tipo' => $tipo, 'unidad' => $unidad, 'tipot' => $tipot, 'proveedor' => $proveedor, 'color' => $color, 'modelo' => $modelo));
    }

    public function productosfechasAction(Request $request) {

        $permisosmodulo = $this->permisos_por_modulo();

        $em = $this->getDoctrine()->getManager();
        $repc = $em->getRepository('AdminBundle:clase');
        $rept = $em->getRepository('AdminBundle:tipo');
        $repu = $em->getRepository('AdminBundle:Unidad');
        $reptt = $em->getRepository('AdminBundle:Tipotejido');
        $repp = $em->getRepository('AdminBundle:Proveedor');
        $repm = $em->getRepository('AdminBundle:modelo');
        $repcl = $em->getRepository('AdminBundle:color');
        $clase = $repc->ClaseProducto();
        $tipo = $rept->TipoProducto();
        $unidad = $repu->UnidadMedida();
        $tipot = $reptt->TipoTejido();
        $proveedor = $repp->Proveedores();
        $modelo = $repm->Modelo();
        $color = $repcl->Color();
        if ($request->getMethod() == 'POST') {


            $post = $request->request->all();


            $mina = $post['min'];
            $maxa = $post['max'];
            

            if (empty($mina) && empty($maxa)) {
                $productos = $em->getRepository('AdminBundle:Producto')->productosp();
            } elseif(!empty($mina) && empty($maxa)) {
               // print_r($post);
               // print_r('inicio no vacio');

                $min = DateTime::createFromFormat('d/m/Y',$mina);
                
                $max =  date_create(date('Y/m/d'));
                $productos = $em->getRepository('AdminBundle:Producto')->productosfecha($min, $max);
                 return $this->render('AdminBundle:Productos:productos.html.twig', array('productos' => $productos, 'ppm' => $permisosmodulo, 'clase' => $clase, 'tipo' => $tipo, 'unidad' => $unidad, 'tipot' => $tipot, 'proveedor' => $proveedor, 'color' => $color, 'modelo' => $modelo, 'min' => $min, 'max' => $max));
            }
            elseif((empty($mina) && !empty($maxa)))
            {
               // print_r($post);
               // print_r('final no vacio');

                $min = DateTime::createFromFormat('d/m/Y', '01/01/2000');
                $max = DateTime::createFromFormat('d/m/Y', $maxa);
                $productos = $em->getRepository('AdminBundle:Producto')->productosfecha($min, $max);
                 return $this->render('AdminBundle:Productos:productos.html.twig', array('productos' => $productos, 'ppm' => $permisosmodulo, 'clase' => $clase, 'tipo' => $tipo, 'unidad' => $unidad, 'tipot' => $tipot, 'proveedor' => $proveedor, 'color' => $color, 'modelo' => $modelo, 'min' => $min, 'max' => $max));
            
            }else
            {
                //print_r($post);

                //print_r('los 2 seteados');

                $min = DateTime::createFromFormat('d/m/Y', $mina);
                $max = DateTime::createFromFormat('d/m/Y', $maxa);
                $productos = $em->getRepository('AdminBundle:Producto')->productosfecha($min, $max);
                 return $this->render('AdminBundle:Productos:productos.html.twig', array('productos' => $productos, 'ppm' => $permisosmodulo, 'clase' => $clase, 'tipo' => $tipo, 'unidad' => $unidad, 'tipot' => $tipot, 'proveedor' => $proveedor, 'color' => $color, 'modelo' => $modelo, 'min' => $min, 'max' => $max));
            
            }
        } else {
            //si no es post
           
            $productos = $em->getRepository('AdminBundle:Producto')->productosp();
             return $this->render('AdminBundle:Productos:productos.html.twig', array('productos' => $productos, 'ppm' => $permisosmodulo, 'clase' => $clase, 'tipo' => $tipo, 'unidad' => $unidad, 'tipot' => $tipot, 'proveedor' => $proveedor, 'color' => $color, 'modelo' => $modelo));
        }

        if (!in_array('LECTURA', $permisosmodulo)) {
            throw new AccessDeniedException('No tienes permiso para ver esta página');
        }

        




        return $this->render('AdminBundle:Productos:productos.html.twig', array('productos' => $productos, 'ppm' => $permisosmodulo, 'clase' => $clase, 'tipo' => $tipo, 'unidad' => $unidad, 'tipot' => $tipot, 'proveedor' => $proveedor, 'color' => $color, 'modelo' => $modelo));
    }

    public function proveedorProductoAction($id) { //método para mostrar proveedores en tabla
        $em = $this->getDoctrine()->getManager();
        $prov = $em->getRepository('AdminBundle:Producto')->detalleProductoProveedor($id);





        return $this->render('AdminBundle:Productos:proveedores.html.twig', array('prov' => $prov));
    }

    public function existenciaAction($id) { //método para mostrar proveedores en tabla
        $em = $this->getDoctrine()->getManager();
        $exi = $em->getRepository('AdminBundle:Producto')->existenciaProducto($id);


        return $this->render('AdminBundle:inventario:existencias.html.twig', array('exi' => $exi));
    }

    public function crearAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        if ($request->getMethod() == 'POST') {
            try {
                $em->getConnection()->beginTransaction();
                $post = $request->request->all();

                $prod = new Producto();
                $folio = "";
                $folio2 = '';


                $productonombre = $post['producto'];
                $otros = $post['otrosnombres'];
                //$clase = $post['claseproducto_ddl'];
                //$tipo = $post['tipoproducto_ddl'];
                $marca = $post['marca'];

                /* $numero = $post['numero'];
                  $unidad = $post['unidadmedida_ddl'];
                  $ancho = $post['ancho'];
                  $longitud = $post['longitud'];
                  $peso = $post['peso']; */

                //$folio2 = $ancho . '_' . $longitud . '_' . $peso;
                $folio2 = ' ';



                $modelo = $post['modelo'];
                $color = $post['color'];
                $tipotejido = $post['tipotejido_ddl'];
                $densidad = $post['densidad'];
                $hilados = $post['hilados'];
                $fibras = $post['fibras'];
                $composicion = $post['composicion'];
                $usos = $post['usos'];
                //$precio = $post['precio'];
                $adicional = $post['adicional'];




                //seteos

                $prod->setFolio($folio);
                $prod->setNombre($productonombre);
                $prod->setOtrosnombres($otros);
                //$cl = $em->getRepository('AdminBundle:clase')->findOneByid($clase);
                //$prod->setClase($cl);
                //$tp = $em->getRepository('AdminBundle:tipo')->findOneByid($tipo);
                //$prod->setTipo($tp);
                $prod->setMarca($marca);
                $mod = $em->getRepository('AdminBundle:modelo')->findOneByid($modelo);
                $prod->setModelo($mod);
                $col = $em->getRepository('AdminBundle:color')->findOneByid($color);
                $prod->setColor($col);
                /* $prod->setNumero($numero);
                  $un = $em->getRepository('AdminBundle:Unidad')->findOneByid($unidad);
                  $prod->setUnidad($un);

                  $prod->setAncho($ancho);
                  $prod->setLongitud($longitud);
                  $prod->setPeso($peso); */




                $tt = $em->getRepository('AdminBundle:Tipotejido')->findOneByid($tipotejido);
                $prod->setTipotejido($tt);
                $prod->setDensidad($densidad);
                $prod->setHilados($hilados);
                $prod->setFibras($fibras);
                $prod->setComposicion($composicion);
                $prod->setPrincipalesusos($usos);
                //$prod->setPrecioadquisicion($precio);
                $prod->setInformacionadicional($adicional);

                $prod->setFecha(date_create(date('Y/m/d')));
                $prod->setUsuario($this->getUser());


                $archivo = $_FILES['input-file-preview'];

                if ($archivo['size'] == 0) {
                    $prod->setFoto('noimagen.gif');
                    $resultadoArchivo = 1;
                } else {
                    $foto = $folio . $archivo['name'];
                    $fotook = preg_replace('/\s+/', '_', $foto);
                    $prod->setFoto($fotook);
                    $resultadoArchivo = $this->moveFile($archivo, 'bundles/admin/img/productos/', $fotook);
                }

                if ($resultadoArchivo) {

                    $em->persist($prod);
                    $em->flush();
                    //Se quitó de aquí la parte de proveedores
                    //
                    //proveedores producto
                    //proveedores inserción
                    /* $proveedor[1] = $post['proveedor_ddl1'];
                      $proveedor[2] = $post['proveedor_ddl2'];
                      $proveedor[3] = $post['proveedor_ddl3'];
                      $proveedor[4] = $post['proveedor_ddl4'];
                      //$proveedor[5] = $post['proveedor_ddl5'];
                      $idinsertadopp = $prod->getId();
                      $preprov = "";
                      for ($i = 1; $i <= 1; $i++) { //cuando sean 4 se cambia aquí a 4
                      if ($proveedor[$i] > 0) {
                      $pp = new ProductoProveedor();
                      $prov = $em->getRepository('AdminBundle:Proveedor')->findOneByid($proveedor[$i]);
                      $prodpp = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Producto')->findOneByid($idinsertadopp);
                      //3 precios por producto
                      $pp->setPrecio1($post['precio' . $i . "1"]);
                      $pp->setProducto($prodpp);
                      $pp->setProveedor($prov);
                      $preprov+=$proveedor[$i];
                      $em->persist($pp);
                      $em->flush();

                      $pp->setPrecio2($post['precio' . $i . "2"]);
                      $pp->setProducto($prodpp);
                      $pp->setProveedor($prov);
                      //$preprov+=$proveedor[$i];
                      $em->persist($pp);
                      $em->flush();

                      $pp->setPrecio3($post['precio' . $i . "3"]);
                      $pp->setProducto($prodpp);
                      $pp->setProveedor($prov);
                      //$preprov+=$proveedor[$i];
                      $em->persist($pp);
                      $em->flush();
                      }
                      }
                     */

                    //folio

                    $idinsertado = $prod->getId();



                    $prodm = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Producto')->findOneByid($idinsertado);

                    $folio = $idinsertado . '-' . $modelo . '-' . $color . /* '-' . $folio2 . */ '-' . date('z') . '-TSJ';
                    $codigo = $idinsertado . '-' . $modelo . '-' . $color . /* '-' . $folio2 . */ '-' . date('z') . '-0-TSJ';
                    $prodm->setFolio($folio);
                    $prodm->setCodigo($codigo);
                    $em->flush();
                    //codigo en 0
                    $codigo = new codigo();
                    $prodinsertado = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Producto')->findOneByid($idinsertado);

                    $codigo->setProducto($prodinsertado);
                    $codigo->setConsecutivo(0);
                    $em->persist($codigo);
                    $em->flush();
                    $exito = 'Producto Agregado Correctamente';
                    $em->getConnection()->commit();
                    $this->get('session')->getFlashBag()->add(
                            'notice', $exito
                    );
                } else {
                    $em->getConnection()->rollback();
                    $error = 'No se pudo agregar el Producto, Error: ' . $e;
                    $this->get('session')->getFlashBag()->add(
                            'warning', $error
                    );
                    return $this->redirect($this->generateUrl('productos')); //nombre en el router
                }




                $url = $this->generateUrl("productos");
                return $this->redirect($url);
            } catch (\Doctrine\DBAL\Exception\UniqueConstraintViolationException $u) {
                $ex = get_class($u);
                $em->getConnection()->rollback();
                $error = 'Error, ya existe un folio con el valor agregado, favor de verificar la información';
                $this->get('session')->getFlashBag()->add(
                        'warning', $error
                );
                $url = $this->generateUrl("productos");
            } catch (\Exception $e) {
                $ex = get_class($e);
                $em->getConnection()->rollback();
                $error = 'Ocurrió un error del tipo: ' . $ex . ' Verifique la información que teclea sea válida y/o Contacte al Administrador del sistema. ' . $e;
                $this->get('session')->getFlashBag()->add(
                        'warning', $error
                );
                $url = $this->generateUrl("productos");
                return $this->redirect($url);
            }
        }
        $url = $this->generateUrl("productos");
        return $this->redirect($url);
    }

    public function modificarAction() {
        $em = $this->getDoctrine()->getManager();

        try {
            $em->getConnection()->beginTransaction();
            $request = $this->getRequest();
            $post = $request->request->all();

            $folio = "";
            $folio2 = '';
            //$proveedor = $post['det_proveedor'];
            $productonombre = $post['det_producto'];
            $otros = $post['det_otrosnombres'];
            //$clase = $post['det_clase'];
            //$tipo = $post['det_tipo'];
            $marca = $post['det_marca'];
            //$numero = $post['det_numero'];
            //$unidad = $post['det_unidad'];
            //$ancho = $post['det_ancho'];
            //$longitud = $post['det_longitud'];
            //$peso = $post['det_peso'];
            //$folio2 = $ancho . '_' . $longitud . '_' . $peso;




            $modelo = $post['det_modelo'];
            $color = $post['det_color'];
            $tipotejido = $post['det_tipotejido'];
            $densidad = $post['det_densidad'];
            $hilados = $post['det_hilados'];
            $fibras = $post['det_fibras'];
            $composicion = $post['det_composicion'];
            $usos = $post['det_usos'];
            //$precio = $post['det_precio'];
            $adicional = $post['det_adicional'];
            $fecha = $post['det_fecha'];



            $id = $post['idproductomodificar'];
            $prod = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Producto')->findOneByid($id);

            //seteos
            //$prov = $em->getRepository('AdminBundle:Proveedor')->findOneByid($proveedor);
            //$prod->setProveedor($prov);
            $prod->setFolio($folio);
            $prod->setNombre($productonombre);
            $prod->setOtrosnombres($otros);
            //$cl = $em->getRepository('AdminBundle:clase')->findOneByid($clase);
            //$prod->setClase($cl);
            //$tp = $em->getRepository('AdminBundle:tipo')->findOneByid($tipo);
            //$prod->setTipo($tp);
            $prod->setMarca($marca);
            $mod = $em->getRepository('AdminBundle:modelo')->findOneByid($modelo);
            $prod->setModelo($mod);
            $col = $em->getRepository('AdminBundle:color')->findOneByid($color);
            $prod->setColor($col);
            //$prod->setNumero($numero);
            // $un = $em->getRepository('AdminBundle:Unidad')->findOneByid($unidad);
            // $prod->setUnidad($un);
            //$prod->setAncho($ancho);
            // $prod->setLongitud($longitud);
            //$prod->setPeso($peso);

            $tt = $em->getRepository('AdminBundle:Tipotejido')->findOneByid($tipotejido);
            $prod->setTipotejido($tt);
            $prod->setDensidad($densidad);
            $prod->setHilados($hilados);
            $prod->setFibras($fibras);
            $prod->setComposicion($composicion);
            $prod->setPrincipalesusos($usos);
            //$prod->setPrecioadquisicion($precio);
            $prod->setInformacionadicional($adicional);
            $prod->setFecha(date_create($fecha));


            $em->flush();

            //proveedores producto
            //proveedores inserción
            //$proveedor[1] = $post['det_proveedor1'];
            //$proveedor[2] = $post['det_proveedor2'];
            //$proveedor[3] = $post['det_proveedor3'];
            //$proveedor[4] = $post['det_proveedor4'];
            //$idinsertadopp = $prod->getId();
            //$preprov = "";
            //BORRAR MIS DETALLES PRODUCTO PROVEEDOR DE CIERTO PRODUCTO
            //$del = $this->getDoctrine()->getManager()->getRepository('AdminBundle:ProductoProveedor')->prodprov($id);
            //   foreach ($del as $del) {
            //     $em->remove($del);
            // }
            // $em->flush();
            /* for ($i = 1; $i <= 1; $i++) { //cuando sean 4 proveedores, se cambia aquí
              if ($proveedor[$i] > 0) {
              $pp = new ProductoProveedor();
              $this->getDoctrine()->getManager()->getRepository('AdminBundle:Producto')->findOneByid($id);
              $prov = $em->getRepository('AdminBundle:Proveedor')->findOneByid($proveedor[$i]);
              $prodpp = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Producto')->findOneByid($idinsertadopp);


              //SECCION DE 3 PRECIOS POR PRODUCTO POR PROVEEDOR
              $pp->setPrecio1($post['det_precio' . $i . "1"]);
              $pp->setProducto($prodpp);
              $pp->setProveedor($prov);
              $preprov+=$proveedor[$i]; //solo 1 vez para el folio
              $em->persist($pp);
              $em->flush();

              $pp->setPrecio2($post['det_precio' . $i . "2"]);
              $pp->setProducto($prodpp);
              $pp->setProveedor($prov);
              //$preprov+=$proveedor[$i];
              $em->persist($pp);
              $em->flush();

              $pp->setPrecio3($post['det_precio' . $i . "3"]);
              $pp->setProducto($prodpp);
              $pp->setProveedor($prov);
              //$preprov+=$proveedor[$i];
              $em->persist($pp);
              $em->flush();
              }
              } */
            //folio
            //folio2 es ancho lngit y peso

            $idinsertado = $prod->getId();
            $prodm = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Producto')->findOneByid($idinsertado);
            $folio = $idinsertado . '-' . $modelo . '-' . $color . '-' . date('z') . '-TSJ';
            $codigo = $idinsertado . '-' . $modelo . '-' . $color . '-' . date('z') . '-0-TSJ';
            $prodm->setFolio($folio);
            $prodm->setCodigo($codigo);
            $em->flush();
            $response = array("code" => 100, "success" => true, 'producto' => $post, 'status' => 'ok');
            //you can return result as JSON
            $exito = "Producto modificado correctamente";

            $em->getConnection()->commit();
            $this->get('session')->getFlashBag()->add(
                    'notice', $exito
            );

            return new Response(json_encode($response), 200, Array('Content-Type', 'application/json;  charset=utf-8'));
        } catch (\Exception $e) {
            $ex = get_class($e);
            $em->getConnection()->rollback();
            $error = 'Ocurrió un error del tipo: ' . $ex . ' Verifique la información que teclea sea válida y/o Contacte al Administrador del sistema. ' . $e;
            $this->get('session')->getFlashBag()->add(
                    'warning', $error
            );
        }
    }

    public function detalleAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $repp = $em->getRepository('AdminBundle:Producto');

        $post = $request->request->all();


        $id = $post['id'];
        $detalle = $repp->detalleProducto($id);

        $pp = $repp->detalleProductoProveedor($id);
        $response = array("code" => 100, "success" => true, 'detalle' => $detalle[0], 'pp' => $pp);
        return new Response(json_encode($response), 200, Array('Content-Type', 'application/json;  charset=utf-8'));
    }

    public function eliminarAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $producto = "";
        try {
            $em->getConnection()->beginTransaction();
            $post = $request->request->all();
            $id = $post['id'];
            $prod = $em->getRepository('AdminBundle:Producto')->findOneByid($id);
            if (!$prod) {
                throw $this->createNotFoundException('No existe el Producto con ID: ' . $id);
            }
            $foto = $prod->getFoto();
            $producto = $prod->getNombre();
            $ruta = 'bundles/admin/img/productos/' . $foto;

            // if (!unlink($ruta)) 
            //    $unlink = ", No se encontró la imagen a eliminar.";
            ////} else {}

            $em->remove($prod);
            $em->flush();
            $exito = 'Producto eliminado Correctamente ';

            $this->get('session')->getFlashBag()->add(
                    'notice', $exito
            );
            $response = array("code" => 100, "success" => true, 'detalle' => $ruta);
            $em->getConnection()->commit();
            return new Response(json_encode($response), 200, Array('Content-Type', 'application/json;  charset=utf-8'));
        } catch (\Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException $e) {
            $ex = get_class($e);
            $em->getConnection()->rollback();
            $this->forward('AdminBundle:Productos:action');
            $error = 'El último intento de eliminación del producto: ' . $producto . ' no se pudo realizar de forma correcta pues tiene registros relacionados dados de alta. ';
            $this->get('session')->getFlashBag()->add(
                    'warning', $error
            );
        } catch (\Exception $e) {
            $ex = get_class($e);
            $em->getConnection()->rollback();
            $error = 'Ocurrió un error del tipo: ' . $ex . ' Verifique la información que teclea sea válida y/o Contacte al Administrador del sistema. ' . $e;
            $this->get('session')->getFlashBag()->add(
                    'warning', $error
            );
            $this->forward('AdminBundle:Productos:action');
        }

        //}
    }

    public function clasetiposAction($id) {
        $em = $this->getDoctrine()->getManager();
        $repp = $em->getRepository('AdminBundle:tipo');


        $detalle = $repp->Tipo_por_clase($id);

        $response = array("code" => 100, "success" => true, 'detalle' => $detalle);

        return new Response(json_encode($response), 200, Array('Content-Type', 'application/json;  charset=utf-8'));
    }

    public function permisos_por_modulo() {
        #USUARIO ACTUAL Y SUS PERMISOS
        $currentuser = $this->get('security.context')->getToken()->getUser();

        $idmodulo = 3;
        $em = $this->getDoctrine()->getManager();
        $usuariopermiso = $em->getRepository('LoginBundle:Usuario')->permisospormodulo($currentuser->getId(), $idmodulo);

        $permisos = array();
        for ($i = 0; $i < count($usuariopermiso); $i++) {
            $permisos[$i] = $usuariopermiso[$i]['idrol']['nombre'];
        }

        return $permisos;
    }

    public function agregar_modeloAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $mod = new modelo();
        try {
            $em->getConnection()->beginTransaction();
            $post = $request->request->all();
            $modelo = $post['modelo'];
            $mod->setNombre($modelo);
            $em->persist($mod);
            $em->flush();
            $em->getConnection()->commit();
            $id = $mod->getId();
            $response = array("code" => 100, "success" => true, 'modelo' => $modelo, 'id' => $id);
            return new Response(json_encode($response), 200, Array('Content-Type', 'application/json;  charset=utf-8'));
        } catch (\Exception $e) {
            $ex = get_class($e);
            $em->getConnection()->rollback();
            $error = 'Ocurrió un error del tipo: ' . $ex . ' Verifique la información que teclea sea válida y/o Contacte al Administrador del sistema. ' . $e;
            $this->get('session')->getFlashBag()->add(
                    'warning', $error
            );
            $url = $this->generateUrl("productos");
            return $this->redirect($url);
        }
    }

    public function agregar_colorAction(Request $request) {

        $em = $this->getDoctrine()->getManager();


        try {
            $em->getConnection()->beginTransaction();
            $post = $request->request->all();
            $color = $post['color'];
            $col = new color();
            $col->setNombre($color);
            $em->persist($col);
            $em->flush();
            $em->getConnection()->commit();
            $id = $col->getId();
            $response = array("code" => 100, "success" => true, 'color' => $color, 'id' => $id);
            return new Response(json_encode($response), 200, Array('Content-Type', 'application/json;  charset=utf-8'));
        } catch (\Exception $e) {
            $ex = get_class($e);
            $em->getConnection()->rollback();
            $error = 'Ocurrió un error del tipo: ' . $ex . ' Verifique la información que teclea sea válida y/o Contacte al Administrador del sistema. ' . $e;
            $this->get('session')->getFlashBag()->add(
                    'warning', $error
            );
            $url = $this->generateUrl("productos");
            return $this->redirect($url);
        }
    }

//SUBIR FOTO
    public function crearDir($ruta) {
        if (!file_exists($ruta)) {
            if (!mkdir($ruta, 0777, true)) {
                die('Fallo al crear ' . $ruta);
            } else {
                chmod($ruta, 0777);
            }
        }
    }

    function validateFile($file, $max_size) {
        $request = $this->getRequest();
        $session = $request->getSession();
        $lang = $session->get('lang');
        $validacion = $this->textos->textosValidacion($lang);
        $ext = array("JPG", "JPEG", "PNG");
        $result = Array();
        if ($file['error'] == 4) {
            $result["error"] = $validacion['no_seleccion'];
        } else if (!($this->validateExtention($file, $ext))) {
            $result["error"] = $validacion['tipo_archivo'];
        } else if ((int) $file['size'] >= (int) $max_size || $file['error'] == 1) {
            $result["error"] = $validacion['max_size'];
        }
        return $result;
    }

    public function validateExtention($archivo, $ext) {
        $tipo = explode(".", $archivo['name']);
        if (in_array(strtoupper(end($tipo)), $ext))
            return TRUE;
        else
            return FALSE;
    }

    public function moveFile($archivo, $path, $nuevo_nombre) {
        $archivosPermitidos = array("jpg", "jpeg", "png");
        $this->verifyDir($path);
        $dest = $path . $nuevo_nombre;

        if ($archivo['error'] != 4)
            $this->existFile($nuevo_nombre, $path, $archivosPermitidos);

        if (move_uploaded_file($archivo['tmp_name'], $dest))
            return true;
        return false;
    }

    public function existFile($file, $path, $ext) {
        $nombre = explode(".", $file);
        $file_path = $path . $nombre[0];

        foreach ($ext as $key => $value) {
            if (file_exists($file_path . "." . $value)) {
                unlink($file_path . "." . $value);
            }
        }
    }

    public function verifyDir($dir) {
        if (!is_dir($dir)) {
            mkdir($dir, 0777); //Crea el directorio para el expositor con permisos de lectura-escritura
            chmod($dir, 0777); //Vuelve a asignar los permisos de lectura-escritura para browser IE
        }
    }

}
