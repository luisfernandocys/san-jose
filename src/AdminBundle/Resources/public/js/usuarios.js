/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



$(document).ready(function() {
    $('#loading').slideUp('2000');//subir el loading

    $('#btn_agregar_usuario').click(agregarusuario);// cachar el clic de registrar usuario 

    generartabla(tujson, ruta);
    var table = $('#tabla_ajax').DataTable({
        "language": {
            "emptyTable": "No hay datos para mostrar",
            "info": "Mostrando _START_ de _END_ de _TOTAL_ entradas totales ",
            "infoEmpty": "Mostrando 0 de 0 de 0 entradas",
            "infoFiltered": "(filtered from _MAX_ total entries)",
            "infoPostFix": "",
            "lengthMenu": "Mostrando _MENU_ resultados",
            "thousands": ",",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "No hay resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior",
            },
            "aria": {
                "sortAscending": ": Activa para ordenar de forma ascendente",
                "sortDescending": ": Activa para ordenar de forma descendente"
            },
        },
        "scrollX": true,
        "columnDefs": [
            {"visible": false, "targets": 2}
        ],
        "order": [[2, 'asc']],
        "displayLength": 25,
        "drawCallback": function(settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;

            api.column(2, {page: 'current'}).data().each(function(group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr class="group titulo_tabla" style="background-color:#009688;color:#fbfbfb;"><td colspan="6" class="text-center">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

    // Order by the grouping
    $('#tabla_usuarios tbody').on('click', 'tr.group', function() {
        var currentOrder = table.order()[0];
        if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
            table.order([2, 'desc']).draw();
        }
        else {
            table.order([2, 'asc']).draw();
        }
    });


    $('.detalle_usuario').click(detalle_usuarios);
    //MODIFICAR
    $("#btn_modificar").click(modificar_usuario);

    $("#mostar_agregar_permiso").click(function() {
        $("#agregar_permiso").toggle("slow");
    });

    $("#usuario_ddl").change(cargar_modulos_usuario);

    $("#modulo_ddl").change(cargar_boton);

    $("#btn_agregar_permiso").click(agregar_permiso);

    $(".boton_disable").click(boton_disable);

    $(".editarpermiso").click(mostrar_tr);
    
    $(".agregar_permiso").click(editarpermiso);
});

function modificar_usuario() {
    $('#loading').slideDown();
    var post = $('#modificar_usuario').serialize();
    console.log(post);
    var path = $("#btn_modificar").attr("data-path");
    console.log(path);
    $.ajax({
        type: 'POST',
        url: path,
        dataType: 'json',
        data: post,
        success: function(response) {

            if (response.status) {

                console.log(response);
                var urlred = window.location.href;
                window.location.href = urlred;



            } else {
                alert('error');
            }
        },
        error: function(data, status, e)
        {
            alert('error2');
            $('#loading').slideUp();
        }
    });

}


function agregarusuario() {


    $("div.modal-body").find('input').val('');//pa limpiar modals


    $("#usuarioform").validate({
        errorClass: "error_validate",
        validClass: "ok_validate",
        rules: {
            nombre: "required",
            apellidop: "required",
            usuario: {
                required: true,
                minlength: 4
            },
            password:
                    {
                        required: true,
                        minlength: 6
                    },
            puesto: "required",
            
        }, //fin rules
        messages:
                {
                    nombre: "Favor de teclear un nombre",
                    apellidop: "Favor de teclear el primer apellido",
                    usuario: {
                        required: "Favor de teclear un nombre de usuario",
                        minlength: "El  nombre d eusuario debe ser de mínimo 4 letras"
                    },
                    password: {
                        required: "Favor de teclear un password",
                        minlength: "El password debe contener al menos 6 letras"
                    },
                    puesto: "Favor de teclear el puesto"
                }, //fin msg
        submitHandler: function(form) {


            //var isGood = confirm('¿Son correctos los datos a ingresar?');
            //if (isGood) {
            $('#loading').slideDown();
            var path = $("#urlinsertarusuario").val();
            console.log(path);
            var identrada = $(this).attr('data-id');

            var post = $('#usuarioform').serialize();
            $.ajax({
                type: 'POST',
                url: path,
                dataType: 'json',
                data: post,
                success: function(response) {
                    if (response.status) {

                        var urlred = window.location.href;
                        window.location.href = urlred;
                    } else {
                        console.log('error');
                        console.log(response);
                    }
                },
                error: function(data, status, e)
                {
                    console.log(post);
                    //var urlred = window.location.href;
                    //      window.location.href = urlred;
                }
            });
            //}
            //else //else no correctos
            //{
            // }

        }, //fin submit handler
        invalidHandler: function(event, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                var message = errors == 1
                        ? 'Please correct the following error:\n'
                        : 'Please correct the following ' + errors + ' errors.\n';
                var errors = "";
                if (validator.errorList.length > 0) {
                    for (x = 0; x < validator.errorList.length; x++) {
                        errors += "\n\u25CF " + validator.errorList[x].message;
                    }
                }
                //  alert(message + errors);
            }
            validator.focusInvalid();

        }



    });//fin validate


}

function generartabla()
{
    //nombre del usuario = datos.usuario[u].nombre
    var datos = tujson;
    var m2 = modulo2;
   
   
    var usuarios = datos.usuario.length; //cuantos usuarios hay en el array
    var tabla = '';
    for (var u = 0; u < usuarios; u++)
    {
        //cuantos módulos por usuario
        var modulos = datos.usuario[u].modulo;
        
        var numeromodulos = Object.keys(modulos).length;
        for (var m = 0; m < numeromodulos; m++)
        {

            var p1 = '';
            var p2 = '';
            var p3 = '';
            var p4 = '';
            nombre = Object.keys(modulos)[m];
             var idm =datos.usuario[u].modulo[nombre].idmodulo;
            var p1 = datos.usuario[u].modulo[nombre].permiso;

            var nruta = ruta + '/detalle/' + datos.usuario[u].id;

            tabla += '<tr class="resultado_tabla" id="tr'+datos.usuario[u].id+idm+'""><td>' + Object.keys(modulos)[m] + '</td><td class="text-center">' + escritura(p1) + '</td><td class="text-center"><div class="detalle_usuario"  data-path="' + nruta + '">' + datos.usuario[u].nombre + '</div></td><td class="text-center">' + impresion(p1) + '</td><td class="text-center">' + lectura(p1) + '</td><td class="text-center">' + edicion(p1) + '</td><td class="text-center">' + escrituraedicion(m2, datos.usuario[u].id,idm) + '</td></tr>';
            tabla += '<tr class="radios" id="edit'+datos.usuario[u].id+idm+'"" style="display:none;"><td>' + Object.keys(modulos)[m] + '</td><td class="text-center">' + escrituracheck(p1) + '</td><td class="text-center"><div class="detalle_usuario"  data-path="' + nruta + '">' + datos.usuario[u].nombre + '</div></td><td class="text-center">' + impresioncheck(p1) + '</td><td class="text-center">' + lecturacheck(p1) + '</td><td class="text-center">' + edicioncheck(p1) + '</td><td class="text-center">' + escrituraedicioncheck(m2, datos.usuario[u].id,idm)+ '</td></tr>';
        }

    }
    //console.log(usuarios +' usuarios');
    //var modulos = datos.usuario[0].modulo;
    ////var modulos = datos.usuario[0].modulo['Módulo 2'].permiso[2];//acceder a módulo y permisos
    //console.log(Object.keys(modulos)[0]);//obtener el nombre del módulo
    //console.log(Object.keys(modulos).length);
    //var permisos = Object.keys(modulos[1]).length;
    $('#tabla_ajax').append(tabla);
}



function escrituraedicion(permisos, id,idm) //aquí generaríamos el enlace editar
{

    
    var si = '<div class="text-center editarpermiso"  data-idusuario="'+id+'"  data-idmodulo="'+idm+'" title="Modificar" style="cursor: pointer; text-decoration:underline;color:#009888;"> Editar</div>';
    var no = '<span class="boton_disable text-danger">No tienes permiso</span>';
    if (permisos == 'si')
    {
        return si;
    }
    else
    {
        return no;
    }
}

//

function escrituraedicioncheck(permisos, id,idm) //aquí generaríamos el enlace modificar
{
    
    var si = '<div class="text-center btn-lg-tabla agregar_permiso" id="agregarpermiso'+id+'" data-idusuario="'+id+'"  data-idmodulo="'+idm+'"  title="Modificar" style="cursor: pointer; "> Modificar</div>';
    var no = '<span class="boton_disable text-danger">No tienes permiso</span>';
    if (permisos == 'si')
    {
        return si;
    }
    else
    {
        return no;
    }
}



function escritura(permisos)
{
    var si = '<img src="../../imagenes/verde.png">';
    var no = '<img src="../../imagenes/rojo.png">';
    if (permisos[0] == 2 || permisos[1] == 2 || permisos[2] == 2 || permisos[3] == 2 || permisos[4] == 2)
    {
        return si;
    }
    else
    {
        return no;
    }
}

function escrituracheck(permisos)
{
    var si = '<div class="checkbox check_escritura"><label><input type="checkbox" class="check" checked value=2> </label></div>';
    var no = '<div class="checkbox check_escritura"><label><input type="checkbox" class="check" value=2 > </label></div>';
    if (permisos[0] == 2 || permisos[1] == 2 || permisos[2] == 2 || permisos[3] == 2 || permisos[4] == 2)
    {
        return si;
    }
    else
    {
        return no;
    }
}

function impresion(permisos)
{
    var si = '<img src="../../imagenes/verde.png">';
    var no = '<img src="../../imagenes/rojo.png">';
    if (permisos[0] == 3 || permisos[1] == 3 || permisos[2] == 3 || permisos[3] == 3 || permisos[4] == 3)
    {
        return si;
    }
    else
    {
        return no;
    }
}

function impresioncheck(permisos)
{
    var si = '<div class="checkbox check_impresion"><label><input type="checkbox" class="check" checked value=3> </label></div>';
    var no = '<div class="checkbox check_impresion"><label><input type="checkbox" class="check" value=3> </label></div>';
    if (permisos[0] == 3 || permisos[1] == 3 || permisos[2] == 3 || permisos[3] == 3 || permisos[4] == 3)
    {
        return si;
    }
    else
    {
        return no;
    }
}



function lectura(permisos)
{
    var si = '<img src="../../imagenes/verde.png">';
    var no = '<img src="../../imagenes/rojo.png">';
    if (permisos[0] == 4 || permisos[1] == 4 || permisos[2] == 4 || permisos[3] == 4 || permisos[4] == 4 || permisos[0] == 1 || permisos[1] == 1 || permisos[2] == 1 || permisos[3] == 1 || permisos[4] == 1)
    {
        return si;
    }
    else
    {
        return no;
    }
}

function lecturacheck(permisos)
{
    var si = '<div class="checkbox check_lectura"><label><input type="checkbox" class="check" checked value=4> </label></div>';
    var no = '<div class="checkbox check_lectura"><label><input type="checkbox" class="check" value=4> </label></div>';
    if (permisos[0] == 4 || permisos[1] == 4 || permisos[2] == 4 || permisos[3] == 4 || permisos[4] == 4 || permisos[0] == 1 || permisos[1] == 1 || permisos[2] == 1 || permisos[3] == 1 || permisos[4] == 1)
    {
        return si;
    }
    else
    {
        return no;
    }
}
function edicion(permisos)
{
    var si = '<img src="../../imagenes/verde.png">';
    var no = '<img src="../../imagenes/rojo.png">';
    if (permisos[0] == 5 || permisos[1] == 5 || permisos[2] == 5 || permisos[3] == 5 || permisos[4] == 5)
    {
        return si;
    }
    else
    {
        return no;
    }
}

function edicioncheck(permisos)
{
    var si = '<div class="checkbox check_edicion"><label><input type="checkbox" class="check" checked value=5 > </label></div>';
    var no = '<div class="checkbox check_edicion"><label><input type="checkbox" class="check" value=5> </label></div>';
    if (permisos[0] == 5 || permisos[1] == 5 || permisos[2] == 5 || permisos[3] == 5 || permisos[4] == 5)
    {
        return si;
    }
    else
    {
        return no;
    }
}

///DETALLES
function detalle_usuarios()
{
    $('#loading').slideDown();
    var path = $(this).attr('data-path');
    var identrada = $(this).attr('data-id');


    $.post(path,
            {
                dataentrada: identrada
            },
    function(response) {
        if (response.code == 100 && response.success) {//dummy check

            console.log(response);
            var datos = response.detalle[0];

            $("#nombremodificar").val(datos.nombre);
            $("#apellidopmodificar").val(datos.apellidop);
            $("#apellidommodificar").val(datos.apellidom);
            $("#puestomodificar").val(datos.puesto);
            $("#idusuariosmodificar").val(datos.id);
            var date = datos.fechahabilitacion.date;
            var fechaformateada = new Date(date);
            var d = fechaformateada.getDate();
            var m = fechaformateada.getMonth();
            m += 1;
            var y = fechaformateada.getFullYear();

            $("#fechamodificar").val(d + "-" + m + "-" + y);
            $('#loading').slideUp('3000');
            $('#detalleusuarios').modal();
            //var urlred = window.location.href;
            //window.location.href = urlred;
        }

    }, "json");
}

//PERMISOS
function cargar_modulos_usuario()
{

    //get the selected value
    $("#loadingddl").show();
    $('#moduloddl').hide('slow');

    var id = this.value;
    var pre_url = $("#usuario_ddl").attr("data-path");
    var url = pre_url + '/' + id;
    console.log(url);
    //make the ajax call
    $.ajax({
        url: url,
        dataType: 'json',
        type: 'POST',
        data: {},
        success: function(response) {

            $('#modulo_ddl option').remove();
            $("#btn_agregar_permiso").hide('fast');
            var modulos = response.modulos;
            console.log($("#usuario_ddl").val());
            if ($("#usuario_ddl").val() == 0)
            {
                $("#btn_agregar_permiso").hide('fast');
                $('#moduloddl').hide('slow');

            }
            else
            {
                if (jQuery.isEmptyObject(modulos))
                {
                    $('#modulo_ddl').append($('<option>', {
                        value: 0,
                        text: 'No hay módulos por asignar'
                    }));
                }
                else
                {
                    $('#modulo_ddl').append($('<option>', {
                        value: 0,
                        text: 'Elige un módulo'
                    }));

                    $.each(modulos, function(i, item) {
                        $('#modulo_ddl').append($('<option>', {
                            value: item.id,
                            text: item.nombre
                        }));
                    });
                }
                $('#moduloddl').show('slow');
            }

            $("#loadingddl").hide();
        }
    });

}

function cargar_boton()
{
    var idmodulo = this.value;
    console.log(idmodulo);
    if (idmodulo != 0)
    {
        $("#btn_agregar_permiso").show('slow');
    }
    else
    {
        $("#btn_agregar_permiso").hide('slow');
    }
}

function agregar_permiso()
{
    $('#loading').slideDown();
    var usuario = $("#usuario_ddl").val();
    var modulo = $("#modulo_ddl").val();
    var url = $("#btn_agregar_permiso").attr("data-path");

    $.ajax({
        type: 'POST',
        url: url,
        dataType: 'json',
        data: {
            usuario: usuario,
            modulo: modulo
        },
        success: function(response) {
            if (response.status) {

                var urlred = window.location.href;
                window.location.href = urlred;
            } else {

                $("loading").slideUp('slow');
                alert('error' + response);
            }
        },
        error: function(data, status, e)
        {
            $("loading").slideUp('slow');
            alert('error' + response);
            //var urlred = window.location.href;
            //      window.location.href = urlred;
        }
    });
}

function boton_disable()
{
    alert('No tienes los permisos suficientes para realizar esta acción');
}

function mostrar_tr()
{
    var idu= $(this).attr('data-idusuario');
    var idm= $(this).attr('data-idmodulo');
    var idedit = 'edit'+idu+idm;
    var idtr= 'tr'+idu+idm;
    $("#"+idtr).hide('fast');
    $("#"+idedit).show('slow');
    
}



function editarpermiso()
{
    
    
   
    var row = $(this).closest("tr"); //tr donde está el div de boton modificar
    var idusaurio = $(this).attr('data-idusuario');
    var idmodulo=$(this).attr('data-idmodulo');
   var idrow= row.attr('id');
   var opciones= $('#'+idrow).find('input[type="checkbox"]:checked');
   var cont = opciones.length;
   var permisos = [];
   var preurl      = window.location.href;
   var url=preurl+'/permisos/editar';
   
   for($i=0;$i<cont;$i++)
   {
       
       permisos[$i]=opciones[$i].value;
   }
   
   if(permisos.length===0)
   {
       alert('Debes elegir al menos un permiso por usuario');
   $("#"+idrow).find('td').parent().find('input:checkbox:first').focus();
   
  
       
   }
   else
   {
       // var permisosjs = JSON.stringify(permisos);
  $('#loading').slideDown();
   $.ajax({
        type: 'POST',
        url: url,
        dataType: 'json',
        data: {
            permisos:permisos,
            idmodulo:idmodulo,
            idusuario:idusaurio,
            url:url
        },
        success: function(response) {
            if (response.status) {
                
                
                var urlred = window.location.href;
                window.location.href = urlred;
            } else {

                $("loading").slideUp('slow');
                alert('error' + response);
            }
        },
        error: function(data, status, e)
        {
            $("loading").slideUp('slow');
            
            var urlred = window.location.href;
            window.location.href = urlred;
        }
    });
   }
   
}
