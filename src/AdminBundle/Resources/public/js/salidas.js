/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    $('#btn_agregar_salida').prop('disabled', true);

    //  $('#producto').change(cargar_proveedor); //cargar proveedor al elegir un producto
    $("#cb").bind('change paste keyup', buscar_codigo);

    //tabla de entradas
    var table = $('#tabla_salidas').DataTable({
        "language": {
            "emptyTable": "No hay datos para mostrar",
            "info": "Mostrando _START_ de _END_ de _TOTAL_ entradas totales ",
            "infoEmpty": "Mostrando 0 de 0 de 0 entradas",
            "infoFiltered": "(filtered from _MAX_ total entries)",
            "infoPostFix": "",
            "lengthMenu": "Mostrando _MENU_ resultados",
            "thousands": ",",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "No hay resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior",
            },
            "aria": {
                "sortAscending": ": Activa para ordenar de forma ascendente",
                "sortDescending": ": Activa para ordenar de forma descendente"
            },
        },
        "scrollX": true

    });
});

function agregarsalida() {


    $("div.modal-body").find('input').val(''); //pa limpiar modals

    // add the rule here
    $.validator.addMethod("valueNotEquals", function (value, element, arg) {
        return arg != value;
    }, "Value must not equal arg.");
    $("#salidasform").validate({
        errorClass: "error_validate",
        validClass: "ok_validate",
        rules: {
            
            cb: "required"

        }, //fin rules
        messages:
                {
                    cb:"No hay código de barras"
                }, //fin msg
        submitHandler: function (form) {



            $('#loading').slideDown();

            form.submit();
        }, //fin submit handler
        invalidHandler: function (event, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                var message = errors == 1
                        ? 'Please correct the following error:\n'
                        : 'Please correct the following ' + errors + ' errors.\n';
                var errors = "";
                if (validator.errorList.length > 0) {
                    for (x = 0; x < validator.errorList.length; x++) {
                        errors += "\n\u25CF " + validator.errorList[x].message;
                    }
                }
                //  alert(message + errors);
            }
            validator.focusInvalid();
        }



    }); //fin validate


}



function buscar_codigo()
{
    var val = $("#cb").val();
    var largo = val.length;
    console.log(largo);
    if (largo >= 15)
    {
        //buscar
        
        var path = $("#path_entrada").attr('data-path');
        // var identrada = $(this).attr('data-id');
        //limpian los inputs e img

        $("div.modal-body").find('input').val(''); //pa limpiar modals


        $.post(path,
                {
                    codigo: val
                },
                function (response) {
                    if (response.code == 100 && response.success) {//dummy check


                        var datos = response.detalle;
                        if (Object.keys(datos).length > 0) {
                            $('#loading').slideDown();
                            console.log(datos[0]);
                            $("#lbproducto").addClass("text-success").text(datos[0].entrada.producto.nombre);
                            var valortemp = datos[0].valor;
                            var unidadtemp = datos[0].entrada.producto.unidad.nombre;
                            var unidad = "";
                            if (unidadtemp == "Metros")
                            {
                                unidad = "Metros/2";
                            } else
                                unidad = unidadtemp;
                            var valor = valortemp + " " + unidad;
                            $("#lbvalor").addClass("text-success").text(valor);

                            //FECHA
                            var date = datos[0].entrada.fecharegistro.date;
                            var fechaformateada = new Date(date);
                            var d = fechaformateada.getDate();
                            var m = fechaformateada.getMonth();
                            m += 1;
                            var y = fechaformateada.getFullYear();
                           
                            //FIN FECHA

                            $("#lbfecha").addClass("text-success").text(d + "-" + m + "-" + y);
                            $('#btn_agregar_salida').prop('disabled', false);
                            var status = datos[0].vendido;
                            if(status == 1)
                            {
                                $("#lbstatus").removeClass("text-danger").addClass("text-success").text("Disponible");
                            }
                            else
                            {
                                $("#lbstatus").removeClass("text-success").addClass("text-danger").text("Vendido");
                                $('#btn_agregar_salida').prop('disabled', true);
                            }
                            $("#iddetalleentrada").attr('data-id',datos[0].id).val(datos[0].id);
                            $("#folio").attr('data-id',datos[0].folio).val(datos[0].folio);
                            $("#idproducto").attr('data-id',datos[0].entrada.producto.id).val(datos[0].entrada.producto.id);
                            $('#loading').slideUp('3000');
                        } else
                        {
                            $("#lbproducto").text("No hay datos");
                            $("#lbvalor").text("No hay datos");
                            $("#lbfecha").text("No hay datos");
                            $("#lbstatus").text("No hay datos").removeClass();
                            $('#btn_agregar_salida').prop('disabled', true);
                            $("#iddetalleentrada").attr('data-id',0);
                            $("#idproducto").attr('data-id',0);
                            $('#loading').slideUp('3000');
                        }



                    }

                }, "json");
    }
}




function boton_disable()
{
    alert('No tienes los permisos suficientes para realizar esta acción');
    exit(0);
}