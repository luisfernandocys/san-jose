/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



$(document).ready(function () {
//deshabilitamos cajas de peso, ancho y longitud hasta que se activen cuando se elige unidad de medida
    //$('#peso').prop('disabled', true);
    //$('#ancho').prop('disabled', true);
    //$('#longitud').prop('disabled', true);
    //$('#unidadmedida_ddl').change(unidad_medida);
    //$('#det_unidad').change(unidad_medida_modal);
    //////////////////
    //las líneas de arriba (5) son para las medidas,cajas de texto etc etc 
    //////////////////
    $('#loading').slideUp('2000'); //subir el loading
    $(".boton_disable").click(boton_disable);
    $('#btn_agregar_producto').click(agregarproducto);
    $("#divnuevoproducto").click(function () {
        $("#nuevoproductodiv").toggle('slow');
    });
    $('#claseproducto_ddl').change(cargar_tipos); //tipos por clase de producto
   // $("#det_clase").change(cargar_tipos_det_modal);
    $("#modelo").change(modelo);
    $("#color").change(color);
    $("#btn_modificar").click(modificar_producto);
    $(".print").click(imprimir);
    $(".detalle_producto").click(detalle_producto);
    $(".eliminarproducto").click(eliminar_producto);
    $("#btn_agregar_modelo").click(agregar_modelo);
    $("#btn_agregar_color").click(agregar_color);
    $("#printcodigo").click(imprimir);
    //PROVEEDORES SHOW
    /*$("#proveedor_ddl1").change(function () {
        var provop = $("#proveedor_ddl").val();
        if (provop == 0)
        {
            $("#addp1").hide();
        } else
        {
            $("#addp1").show();
        }

    });*/

    $("#addp1").click(function () {
        $("#addp1").hide();
        $("#proveedor2").fadeIn("slow");

    });
    $("#addp2").click(function () {
        $("#addp2").hide();
        $("#proveedor3").fadeIn("slow");

    });
    $("#addp3").click(function () {
        $("#addp3").hide();
        $("#proveedor4").fadeIn("slow");

    });
    /*
     [
     {
     extend: 'print',
     exportOptions: {
     columns: ':visible'
     }
     },
     'colvis',
     text: 'Selecciona columnas',
     ],
     columnDefs: [ {
     targets: -1,
     visible: false
     } ]
     */
    var table = $('#tabla_productos').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'colvis',
                text: 'Selecciona columnas',
                columns: ':not(:first-child)'
            },
            {
                extend: 'print',
                text:'Imprimir',
                exportOptions: {
                    columns: ':visible'
                }
            },
             {
                extend: 'pdf',
                exportOptions: {
                    columns: ':visible'
                }
            },
             {
                extend: 'excel',
                exportOptions: {
                    columns: ':visible'
                }
            }
        ],
        /*'copy', 'csv', 'excel', 'pdf', 'print',*/
        "language": {
            "emptyTable": "No hay datos para mostrar",
            "info": "Mostrando _START_ de _END_ de _TOTAL_ entradas totales ",
            "infoEmpty": "Mostrando 0 de 0 de 0 entradas",
            "infoFiltered": "(filtered from _MAX_ total entries)",
            "infoPostFix": "",
            "lengthMenu": "Mostrando _MENU_ resultados",
            "thousands": ",",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "No hay resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior",
            },
            "aria": {
                "sortAscending": ": Activa para ordenar de forma ascendente",
                "sortDescending": ": Activa para ordenar de forma descendente"
            },
        }, "scrollX": true

    });

});
function unidad_medida()
{

    var val = $("#unidadmedida_ddl").val();
    if (val == 1)
    {

        $('#ancho').prop('disabled', false).focus();
        $('#longitud').prop('disabled', false);
        $('#peso').prop('disabled', true);
    }

    if (val == 2)
    {
        $('#peso').prop('disabled', false).focus();
        $('#ancho').prop('disabled', true);
        $('#longitud').prop('disabled', true);
    }
    if (val == 3 || val == 4 || val == 5) {
        $('#peso').prop('disabled', true);
        $('#ancho').prop('disabled', true);
        $('#longitud').prop('disabled', true);
        $('#numero').focus();
    }

}

function unidad_medida_modal(id)
{
    if (id > 0)
    {
        if (id == 1)
        {

            $('#det_ancho').prop('disabled', false);
            $('#det_longitud').prop('disabled', false);
            $('#det_peso').prop('disabled', true);
        }

        if (id == 2)
        {
            $('#det_peso').prop('disabled', false);
            $('#det_ancho').prop('disabled', true);
            $('#det_longitud').prop('disabled', true);
        }
        if (id == 3 || id == 4 || id == 5) {
            $('#det_peso').prop('disabled', true);
            $('#det_ancho').prop('disabled', true);
            $('#det_longitud').prop('disabled', true);
            $('#det_numero').focus();
        }
    } else
    {


        var val = $("#det_unidad").val();
        if (val == 1)
        {

            $('#det_ancho').prop('disabled', false).focus();
            $('#det_longitud').prop('disabled', false);
            $('#det_peso').prop('disabled', true);
        }

        if (val == 2)
        {
            $('#det_peso').prop('disabled', false).focus();
            $('#det_ancho').prop('disabled', true);
            $('#det_longitud').prop('disabled', true);
        }
        if (val == 3 || val == 4 || val == 5) {
            $('#det_peso').prop('disabled', true);
            $('#det_ancho').prop('disabled', true);
            $('#det_longitud').prop('disabled', true);
            $('#det_color').focus();
        }

    }





}

function boton_disable()
{
    alert('No tienes los permisos suficientes para realizar esta acción');
    exit(0);
}


function detalle_producto()
{

    $('#loading').slideDown();
    var path = $(this).attr('data-path');
    var idproducto = $(this).attr('data-id');
    console.log(idproducto);
    
    //limpian los inputs e img
    $("#foto_producto").attr('src', '');
    $("div.modal-body").find('input').val(''); //pa limpiar modals
    $("div.modal-body").find('select').val(''); //pa limpiar modals

    $.post(path,
            {
                id: idproducto
            },
            function (response) {
                if (response.code == 100 && response.success) {//dummy check
                    console.log(response.pp);

                    var datos = response.detalle;
                    var dp = response.pp;
                    var date = datos.fecha.date;
                    var fechaformateada = new Date(date);
                    var d = fechaformateada.getDate();
                    var m = fechaformateada.getMonth();
                    m += 1;
                    var y = fechaformateada.getFullYear();
                    $("#det_fecha").val(d + "-" + m + "-" + y);
                    var imagen = '../bundles/admin/img/productos/' + datos.foto;
                    $("#foto_producto").attr('src', imagen);
                    $("#det_folio").val(datos.folio);
                    $("#det_producto").val(datos.nombre);
                    //proveedores
                    $("#det_proveedor1").val(0);//estas 4 lineas son para evitar que se queden en blanco
                    $("#det_proveedor2").val(0);
                    $("#det_proveedor3").val(0);
                    $("#det_proveedor4").val(0);

                    for (var i = 0; i < Object.keys(dp).length; i++) {
                        var prov = "#det_proveedor" + (i + 1);
                        $(prov).val(dp[i].proveedor.id);

                        var prec1 = "#det_precio" + (i + 1) + "1";
                        var prec2 = "#det_precio" + (i + 1) + "2";
                        var prec3 = "#det_precio" + (i + 1) + "3";
                        $(prec1).val(dp[i].precio1);
                        $(prec2).val(dp[i].precio2);
                        $(prec3).val(dp[i].precio3);




                    }

                    $("#det_otrosnombres").val(datos.otrosnombres);
                    //$("#det_clase").val(datos.clase.id);
                   // cargar_tipos_det(datos.clase.id, datos.tipo.id);
                   // $("#det_tipo").val(datos.tipo.id);
                    $("#det_marca").val(datos.marca);
                    $("#det_modelo").val(datos.modelo.id);
                    $("#det_numero").val(datos.numero);
                    $("#det_unidad").val(datos.unidad.id);
                    //unidad_medida_modal(datos.unidad.id);
                    $("#det_peso").val(datos.peso);
                    $("#det_ancho").val(datos.ancho);
                    $("#det_longitud").val(datos.longitud);
                    $("#det_color").val(datos.color.id);
                    $("#det_tipotejido").val(datos.tipotejido.id);
                    $("#det_densidad").val(datos.densidad);
                    $("#det_hilados").val(datos.hilados);
                    $("#det_fibras").val(datos.fibras);
                    $("#det_composicion").val(datos.composicion);
                    $("#det_usos").val(datos.principalesusos);
                    //$("#det_precio").val(datos.precioadquisicion);
                    $("#det_adicional").val(datos.informacionadicional);
                    $("#eliminarproducto").attr('data-id', datos.id);
                    $("#idproductomodificar").val(datos.id);
                    generar_codigo(datos.codigo);
                    $("#btn_modificar").attr('value', 'Modificar');
                    /*
                     
                     
                     var date = datos.fechahabilitacion.date;
                     var fechaformateada = new Date(date);
                     var d = fechaformateada.getDate();
                     var m = fechaformateada.getMonth();
                     m += 1;
                     var y = fechaformateada.getFullYear();
                     
                     $("#fechamodificar").val(d + "-" + m + "-" + y);
                     
                     */
                    //var urlred = window.location.href;
                    //window.location.href = urlred;
                    $('#loading').slideUp('3000');
                    // $('#detalleusuarios').modal();
                }

            }, "json");
}



function agregarproducto() {


    $("div.modal-body").find('input').val(''); //pa limpiar modals

    // add the rule here
    $.validator.addMethod("valueNotEquals", function (value, element, arg) {
        return arg != value;
    }, "Value must not equal arg.");
    $("#productoform").validate({
        errorClass: "error_validate",
        validClass: "ok_validate",
        rules: {
            proveedor_ddl1: {valueNotEquals: "0"},
            producto: "required",
           
            //claseproducto_ddl: {valueNotEquals: "0"},
            //tipoproducto_ddl: {valueNotEquals: "0"},
            //marca: "required",
            modelo: {valueNotEquals: "0"},
            unidadmedida_ddl: {valueNotEquals: "0"},
            tipotejido_ddl: {valueNotEquals: "0"},
            fototexto: "required",
            color: {valueNotEquals: "0"},
            precio11: "required"
        }, //fin rules
        messages:
                {
                    proveedor_ddl1: "Elija un proveedor",
                    producto: "Capture el nombre del producto",
                    
                    //claseproducto_ddl: "Elija la clase del producto",
                    //tipoproducto_ddl: "Elija el tipo de producto",
                    //marca: "Capture la marca del producto",
                    modelo: "Elija el modelo del producto",
                    unidadmedida_ddl: "Elija la unidad de medida",
                    tipotejido_ddl: "Elija el tipo de tejido",
                    color: "Elija un color",
                    fototexto: "Selecciona la foto del producto",
                    precio11: "Capture el precio del producto"
                }, //fin msg
        submitHandler: function (form) {


            //var isGood = confirm('¿Son correctos los datos a ingresar?');
            //if (isGood) {
            $('#loading').slideDown();
            //var path = $("#urlinsertarproducto").val();
            //console.log(path);
            form.submit();
        }, //fin submit handler
        invalidHandler: function (event, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                var message = errors == 1
                        ? 'Please correct the following error:\n'
                        : 'Please correct the following ' + errors + ' errors.\n';
                var errors = "";
                if (validator.errorList.length > 0) {
                    for (x = 0; x < validator.errorList.length; x++) {
                        errors += "\n\u25CF " + validator.errorList[x].message;
                    }
                }
                //  alert(message + errors);
            }
            validator.focusInvalid();
        }



    }); //fin validate


}

function generar_codigo(codigo)
{

    $("#barcode").JsBarcode(codigo, {
        width: 2,
        height: 100,
        quite: 10,
        format: "CODE128",
        backgroundColor: "#fff",
        lineColor: "#000"

    });

    var img = $("#barcode");
    var src = img.attr('src');
    $("#btndescargar").attr('href', src).attr('download', codigo);
}


function imprimir()
{

    var id = $("#barcode");
    $.print(id);
}
function modificar_producto() {



// add the rule here
    $.validator.addMethod("valueNotEquals", function (value, element, arg) {
        return arg != value;
    }, "Value must not equal arg.");
    $("#modificar_producto").validate({
        errorClass: "error_validate",
        validClass: "ok_validate",
        rules: {
            det_proveedor1: {valueNotEquals: "0"},
            det_producto: "required",
            //det_clase: {valueNotEquals: "0"},
            //det_tipo: {valueNotEquals: "0"},
            //det_marca: "required",
            det_modelo: {valueNotEquals: "0"},
            det_unidad: {valueNotEquals: "0"},
            det_tipotejido: {valueNotEquals: "0"},
            det_color: {valueNotEquals: "0"}
            //det_precio: "required"
        }, //fin rules
        messages:
                {
                    det_proveedor: "Elija un proveedor",
                    det_producto: "Capture el nombre del producto",
                    //det_clase: "Elija la clase del producto",
                    //det_tipo: "Elija el tipo de producto",
                    //det_marca: "Capture la marca del producto",
                    det_modelo: "Elija el modelo del producto",
                    det_unidad: "Elija la unidad de medida",
                    det_tipotejido: "Elija el tipo de tejido",
                    det_color: "Elija un color"
                            // det_precio: "Capture el precio del producto"
                }, //fin msg
        submitHandler: function (form) {


            $('#loading').slideDown();
            $("#det_fecha").prop('disabled', false);
            var post = $('#modificar_producto').serialize();

            var path = $("#btn_modificar").attr("data-path");

            $.ajax({
                type: 'POST',
                url: path,
                dataType: 'json',
                data: post,
                success: function (response) {

                    if (response.status) {


                        var urlred = window.location.href;
                        window.location.href = urlred;



                    } else {
                        alert('error');
                    }
                },
                error: function (xhr, textStatus, error)
                {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                    alert('Algo malo ocurrió, favor de contactar al administrador del sistema.');
                    $('#loading').slideUp();
                }
            });
        }, //fin submit handler
        invalidHandler: function (event, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                var message = errors == 1
                        ? 'Please correct the following error:\n'
                        : 'Please correct the following ' + errors + ' errors.\n';
                var errors = "";
                if (validator.errorList.length > 0) {
                    for (x = 0; x < validator.errorList.length; x++) {
                        errors += "\n\u25CF " + validator.errorList[x].message;
                    }
                }
                //  alert(message + errors);
            }
            validator.focusInvalid();
        }



    }); //fin validate



}

function eliminar_producto()
{
    /*
     * $.ajax({
     type: 'POST',
     url: path,
     dataType: 'json',
     data: post,
     success: function (response) {
     
     if (response.status) {
     
     
     var urlred = window.location.href;
     window.location.href = urlred;
     
     
     
     } else {
     alert('error');
     }
     },
     error: function (xhr, textStatus, error)
     {
     console.log(xhr.statusText);
     console.log(textStatus);
     console.log(error);
     alert('Algo malo ocurrió, favor de contactar al administrador del sistema.');
     $('#loading').slideUp();
     }
     });
     * 
     */
    $('#loading').slideDown();
    var path = $(this).attr('data-path');
    var idproducto = $(this).attr('data-id');

    //limpian los inputs e img
    var isGood = confirm('¿Seguro que quieres eliminar este producto?');
    if (isGood) {
        $.post(path,
                {
                    id: idproducto
                },
                function (response) {
                   
                    if (response.code == 100 && response.success) {//dummy check


                        var urlred = window.location.href;
                        window.location.href = urlred;
                        //$('#loading').slideUp('3000');
                        // $('#detalleusuarios').modal();
                    }

                }, "json");

    } else
    {
        exit(0);
    }
}

function cargar_tipos()
{
    $('#div_tipoproducto_ddl').hide('fast');
    $('#loadingddl').slideDown();
    var path = $(this).attr('data-path');
    var id = this.value;
    var url = path + '/' + id;

    $.ajax({
        url: url,
        dataType: 'json',
        type: 'POST',
        data: {},
        success: function (response) {

            $('#tipoproducto_ddl option').remove();
            var tipos = response.detalle;

            if ($("clase_ddl").val() == 0)
            {

                $('#tipoproducto_ddl').hide('slow');
            } else
            {
                if (jQuery.isEmptyObject(tipos))
                {
                    $('#tipoproducto_ddl').append($('<option>', {
                        value: 0,
                        text: 'No hay tipos por asignar'
                    }));
                } else
                {
                    $('#tipoproducto_ddl').append($('<option>', {
                        value: 0,
                        text: 'Elige un tipo de producto'
                    }));
                    $.each(tipos, function (i, item) {
                        $('#tipoproducto_ddl').append($('<option>', {
                            value: item.id,
                            text: item.tipo
                        }));
                    });
                }
                $('#div_tipoproducto_ddl').show('slow');
            }

            $("#loadingddl").hide();
        }
    });
}


function cargar_tipos_det(id, idtipo)
{

    $('#div_tipoproducto_det').hide('fast');
    $('#loadingddl_det').slideDown();
    var path = $("#det_clase").attr('data-path');
    var url = path + '/' + id;

    $.ajax({
        url: url,
        dataType: 'json',
        type: 'POST',
        data: {},
        success: function (response) {

            $('#det_tipo option').remove();
            var tipos = response.detalle;
            if ($("det_clase").val() == 0)
            {

                $('#det_tipo').hide('slow');
            } else
            {
                if (jQuery.isEmptyObject(tipos))
                {
                    $('#div_tipoproducto_det').append($('<option>', {
                        value: 0,
                        text: 'No hay tipos por asignar'
                    }));
                } else
                {
                    $('#det_tipo').append($('<option>', {
                        value: 0,
                        text: 'Elige un tipo de producto'
                    }));
                    $.each(tipos, function (i, item) {
                        $('#det_tipo').append($('<option>', {
                            value: item.id,
                            text: item.tipo
                        }));
                    });
                }
                $('#div_tipoproducto_det').show('slow');
                $('#det_tipo').val(idtipo);
            }

            $("#loadingddl_det").hide();
        },
        error: function (xhr, textStatus, error)
        {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
            alert('Algo malo ocurrió, favor de contactar al administrador del sistema.');
        }
    });
}

function cargar_tipos_det_modal()
{

    $('#div_tipoproducto_det').hide('fast');
    $('#loadingddl_det').slideDown();
    var path = $(this).attr('data-path');
    var id = this.value;
    var url = path + '/' + id;

    $.ajax({
        url: url,
        dataType: 'json',
        type: 'POST',
        data: {},
        success: function (response) {

            $('#det_tipo option').remove();
            var tipos = response.detalle;
            if ($("det_clase").val() == 0)
            {

                $('#det_tipo').hide('slow');
            } else
            {
                if (jQuery.isEmptyObject(tipos))
                {
                    $('#div_tipoproducto_det').append($('<option>', {
                        value: 0,
                        text: 'No hay tipos por asignar'
                    }));
                } else
                {
                    $('#det_tipo').append($('<option>', {
                        value: 0,
                        text: 'Elige un tipo de producto'
                    }));
                    $.each(tipos, function (i, item) {
                        $('#det_tipo').append($('<option>', {
                            value: item.id,
                            text: item.tipo
                        }));
                    });
                }
                $('#div_tipoproducto_det').show('slow');
            }

            $("#loadingddl_det").hide();
        },
        error: function (xhr, textStatus, error)
        {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
            alert('Algo malo ocurrió, favor de contactar al administrador del sistema.');
        }
    });
}



function modelo()
{
    var op = this.value;
    if (op == 9999)
    {
        $("#divnuevomodelo").fadeIn('slow');
        $("#modelonuevo").val('');
        $("#modelonuevo").focus();
    } else
    {
        $("#divnuevomodelo").fadeOut('slow');
    }
}

function agregar_modelo(event)
{
    var path = $(this).attr('data-path');
    var modelo = $("#modelonuevo").val();
    $("#loadingmodelo").show();
    event.preventDefault();
    if (modelo == '')
    {
        alert('Por favor escribe el nombre del nuevo modelo.');
    } else
    {
        $.ajax({
            url: path,
            dataType: 'json',
            type: 'POST',
            data: {
                modelo: modelo
            },
            success: function (response) {



                var modelo = response;

                $("#loadingmodelo").fadeOut('slow');
                $('#modelo option:first').after($('<option>', {
                    value: modelo.id,
                    text: modelo.modelo
                }));
                $("#divnuevomodelo").fadeOut('slow');
                $('#modelo').val(modelo.id); //debe ser el último id

            },
            error: function (xhr, textStatus, error)
            {
                console.log(xhr.statusText);
                console.log(textStatus);
                console.log(error);
                alert('Algo malo ocurrió, favor de contactar al administrador del sistema.');
            }

        });
    }

}

function color()
{
    var op = this.value;
    if (op == 9999)
    {
        $("#divnuevocolor").fadeIn('slow');
        $("#colornuevo").val('');
        $("#colornuevo").focus();
    } else
    {
        $("#divnuevocolor").fadeOut('slow');
    }
}

function agregar_color(event)
{
    var path = $(this).attr('data-path');
    var color = $("#colornuevo").val();

    $("#loadingcolor").show();
    event.preventDefault();
    if (color == '')
    {
        alert('Por favor escribe el nombre del nuevo color.');
    } else
    {
        $.ajax({
            url: path,
            dataType: 'json',
            type: 'POST',
            data: {
                color: color
            },
            success: function (response) {



                var color = response;

                $("#loadingcolor").fadeOut('slow');
                $('#color option:first').after($('<option>', {
                    value: color.id,
                    text: color.color
                }));
                $("#divnuevocolor").fadeOut('slow');
                $('#color').val(color.id); //debe ser el último id

            },
            error: function (xhr, textStatus, error) {
                console.log(xhr.statusText);
                console.log(textStatus);
                console.log(error);
                alert('Algo malo ocurrió, favor de contactar al administrador del sistema.');
            }

        });
    }

}