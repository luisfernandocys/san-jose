/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function() {
    $('#loading').slideUp('2000');//subir el loading
    $(".boton_disable").click(boton_disable);
    $('#btn_agregar_proveedor').click(agregarproveedor);
    $("#divnuevoproveedor").click(function() {
        $("#nuevoproveedordiv").toggle('slow');
    });
    $("#btn_modificar").click(modificar_proveedor);

    $(".detalle_proveedor").click(detalle_proveedor);
    $(".eliminarproducto").click(eliminar_proveedor);


    var table = $('#tabla_proveedores').DataTable({
        "language": {
            "emptyTable": "No hay datos para mostrar",
            "info": "Mostrando _START_ de _END_ de _TOTAL_ entradas totales ",
            "infoEmpty": "Mostrando 0 de 0 de 0 entradas",
            "infoFiltered": "(filtered from _MAX_ total entries)",
            "infoPostFix": "",
            "lengthMenu": "Mostrando _MENU_ resultados",
            "thousands": ",",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "No hay resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior",
            },
            "aria": {
                "sortAscending": ": Activa para ordenar de forma ascendente",
                "sortDescending": ": Activa para ordenar de forma descendente"
            },
            
        },"scrollX": true

    });



});



function boton_disable()
{
    alert('No tienes los permisos suficientes para realizar esta acción');
}


function detalle_proveedor()
{
    $('#loading').slideDown();
    var path = $(this).attr('data-path');
    var idprov = $(this).attr('data-id');
    //limpian los inputs e img
    $("#foto_producto").attr('src', '');
    $("div.modal-body").find('input').val('');//pa limpiar modals

    console.log(idprov);

    $.post(path,
    {
    id: idprov
    },
            function(response) {
                if (response.code == 100 && response.success) {//dummy check

                console.log(response);
                        var datos = response.detalle;
                        var date = datos.fecha.date;
                        var fechaformateada = new Date(date);
                        var d = fechaformateada.getDate();
                        var m = fechaformateada.getMonth();
                        m += 1;
                        var y = fechaformateada.getFullYear();
                        $("#det_fecha").val(d + "-" + m + "-" + y);
                        $("#det_empresa").val(datos.empresa);
                        $("#det_nombre").val(datos.nombre);
                        $("#det_apellidos").val(datos.apellidos);
                        $("#det_rfc").val(datos.rfc);
                        $("#det_folio").val(datos.folio);
                        $("#det_tipo").val(datos.tipo);
                        $("#det_correo").val(datos.correo);
                        $("#det_pagina").val(datos.pagina);
                        $("#det_calle").val(datos.calle);
                        $("#det_numero").val(datos.numero);
                        $("#det_colonia").val(datos.colonia);
                        $("#det_estado").val(datos.estado);
                        $("#det_municipio").val(datos.municipio);
                        $("#det_telefono").val(datos.telefono);
                        $("#det_contacto").val(datos.contacto);
                        $("#det_telcontacto").val(datos.telcontacto);
                        $("#det_mailcontacto").val(datos.mailcontacto);
                        $("#det_rama").val(datos.rama);
                        $("#det_tamanio").val(datos.tamanio);
                        $("#det_actividad").val(datos.actividad);
                        $("#det_servicios").val(datos.servicios);
                        $("#det_titular").val(datos.titular);
                        $("#det_rfccuenta").val(datos.rfccuenta);
                        $("#det_banco").val(datos.banco);
                        $("#det_cuenta").val(datos.cuenta);
                        $("#det_clabe").val(datos.clabe);
                        $("#idproveedormodificar").val(datos.id);

                        /*
                         $("#eliminarproducto").attr('data-id',datos.id);
                         */


                        //var urlred = window.location.href;
                        //window.location.href = urlred;
                        $('#loading').slideUp('3000');
                $('#detalleproveedores').modal();
            }

            }, "json");
}



function agregarproveedor() {


    $("div.modal-body").find('input').val('');//pa limpiar modals

    // add the rule here
    $.validator.addMethod("valueNotEquals", function(value, element, arg) {
        return arg != value;
    }, "Value must not equal arg.");

    $("#proveedoresform").validate({
        errorClass: "error_validate",
        validClass: "ok_validate",
        rules: {
            estado: {valueNotEquals: "0"},
            nombre: "required",
           
            empresa: "required",
            tipo: "required",
            email: "required",
            calle: "required",
            numero: "required",
            colonia: "required",
            municipio: "required",
            rama: "required",
            actividad: "required"
        }, //fin rules
        messages:
                {
                    estado: "Elija un estado",
                    nombre: "Nombre requerido",
                    
                    empresa: "Nombre de empresa requerido",
                    tipo: "Tipo de empresa requerido",
                    email: "Correo electrónico requerido",
                    calle: "Calle requerida",
                    numero: "Número requerido",
                    colonia: "Colonia requerida",
                    municipio: "Municipio requerido",
                    rama: "Rama de la empresa requerida",
                    actividad: "Actividad de la empresa requerida"
                }, //fin msg
        submitHandler: function(form) {


            //var isGood = confirm('¿Son correctos los datos a ingresar?');
            //if (isGood) {
            $('#loading').slideDown();
            //var path = $("#urlinsertarproducto").val();
            //console.log(path);
            form.submit();



        }, //fin submit handler
        invalidHandler: function(event, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                var message = errors == 1
                        ? 'Please correct the following error:\n'
                        : 'Please correct the following ' + errors + ' errors.\n';
                var errors = "";
                if (validator.errorList.length > 0) {
                    for (x = 0; x < validator.errorList.length; x++) {
                        errors += "\n\u25CF " + validator.errorList[x].message;
                    }
                }
                //  alert(message + errors);
            }
            validator.focusInvalid();

        }



    });//fin validate


}


function modificar_proveedor() {
    $('#loading').slideDown();
    var post = $('#modificar_proveedor').serialize();
    console.log(post);
    var path = $("#btn_modificar").attr("data-path");
    console.log(path);
    $.ajax({
        type: 'POST',
        url: path,
        dataType: 'json',
        data: post,
        success: function(response) {

            if (response.status) {

                
              var urlred = window.location.href;
                window.location.href = urlred;



            } else {
                alert('error');
            }
        },
        error: function(data, status, e)
        {
            alert('error2');
            $('#loading').slideUp();
        }
    });

}


function eliminar_proveedor()
{

    $('#loading').slideDown();
    var path = $(this).attr('data-path');
    var idproducto = $(this).attr('data-id');
    //limpian los inputs e img

    $.post(path,
            {
                id: idproducto
            },
    function(response) {
        if (response.code == 100 && response.success) {//dummy check

            console.log(response);
            var urlred = window.location.href;
            window.location.href = urlred;
            //$('#loading').slideUp('3000');
            // $('#detalleusuarios').modal();
        }

    }, "json");
}