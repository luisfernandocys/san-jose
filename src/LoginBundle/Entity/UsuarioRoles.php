<?php

namespace LoginBundle\Entity;

/**
 * UsuarioRoles
 */
class UsuarioRoles
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $idusuario;

    /**
     * @var int
     */
    private $idrol;

    /**
     * @var string
     */
    private $descripcion;
    
    /**
     * @var \AdminBundle\Entity\Modulos
     */
    private $idmodulo;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idusuario
     *
     * @param integer $idusuario
     *
     * @return UsuarioRoles
     */
    public function setIdusuario($idusuario)
    {
        $this->idusuario = $idusuario;

        return $this;
    }

    /**
     * Get idusuario
     *
     * @return int
     */
    public function getIdusuario()
    {
        return $this->idusuario;
    }

    /**
     * Set idrol
     *
     * @param integer $idrol
     *
     * @return UsuarioRoles
     */
    public function setIdrol($idrol)
    {
        $this->idrol = $idrol;

        return $this;
    }

    /**
     * Get idrol
     *
     * @return int
     */
    public function getIdrol()
    {
        return $this->idrol;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return UsuarioRoles
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    
    /**
     * Set idmodulo
     *
     * @param \AdminBundle\Entity\Modulos $idmodulo
     *
     * @return UsuarioRoles
     */
    public function setIdmodulo(\AdminBundle\Entity\Modulos $idmodulo = null)
    {
        $this->idmodulo = $idmodulo;

        return $this;
    }

    /**
     * Get idmodulo
     *
     * @return \AdminBundle\Entity\Modulos
     */
    public function getIdmodulo()
    {
        return $this->idmodulo;
    }
}
