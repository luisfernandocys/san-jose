<?php

namespace AdminBundle\Repository;

/**
 * ModulosRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ModulosRepository extends \Doctrine\ORM\EntityRepository
{
    public function ObtenerModulos()
    {
        return $this->getEntityManager()->createQueryBuilder('c')
               ->select('m')
               ->from('AdminBundle:Modulos','m')->getQuery()->getArrayResult();
    }
    
    public function ObtenerModulosUsuario($id)
    {
        return $this->getEntityManager()->createQueryBuilder('c')
               ->select('m,ur')
               ->from('LoginBundle:UsuarioRoles','ur')->join('ur.idmodulo', 'm')->where('ur.idusuario = :id')->setParameter('id', $id)->groupBy('m.id')->getQuery()->getArrayResult();
    }
    
    public function ObtenerPermisosModulos($id,$user)
    {
        $parameters = array('id'=>$id,'usuario'=>  $user);
        return $this->getEntityManager()->createQueryBuilder('c')
               ->select('r,ur')
               ->from('LoginBundle:UsuarioRoles','ur')->join('ur.idrol', 'r')->where('ur.idmodulo = :id')->andWhere('ur.idusuario = :usuario')->setParameters($parameters)->getQuery()->getArrayResult();
    }
    
    public function ObtenerModulosUsuarioSinPermiso($id)
    {
        $subQueryBuilder = $this->getEntityManager()->createQueryBuilder();
        $subQuery = $subQueryBuilder
                ->select('md.id')
               ->from('LoginBundle:UsuarioRoles','ur')->join('ur.idmodulo', 'md')->
                where('ur.idusuario = :id')->
                setParameter('id', $id)->groupBy('md.id')->
                getQuery()->
                getResult();
        
                $queryBuilder = $this->getEntityManager()->createQueryBuilder();
                $query = $queryBuilder
               ->select('m')
               ->from('AdminBundle:Modulos','m')->where($queryBuilder->expr()->notIn('m.id', ':sub'))->setParameter('sub', $subQuery)->getQuery();
    
                $result =$query->getArrayResult();
                return $result;
    }
}
